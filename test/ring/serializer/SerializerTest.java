package ring.serializer;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.*;

import ring.serializer.annot.SClass;
import ring.serializer.annot.SConvert;
import ring.serializer.annot.SField;
import ring.serializer.annot.SSubClass;
import ring.serializer.lang.JsonLanguage;
import ring.serializer.token.SMap;
import ring.serializer.token.SObject;
import ring.serializer.token.SValue;
import ring.serializer.types.ConvertTypeDescriptor;

public class SerializerTest {
	@SClass
	static class A extends DefaultSerializable {
		@SField int a1 = -1;
		private String a2 = "test";
		@SField String a2() { return a2; }
		void setA2(String a2) { this.a2 = a2; }
		@SField String a3Type = "b";
		@SField(externalTyped=true) Object a3;
		@SField I a4;
	}
	
	@SSubClass
	static class B extends A implements I {
		double b1 = -1;
		@SField List<A> b2;
		@SField List<Integer> b3 = new ArrayList<>();
		@SField C b4;
		@SField(isLeftover=true) final SMap leftover = new SMap();
		@Override public double x() { return b1; }
		@Override public void setX(double value) { b1 = value; }
	}
	
	static class C {
		@SConvert static final ConvertTypeDescriptor converter =
				new ConvertTypeDescriptor(null,C.class,Long.class,x->x.value,x->new C(x));
		final long value;
		C(long value) { this.value = value; }
		@Override public int hashCode() { throw new UnsupportedOperationException(); }
		@Override public boolean equals(Object obj) { throw new UnsupportedOperationException(); }
	}
	
	@SSubClass
	static class D implements I {
		double d1 = 0;
		
		@Override public double x() { return d1; }
		
		@Override public void setX(double value) { d1 = value; }
	}
	
	@SClass
	static interface I {
		@SField double x();
		void setX(double value);
	}
	
	static class ListA {
		@SField(replaceParent=true) List<A> l;
		@Override public int hashCode() { return Objects.hashCode(l); }
		@Override
		public boolean equals(Object obj) {
			if(obj == null) return l == null;
			return getClass() == obj.getClass() && Objects.equals(l,((ListA)obj).l);
		}
	}
	
	public static void main(String[] args) throws IOException {
		test(new JsonLanguage());
	}
	
	private static void test(Language lang) throws IOException {
		var serializer = DefaultSerializable.serializer.withLanguage(lang);
		var obj = new B();
		obj.setA2("abc def"); obj.b1 = 3;
		obj.b2 = Arrays.asList(new A(),new B());
		obj.b3 = Arrays.asList(4,-2);
		obj.a3 = new B();
		obj.a4 = new D();
		var obj2 = new B();
		obj2.b3 = null;
		obj2.b4 = new C(123);
		obj2.a4 = new B();
		obj2.a4.setX(5);
		((B)obj2.a4).b4 = new C(42);
		obj2.leftover.put("bX",new SValue(456L));
		var obj3 = new ListA();
		obj3.l = Arrays.asList(obj,obj2);
		test(serializer,ListA.class,obj3);
		var names = new LinkedHashMap<String,List<String>>();
		for(var cls : List.of(A.class,B.class,C.class,D.class,I.class,ListA.class)) {
			names.put(cls.getSimpleName(),serializer.getNamesForType(cls));
		}
		System.out.println("names by type: "+names+"\n");
		benchmark(serializer,ListA.class,obj3);
	}
	
	private static <T> void test(Serializer serializer, Class<T> cls, T obj) throws IOException {
		try(var out = new StringWriter();
				var out2 = serializer.charLanguage().createChannel(out)) {
			serializer.serialize(cls,obj,out2);
			System.out.println(out.toString());
			try(var in = new StringReader(out.toString());
					var in2 = serializer.charLanguage().createChannel(in)) {
				var obj2 = serializer.deserialize(cls,in2);
				if(!Objects.equals(obj,obj2)) throw new Error();
				obj2 = serializer.clone(obj);
				if(!Objects.equals(obj,obj2)) throw new Error();
				var sobj1 = serializer.toSObject(cls,obj);
				System.out.println(sobj1.toString());
				obj2 = serializer.toObject(cls,sobj1);
				if(!Objects.equals(obj,obj2)) throw new Error();
				in.reset();
				var sobj2 = serializer.deserialize(SObject.class,in2);
				if(!Objects.equals(sobj1,sobj2) || !sobj1.toString().equals(sobj2.toString())) throw new Error();
			}
		}
		System.out.println();
	}
	
	private static <T> void benchmark(Serializer serializer, Class<T> cls, T obj) throws IOException {
		final long maxRuntime = 3000; int runs;
		try(var out = new StringWriter();
				var out2 = serializer.charLanguage().createChannel(out)) {
			var startTime = System.currentTimeMillis();
			for(runs = 0; ; runs++) {
				if(System.currentTimeMillis()-startTime > maxRuntime) break;
				serializer.serialize(cls,obj,out2);
				if(runs%1000 == 0) clear(out);
			}
			printBenchmarkResult("serialized",System.currentTimeMillis()-startTime,runs);
			clear(out);
			serializer.serialize(cls,obj,out2);
			startTime = System.currentTimeMillis();
			try(var in = new StringReader(out.toString());
					var in2 = serializer.charLanguage().createChannel(in)) {
				in.mark(Integer.MAX_VALUE);
				for(runs = 0; ; runs++) {
					if(System.currentTimeMillis()-startTime > maxRuntime) break;
					in.reset();
					serializer.deserialize(cls,in2);
				}
			}
			printBenchmarkResult("deserialized",System.currentTimeMillis()-startTime,runs);
		}
	}
	
	private static void clear(StringWriter writer) { writer.getBuffer().delete(0,writer.getBuffer().length()); }
	
	private static void printBenchmarkResult(String name, long runtimeMillis, int runs) {
		System.out.printf("%s %d times, runtime: %d ms\n",name,runs,runtimeMillis);
		System.out.printf("%f ms/run\n",runtimeMillis/(double)runs);
		System.out.printf("%.2f runs/ms\n",runs/(double)runtimeMillis);
	}
}
