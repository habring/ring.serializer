package ring.serializer.lang;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import ring.io.InputStreamByteChannel;
import ring.io.OutputStreamByteChannel;
import ring.serializer.token.SToken;
import ring.serializer.token.STokenType;

public class BasicSTokenChannelTest {
	public static void main(String[] args) throws IOException {
		var rand = new Random(0);
		List<SToken> input = new ArrayList<>(), output = new ArrayList<>();
		for(int i = 0; i < 10*1000*1000; i++) input.add(randomToken(rand,false));
		//for(int i = 0; i < 200; i++) input.add(randomToken(rand,true));
		var rawdataLengths = new ArrayList<Integer>();
		byte[] rawdata;
		long startTime = System.currentTimeMillis(), writeTime, readTime;
		try(var raw = new ByteArrayOutputStream(); var out = new BinaryLanguage().createChannel(new OutputStreamByteChannel(raw))) {
			for(var tok : input) {
				var oldSize = raw.size();
				out.write(tok);
				rawdataLengths.add(raw.size()-oldSize);
			}
			writeTime = System.currentTimeMillis()-startTime;
			rawdata = raw.toByteArray();
		}
		startTime = System.currentTimeMillis();
		try(var raw = new ByteArrayInputStream(rawdata); var in = new BinaryLanguage().createChannel(new InputStreamByteChannel(raw))) {
			for(SToken tok; (tok = in.read()) != null; ) output.add(tok);
		}
		readTime = System.currentTimeMillis()-startTime;
		for(int i = 0, off = 0; i < input.size(); i++) {
			SToken tok1 = input.get(i), tok2 = output.get(i);
			var s1 = tok1.toString();
			if(tok1.equals(tok2) && (i > 100 || s1.length() > 1000)) continue;
			var tmp = Arrays.copyOfRange(rawdata,off,off+rawdataLengths.get(i));
			off += tmp.length; var b = new StringBuilder();
			for(int j = 0; j < tmp.length; j++) {
				if(j%32 == 0 && j != 0) b.append('\n');
				b.append(String.format("%02x",tmp[j])).append('|');
			}
			String s2 = b.deleteCharAt(b.length()-1).toString(), s3 = tok2.toString();
			var sep = s1.length() > 100 || s2.contains("\n") ? "\n->\n" : "   ->   ";
			var str = s1+sep+s2+sep+s3;
			if(!tok1.equals(tok2)) throw new Error(str);
			System.out.println(str);
		}
		System.out.printf("written %d tokens (%d bytes)\n",input.size(),rawdata.length);
		System.out.printf("writing took %d ms\n",writeTime);
		System.out.printf("reading took %d ms\n",readTime);
	}
	
	private static SToken randomToken(Random rand, boolean huge) {
		var types = STokenType.values();
		SToken tok;
		switch(rand.nextInt(huge?3:4)) {
		default : tok = new SToken(types[rand.nextInt(types.length)]); break;
		case 0: tok = new SToken(STokenType.fieldName); break;
		case 1: case 2: tok = new SToken(STokenType.primitiveValue); break;
		}
		if(tok.type == STokenType.fieldName) tok.fieldName = randomString(rand,huge);
		else if(tok.type == STokenType.primitiveValue) {
			switch(huge?3:rand.nextInt(5)) {
			case 0: tok.primitiveValue = rand.nextBoolean(); break;
			case 1: tok.primitiveValue = Math.abs(rand.nextLong()) % 1000; break;
			case 2: tok.primitiveValue = rand.nextDouble(); break;
			case 3: tok.primitiveValue = randomString(rand,huge); break;
			}
		}
		tok.checkValid(); return tok;
	}
	
	private static String randomString(Random rand, boolean huge) {
		int len;
		if(huge) len = 1024*1024 + rand.nextInt(1024*1024);
		else if(rand.nextBoolean()) len = rand.nextInt(2) * Byte.MAX_VALUE + rand.nextInt(5) - 2;
		else len = (int)Math.round(rand.nextGaussian() * Byte.MAX_VALUE/4);
		if(!huge) len = Math.min(Math.abs(len),1024*1024);
		var b = new StringBuilder();
		for(int i = 0; i < len; i++) {
			b.append((char)('a'+rand.nextInt(26)));
		}
		return b.toString();
	}
}
