# Ring.Serializer Changelog

# 24.07

- Allow abstract classes and interfaces as base types
- Add various convenience methods to Serializer class
- Add TypeDescriptor.isAssignableFrom
- Improve null handling in ConvertTypeDescriptor
- Various cleanups and bugfixes

# 23.02

- Add FieldDescriptor.isLeftover to allow additional fields while parsing
- Add SConvert annotation to simplify ConvertTypeDescriptor usage
- Allow more types as MapTypeDescriptor keys
- Add headless support for csv output
- Add native byte-array serialization
- Beaking Change: remove SField.policy

# 22.06

- Add some convenient methods to Serializer
- Class-objects can now be serialized properly
- Objects now also get validated before serialization
- Add initializer to ObjectTypeDescriptor
- Various cleanups and bugfixes

# 21.01

- Breaking Change: SRawObject is replaced by SObject for higher usability

# 20.08

- A Channel can be wrapped into a TokenChannel via TokenChannel.wrap
- Add SerializerChannel class (a TokenChannel that uses the Serializer)
- Various cleanups and bugfixes

# 20.07

- Initial Release

