package ring.util;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Objects;

import ring.serializer.token.SToken;
import ring.serializer.token.STokenType;

public final class ErrorMessages extends InternalClass {
	public final String unreachable = "should never happen";
	public final String unexpectedEoi = "unexpected end of input";
	
	public String typeMismatch(Type expected, Type got) {
		return String.format("type mismatch (expected %s, got %s)",expected,got);
	}
	
	public String typeMismatch(Type expected, Type got, Object context) {
		return typeMismatch(expected,got) + " in " + context;
	}
	
	public String duplicateEntry(Object obj) { return duplicateEntry(obj,obj); }
	
	public String duplicateEntry(Object obj1, Object obj2) {
		var s1 = Objects.toString(obj1); var s2 = Objects.toString(obj2);
		if(s1.equals(s2)) return "duplicate entry "+s1;
		else return "duplicate entry "+s1+" and "+s2;
	}
	
	public String needNoArgConstr(Class<?> type) {
		return type+" is expected to have a parameterless constructor";
	}
	
	public SToken checkToken(STokenType expected, SToken got) {
		if(got.type != expected) throw new IllegalArgumentException(invalidToken(got,expected));
		return got;
	}
	
	public String invalidToken(SToken got, STokenType...expected) {
		var b = new StringBuilder("token mismatch (expected ");
		if(expected.length == 1) b.append(expected[0]);
		else b.append("one of ").append(Arrays.toString(expected));
		return b.append(", got ").append(got).toString();
	}
	
	public String invalidPrimType(SToken tok) { return invalidPrimType(tok.primitiveValue != null ? tok.primitiveValue.getClass() : null); }
	
	public String invalidPrimType(Class<?> type) { return "unsupported primitive type "+type; }
}
