package ring.util;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;

public class ClassCrawler extends InternalClass {
	private static final ReflectionUtil reflect = new ReflectionUtil();
	private static volatile Value data;
	
	public List<Class<?>> getAllClasses() {
		var tmp = data;
		return tmp != null ? tmp.allClasses : crawlAllClasses();
	}
	
	public List<Class<?>> getAllSubclasses(Class<?> parent) {
		var tmp = data;
		if(tmp == null) tmp = crawlAllClasses0();
		return tmp.subClasses.getOrDefault(parent,Collections.emptyList());
	}
	
	public void clearCache() { data = null; }
	
	public List<Class<?>> crawlAllClasses() { return crawlAllClasses0().allClasses; }
	
	private Value crawlAllClasses0() {
		var paths = System.getProperty("java.class.path").split(System.getProperty("path.separator"));
		var visitor = new Visitor();
		for(var path : paths) {
			try {
				var root = new File(path).getCanonicalFile().toPath();
				visitor.walk(root);
			} catch(IOException exc) {}
		}
		return data = new Value(visitor.data);
	}
	
	private static class Visitor implements FileVisitor<Path> {
		private final ClassLoader loader = Thread.currentThread().getContextClassLoader();
		private final Set<Class<?>> data = new HashSet<>();
		private Path root;
		
		public void walk(Path path) throws IOException {
			var oldRoot = this.root; this.root = path;
			try { Files.walkFileTree(path,EnumSet.of(FileVisitOption.FOLLOW_LINKS),Integer.MAX_VALUE,this);
			} finally { this.root = oldRoot; }
		}
		
		@Override public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) { return FileVisitResult.CONTINUE; }
		
		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
			try {
				if(!attrs.isRegularFile()) return FileVisitResult.CONTINUE;
				var str = file.getFileName().toString();
				var i = str.lastIndexOf('.');
				if(i < 0) return FileVisitResult.CONTINUE;
				switch(str.substring(i+1).toLowerCase()) {
				case "class":
					var clsName = this.root.relativize(file).toString().replace(File.separatorChar,'.');
					clsName = clsName.substring(0,clsName.length()-".class".length());
					try { add(Class.forName(clsName,false,loader));
					} catch(NoClassDefFoundError exc) {}
					break;
				case "jar":
					var zipfs = FileSystems.newFileSystem(file);
					walk(zipfs.getPath("/"));
					break;
				}
			} catch(Exception exc) {}
			return FileVisitResult.CONTINUE;
		}
		
		@Override public FileVisitResult visitFileFailed(Path file, IOException exc) { return FileVisitResult.CONTINUE; }
		
		@Override public FileVisitResult postVisitDirectory(Path dir, IOException exc) { return FileVisitResult.CONTINUE; }
		
		private void add(Class<?> cls) {
			if(cls == null) return;
			while(cls.isArray()) cls = cls.getComponentType();
			if(!data.add(cls)) return;
			add(cls.getSuperclass());
			for(var e : cls.getInterfaces()) add(e);
			for(var e : cls.getDeclaredClasses()) add(e);
			add(cls.getDeclaringClass());
		}
	}
	
	private static class Value {
		final List<Class<?>> allClasses;
		final Map<Class<?>,List<Class<?>>> subClasses;
		
		Value(Collection<Class<?>> c) {
			this.allClasses = toList(c,true);
			var m = new HashMap<Class<?>,Set<Class<?>>>();
			for(var cls : allClasses) add(m,cls,cls);
			var m2 = new HashMap<Class<?>,List<Class<?>>>();
			for(var e : m.entrySet()) m2.put(e.getKey(),toList(e.getValue(),false));
			this.subClasses = Collections.unmodifiableMap(m2);
		}
		
		private static List<Class<?>> toList(Collection<Class<?>> c, boolean addPrim) {
			var s = new HashSet<>(c);
			if(addPrim) { s.addAll(reflect.primitiveMap.keySet()); s.addAll(reflect.primitiveMap.values()); }
			s.remove(null);
			var l = new ArrayList<>(s); l.sort(Comparator.comparing(e->e.toString()));
			return Collections.unmodifiableList(l);
		}
		
		private static void add(Map<Class<?>,Set<Class<?>>> m, Class<?> cls, Class<?> leaf) {
			if(cls == null) return;
			if(cls != leaf) {
				var s = m.get(cls);
				if(s == null) m.put(cls,s = new HashSet<>());
				if(!s.add(leaf)) return;
			}
			add(m,cls.getSuperclass(),leaf);
			for(var e : cls.getInterfaces()) add(m,e,leaf);
		}
	}
}
