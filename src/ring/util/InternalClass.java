package ring.util;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class InternalClass {
	public InternalClass() { checkAccess(0); }
	
	public static void checkAccess(int depth) {
		var stackTrace = Thread.currentThread().getStackTrace();
		Class<?> caller = null;
		if(depth < stackTrace.length) {
			var refCls = ReflectionUtilPrivate.getClass(stackTrace[depth]);
			for(int i = depth+1; i < stackTrace.length; i++) {
				var cls = ReflectionUtilPrivate.getClass(stackTrace[i]);
				if(cls == null || refCls.isAssignableFrom(cls)) continue;
				caller = cls; break;
			}
		}
		if(caller == null) throw new UnsupportedOperationException("cannot collect stacktrace");
		checkAccessFrom(caller);
	}
	
	public static void checkAccessFrom(Class<?> caller) {
		var p1 = ReflectionUtilPrivate.getPackageNames(InternalClass.class);
		var p2 = ReflectionUtilPrivate.getPackageNames(caller);
		if(!p1[0].equals(p2[0])) throw new UnsupportedOperationException("constructor can only be called from the "+p1[0]+".*-namespace");
	}
	
	@Retention(RetentionPolicy.RUNTIME)
	@Documented
	public @interface InternalApi {}
}
