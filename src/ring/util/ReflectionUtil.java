package ring.util;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.io.File;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

public final class ReflectionUtil extends InternalClass {
	private static final ErrorMessages errors = new ErrorMessages();
	public final Map<Class<?>,Class<?>> primitiveMap = createPrimitiveMap(false);
	public final Map<Class<?>,Class<?>> primitiveMapInv = createPrimitiveMap(true);
	
	private Map<Class<?>,Class<?>> createPrimitiveMap(boolean inverse) {
		var m = new HashMap<Class<?>,Class<?>>();
		BiConsumer<Class<?>,Class<?>> add = (w,p) -> { if(inverse) m.put(p,w); else m.put(w,p); };
		add.accept(Void.class,void.class); add.accept(Boolean.class,boolean.class); add.accept(Byte.class,byte.class); add.accept(Character.class,char.class);
		add.accept(Short.class,short.class); add.accept(Integer.class,int.class); add.accept(Long.class,long.class); add.accept(Float.class,float.class); add.accept(Double.class,double.class);
		return Collections.unmodifiableMap(m);
	}
	
	public Class<?> getBaseType(Type genericType) {
		if(genericType instanceof Class<?> cls) return cls;
		try {
			return (Class<?>)((ParameterizedType)genericType).getRawType();
		} catch(Exception exc) { throw exc; }
	}
	
	public Class<?>[] getTypeParameters(Type genericType) {
		if(genericType instanceof Class) return new Class[0];
		var args = ((ParameterizedType)genericType).getActualTypeArguments();
		var ret = new Class[args.length];
		for(int i = 0; i < args.length; i++) ret[i] = (Class<?>)args[i];
		return ret;
	}
	
	public Type[] matchTypeParameters(Class<?> type, Class<?> baseType) {
		type.asSubclass(baseType);
		var ret = tryMatchTypeParameters0(type,baseType);
		if(ret != null) return ret;
		throw new Error(errors.unreachable);
	}
	
	@SuppressWarnings("TODO make exact")
	private Type[] tryMatchTypeParameters0(Type type, Class<?> baseType) {
		if(type instanceof Class<?> cls) {
			if(!baseType.isAssignableFrom(cls)) return null;
			if(cls == baseType) return new Type[0];
			var ret = tryMatchTypeParameters0(cls.getGenericSuperclass(),baseType);
			if(ret == null) {
				if(!baseType.isInterface()) throw new Error(errors.unreachable);
				for(var e : cls.getGenericInterfaces()) {
					ret = tryMatchTypeParameters0(e,baseType);
					if(ret != null) return ret;
				}
			}
			if(ret != null) return ret;
			throw new Error(errors.unreachable);
		}
		else if(type instanceof ParameterizedType ptype) {
			var cls = (Class<?>)ptype.getRawType();
			if(!baseType.isAssignableFrom(cls)) return null;
			if(cls == baseType) return ptype.getActualTypeArguments();
//			TODO
			throw new UnsupportedOperationException("not implemented yet for type "+ptype+" -> "+baseType);
		}
		else throw new Error(errors.unreachable);
	}
	
	public MethodHandle unreflect(Method m) {
		try { return MethodHandles.lookup().unreflect(check(m));
		} catch(IllegalAccessException exc) { throw new Error(exc); }
	}
	
	public MethodHandle unreflect(Constructor<?> c) {
		var cls = c.getDeclaringClass();
		if(Modifier.isAbstract(cls.getModifiers()) || cls.isInterface()) {
			throw new IllegalArgumentException(cls+" must not be abstract");
		}
		try { return MethodHandles.lookup().unreflectConstructor(check(c));
		} catch(IllegalAccessException exc) { throw new Error(exc); }
	}
	
	public <T> T createInstance(Class<? extends T> cls) {
		try { var constr = cls.getConstructor(); constr.setAccessible(true); return constr.newInstance(); }
		catch (Throwable t) { throw new Error(errors.needNoArgConstr(cls),t); }
	}
	
	private <T extends Executable> T check(T e) {
		for(var exc : e.getExceptionTypes()) {
			if(RuntimeException.class.isAssignableFrom(exc)) continue;
			if(Error.class.isAssignableFrom(exc)) continue;
			throw new IllegalArgumentException(e+" throws checked exception type "+exc);
		}
		e.setAccessible(true); return e;
	}
	
	public Method resolveOverride(Method m, Class<?> child) {
		if(!m.getDeclaringClass().isAssignableFrom(child)) throw new ClassCastException(errors.typeMismatch(m.getDeclaringClass(),child));
		var todo = new ArrayList<Class<?>>();
		todo.add(child);
		while(todo.size() > 0) {
			var cls = todo.remove(todo.size()-1);
			if(cls == null || !m.getDeclaringClass().isAssignableFrom(cls)) continue;
			for(var e : cls.getDeclaredMethods()) {
				if(overrides(m,e)) return e;
			}
			for(var p : cls.getInterfaces()) todo.add(p);
			todo.add(cls.getSuperclass());
		}
		throw new Error(errors.unreachable);
	}
	
	public boolean overrides(Method parent, Method child) {
		if(!parent.getName().equals(child.getName())) return false;
		if(Modifier.isStatic(parent.getModifiers()) || Modifier.isStatic(child.getModifiers())) return false;
		if(!parent.getDeclaringClass().isAssignableFrom(child.getDeclaringClass())) return false;
		if(parent.getParameterCount() != child.getParameterCount()) return false;
		if(!parent.getReturnType().isAssignableFrom(child.getReturnType())) return false;
		var pt = parent.getParameterTypes(); var ct = child.getParameterTypes();
		for(int i = 0; i < pt.length; i++) {
			if(!ct[i].isAssignableFrom(pt[i])) return false;
		}
		return true;
	}
	
	public Method getParent(Method m) {
		var ret = getParent(m,m.getDeclaringClass().getSuperclass());
		if(ret != null) return ret;
		for(var cls : m.getDeclaringClass().getInterfaces()) {
			ret = getParent(m,cls);
			if(ret != null) return ret;
		}
		return ret;
	}
	
	private Method getParent(Method m, Class<?> parent) {
		if(parent == null) return null;
		for(var p : parent.getDeclaredMethods()) {
			if(overrides(p,m)) return p;
		}
		return null;
	}
	
	public Method[] getMethods(StackTraceElement[] elems) {
		if(elems == null) return null;
		var ret = new Method[elems.length];
		for(int i = 0; i < elems.length; i++) ret[i] = getMethod(elems[i]);
		return ret;
	}
	
	public Method getMethod(StackTraceElement elem) {
		try {
			if(elem == null) return null;
			var cls = Class.forName(elem.getClassName());
			for(var m : cls.getDeclaredMethods()) {
				if(m.getName().equals(elem.getMethodName())) return m;
			}
			return null;
		} catch (Exception exc) { return null; }
	}
	
	public Class<?>[] getClasses(StackTraceElement[] elems) {
		if(elems == null) return null;
		Class<?>[] ret = new Class[elems.length];
		for(int i = 0; i < elems.length; i++) ret[i] = getClass(elems[i]);
		return ret;
	}
	
	public Class<?> getClass(StackTraceElement elem) { return ReflectionUtilPrivate.getClass(elem); }
	
	public String[] getPackageNames(Class<?> cls) { return ReflectionUtilPrivate.getPackageNames(cls); }
	
	public File getContainingFile(Class<?> cls) {
		var url = cls.getResource(cls.getSimpleName()+".class");
		switch(url.getProtocol()) {
		case "file":
			var tmp = url.getFile();
			var suffix = cls.getPackage().getName().replaceAll("\\.",File.separator)+File.separator+cls.getSimpleName()+".class";
			if(tmp.endsWith(suffix)) return new File(tmp.substring(0,tmp.length()-suffix.length()));
		case "jar":
			var i = url.getFile().indexOf(".jar!");
			if(url.getFile().startsWith("file:") && i >= 0) {
				return new File(url.getFile().substring(5,i+4));
			}
		}
		throw new IllegalArgumentException("cannot parse url "+url);
	}
}
