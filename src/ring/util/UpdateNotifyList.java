package ring.util;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import ring.util.InternalClass.InternalApi;

public class UpdateNotifyList<E> extends AbstractList<E> {
	private final Runnable owner;
	private final List<E> l;
	
	@InternalApi
	public UpdateNotifyList(Runnable owner) { this(owner,new ArrayList<>()); }
	
	@InternalApi
	public UpdateNotifyList(Runnable owner, List<E> l) {
		InternalClass.checkAccess(1);
		this.owner = Objects.requireNonNull(owner);
		this.l = Objects.requireNonNull(l);
	}
	
	@Override public int size() { return l.size(); }
	
	@Override public E get(int i) { return l.get(i); }
	
	@Override public E set(int i, E elem) { owner.run(); return l.set(i,elem); }
	
	@Override public void add(int i, E elem) { owner.run(); l.add(i,elem); }
	
	@Override public E remove(int i) { owner.run(); return l.remove(i); }
}
