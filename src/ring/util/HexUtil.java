package ring.util;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.util.Objects;

public class HexUtil {
	private static final String[] hexCache = new String[0x100];
	
	static {
		for(int i = 0; i < hexCache.length; i++) hexCache[i] = "%02x".formatted(i);
	}
	
	private HexUtil() {}
	
	public static String toHexString(byte[] data) { return toHexString(data,0,data.length); }
	
	public static String toHexString(byte[] data, int offset, int length) {
		Objects.checkFromIndexSize(offset,length,data.length); var b = new StringBuilder(2*length);
		for(int i = 0; i < length; i++) b.append(hexCache[data[offset+i]&0xff]);
		return b.toString();
	}
	
	public static boolean isValidHexString(String data) {
		if(data.length()%2 != 0) return false;
		for(int i = 0; i < data.length(); i++) {
			var c = data.charAt(i);
			if('0' <= c && c <= '9') continue;
			if('a' <= c && c <= 'f') continue;
			if('A' <= c && c <= 'F') continue;
			return false;
		}
		return true;
	}
	
	public static byte[] parseHexString(String data) throws NumberFormatException {
		return parseHexString(data,new byte[data.length()/2],0);
	}
	
	public static byte[] parseHexString(String data, byte[] dest, int destOffset) throws NumberFormatException {
		if(data.length()%2 != 0) throw new NumberFormatException("invalid input length");
		Objects.checkFromIndexSize(destOffset,data.length()/2,dest.length);
		for(int i = 0; i < data.length(); i+=2) {
			dest[destOffset+i/2] = (byte)Integer.parseUnsignedInt(data,i,i+2,16);
		}
		return dest;
	}
}
