package ring.serializer;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.lang.reflect.Modifier;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import ring.serializer.lang.BinaryLanguage;

public abstract class DefaultSerializable implements Cloneable {
	public static final Serializer serializer = new Serializer(new SerializerConfig().setLanguage(new BinaryLanguage()));
	private static final Map<Class<?>,Class<?>> descriptors = new ConcurrentHashMap<>();
	
	public DefaultSerializable() {
		if(descriptors.putIfAbsent(getClass(),getClass()) != null) return;
		if(!Modifier.isAbstract(getClass().getModifiers())) serializer.getDescriptor(getClass());
	}
	
	@Override public final int hashCode() { return serializer.hashCode(this); }
	
	@Override public final boolean equals(Object obj) { return serializer.equals(this,obj); }
	
	@Override public final Object clone() { return serializer.clone(this); }
}
