package ring.serializer;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import static ring.serializer.token.STokenType.elemSeperator;
import static ring.serializer.token.STokenType.endList;
import static ring.serializer.token.STokenType.startList;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ConcurrentModificationException;
import java.util.List;

import ring.io.STokenChannel;
import ring.serializer.token.SList;
import ring.serializer.token.SObject;
import ring.serializer.token.SToken;
import ring.serializer.types.ListTypeDescriptor;
import ring.serializer.types.ObjectTypeDescriptor;
import ring.serializer.types.TypeDescriptor.Kind;

/**implementation for {@link ListTypeDescriptor}*/
abstract class SerializerImplList extends SerializerBase {
	SerializerImplList(SerializerConfig config) { super(config); }
	
	/**@see List#hashCode*/
	final int hashCode0(ListTypeDescriptor type, Object obj, int maxDepth) {
		int ret = 1;
		for(var it = type.iterator(obj); it.hasNext(); ) {
			var elem = it.next();
			ret = 31*ret + hashCode0(type.elementType,elem,maxDepth);
		}
		return ret;
	}
	
	/**@see List#equals*/
	final Boolean equals0(ListTypeDescriptor type, Object obj1, Object obj2, int maxDepth) {
		var s1 = type.sizeOf(obj1); var s2 = type.sizeOf(obj2);
		if(s1 != s2) return false;
		var it1 = type.iterator(obj1); var it2 = type.iterator(obj2);
		while(true) {
			var nxt = it1.hasNext();
			if(nxt != it2.hasNext()) throw new ConcurrentModificationException();
			if(!nxt) return true;
			var elem1 = it1.next(); var elem2 = it2.next();
			var eq = equals0(type.elementType,elem1,elem2,maxDepth);
			if(eq == null || !eq) return eq;
		}
	}
	
	final void copy0(ListTypeDescriptor type, Object src, Object dest, int maxDepth) {
		var it1 = type.iterator(src); var it2 = type.iterator(dest);
		while(true) {
			if(!it1.hasNext()) break;
			var srcElem = it1.next();
			if(it2 != null && it2.hasNext()) {
				var destElem = it2.next();
				var destElem2 = copy0(type.elementType,srcElem,destElem,maxDepth);
				if(destElem != destElem2) { //fallback for primitive types
					type.clear(dest);
					it1 = type.iterator(src);
					while(it1.hasNext()) {
						type.add(dest,clone0(it1.next(),maxDepth));
					}
					return;
				}
			}
			else { it2 = null; type.add(dest,clone0(srcElem,maxDepth)); }
		}
		if(it2 != null) {
			while(it2.hasNext()) { it2.next(); it2.remove(); }
		}
	}
	
	final void readListInput0(SToken la, STokenChannel in) throws IOException {
		errors.checkToken(startList,la);
		var first = true;
		while(true) {
			var tok = readTok(in);
			if(tok.type == endList) return;
			if(!first) { checkSeperator(tok); tok = readTok(in); }
			readInput0(tok,in); first = false;
		}
	}
	
	final Object deserialize0(ListTypeDescriptor type, SToken la, STokenChannel in) throws IOException {
		var obj = type.createInstance(); type.clear(obj);
		if(type.simplifySingleElements() && la.type != startList) {
			var elem = deserialize0(type.elementType,la,in);
			type.add(obj,elem); return obj;
		}
		errors.checkToken(startList,la);
		var first = true;
		while(true) {
			var tok = readTok(in);
			if(tok.type == endList) return obj;
			if(!first) { checkSeperator(tok); tok = readTok(in); }
			var elem = deserialize0(type.elementType,tok,in);
			type.add(obj,elem); first = false;
		}
	}
	
	final Object deserializeS0(ListTypeDescriptor type, SObject obj) {
		var ret = type.createInstance(); type.clear(ret);
		if(type.simplifySingleElements() && obj.kind() != SObject.Kind.list) {
			var elem = deserializeS0(type.elementType,obj);
			type.add(ret,elem); return ret;
		}
		for(var elem1 : (SList)checkSKind(obj,SObject.Kind.list)) {
			var elem2 = deserializeS0(type.elementType,elem1);
			type.add(ret,elem2);
		}
		return ret;
	}
	
	final void serialize0(ListTypeDescriptor type, List<Annotation> annotations, Object obj, STokenChannel out) throws IOException {
		if(type.simplifySingleElements() && type.sizeOf(obj) == 1) {
			var elemType = getDescriptor0(type.elementType).type;
			switch(elemType.kind()) {
			case primitive: case map: case object:
				if(elemType.kind() == Kind.object && ((ObjectTypeDescriptor)elemType).fieldReplacesParent()) {
					break;
				}
				var it = type.iterator(obj);
				if(!it.hasNext()) break; //eliminates race conditions
				var elem = it.next();
				if(it.hasNext()) break;
				serialize0(type.elementType,elem,out);
				return;
			default: break;
			}
		}
		out.write(setAnnot(new SToken(startList),annotations)); var first = true;
		for(var it = type.iterator(obj); it.hasNext(); ) {
			if(!first) out.write(new SToken(elemSeperator));
			var elem = it.next();
			serialize0(type.elementType,elem,out);
			first = false;
		}
		out.write(new SToken(endList));
	}
	
	final SObject serializeS0(ListTypeDescriptor type, Object obj) {
		if(type.simplifySingleElements() && type.sizeOf(obj) == 1) {
			var elemType = getDescriptor0(type.elementType).type;
			switch(elemType.kind()) {
			case primitive: case map: case object:
				if(elemType.kind() == Kind.object && ((ObjectTypeDescriptor)elemType).fieldReplacesParent()) {
					break;
				}
				var it = type.iterator(obj);
				if(!it.hasNext()) break;
				var elem = it.next();
				if(it.hasNext()) break;
				return serializeS0(type.elementType,elem);
			default: break;
			}
		}
		var ret = new SList();
		for(var it = type.iterator(obj); it.hasNext(); ) {
			var elem = it.next();
			var elem2 = serializeS0(type.elementType,elem);
			ret.add(elem2);
		}
		return ret;
	}
}
