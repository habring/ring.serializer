package ring.serializer.lang;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import static ring.serializer.token.STokenType.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ring.io.CharChannel;
import ring.serializer.lang.CsvLanguage.CsvConfig;
import ring.serializer.token.SToken;

class CsvChannel extends AbstractCharChannel<CsvConfig> {
	private final List<String> header = new ArrayList<>();
	private final List<SToken> wbuf = new ArrayList<>();
	private boolean headerValid;
	private int rIdx = -1, wIdx = -1;
	
	public CsvChannel(CsvLanguage lang, CharChannel ch) { super(lang,ch); }
	
	@Override
	public SToken read() throws IOException {
		if(wIdx >= 0) throw new IllegalStateException();
		if(rIdx < 0 && (!skipNewLine() || !readHeader())) return null;
		if(rIdx < 0) { rIdx++; return new SToken(startObject); }
		if(rIdx/3 >= header.size()) { rIdx = -1; return new SToken(endObject); }
		switch(rIdx%3) {
		case 0: rIdx++; return SToken.createFieldName(header.get(rIdx/3));
		case 1:
			rIdx++;
			if(rIdx/3+1 >= header.size()) rIdx++;
			return readField();
		case 2: checkRbuf(','); readChar(); rIdx++; return new SToken(fieldSeperator);
		default: throw new Error(errors.unreachable);
		}
	}
	
	@Override
	public boolean write(SToken tok, SToken la) throws IOException {
		if(rIdx >= 0) throw new IllegalStateException();
		switch(tok.type) {
		case startObject: break;
		case fieldName:
			wIdx++;
			if(headerValid) checkFieldName(wIdx,tok);
			else header.add(tok.fieldName);
			errors.checkToken(primitiveValue,la);
			break;
		case primitiveValue:
			if(wIdx < 0) throw new IllegalArgumentException("primitive values are not allowed as root elements");
			if(headerValid) writeField(tok);
			else wbuf.add(tok);
			break;
		case fieldSeperator:
			if(headerValid) write(',');
			break;
		case endObject:
			if(!headerValid) {
				if(header.isEmpty()) throw new IllegalStateException("object has no fields");
				if(header.size() != wbuf.size()) throw new IllegalStateException();
				writeHeader();
				for(int i = 0; i < wbuf.size(); i++) {
					writeField(wbuf.get(i));
					if(i < wbuf.size()-1) write(',');
				}
				wbuf.clear(); headerValid = true;
			}
			if(la.type != endOfInput) throw new IllegalStateException("object end reached, but not end of input");
			wIdx = -1;
			break;
		case startList: throw new IllegalArgumentException("lists are not allowed");
		default: throw new IllegalArgumentException("invalid token "+tok);
		}
		return false;
	}
	
	private boolean readHeader() throws IOException {
		if(headerValid) return true;
		while(true) {
			header.add((String)readField().primitiveValue);
			var c = rbuf.get(0);
			if(c != ',') break;
			readChar();
		}
		if(rbuf.get(0) != '\r') checkRbuf('\n');
		headerValid = true;
		return skipNewLine();
	}
	
	private void writeHeader() throws IOException {
		if(lang.config.headless) return;
		for(int i = 0; i < header.size(); i++) {
			writeField(SToken.createPrimitiveValue(header.get(i)));
			if(i < header.size()-1) write(',');
		}
		writeNewLine();
	}
	
	private void checkFieldName(int i, SToken fld) {
		if(!header.get(i).equals(fld.fieldName)) {
			throw new IllegalArgumentException(String.format("invalid field at index %d (expected %s, got %s)",i,header.get(i),fld.primitiveValue));
		}
	}
	
	private SToken readField() throws IOException {
		var c = rbuf.get(0);
		if(c == '\"') return SToken.createPrimitiveValue(readEscField());
		var b = new StringBuilder();
		while(!needsEscaping(c)) {
			b.append(c);
			if(!readCharOptional()) break;
			c = rbuf.get(0);
		}
		return SToken.createPrimitiveValue(b.length() == 0 ? SToken.skipValue : b.toString());
	}
	
	private void writeField(SToken tok) throws IOException {
		if(tok.primitiveValue == SToken.skipValue) return;
		if(tok.primitiveValue != null && tok.primitiveValue.getClass() == String.class) {
			var str = (String)tok.primitiveValue;
			switch(lang.config.escaping) {
			case none:
				if(needsEscaping(str)) throw new IllegalArgumentException(str+" needs escaping");
				writePrimValue(tok,false); return;
			case ifNeeded:
				if(needsEscaping(str)) writeEscField(str);
				else writePrimValue(tok,false);
				return;
			case all: writeEscField(str); return;
			default: throw new Error(errors.unreachable);
			}
		}
		else if(lang.config.escaping == Escaping.all) {
			write('\"'); writePrimValue(tok,false); write('\"');
		}
		else writePrimValue(tok,false);
	}
	
	private String readEscField() throws IOException {
		checkRbuf('\"');
		var b = new StringBuilder();
		while(true) {
			var c = readChar();
			if(c != '\"') { b.append(c); continue; }
			c = readChar();
			if(c == '\"') { b.append(c); continue; }
			break;
		}
		return b.toString();
	}
	
	private void writeEscField(String str) throws IOException {
		write('\"');
		for(int i = 0; i < str.length(); i++) {
			var c = str.charAt(i);
			if(c == '\"') write(c);
			write(c);
		}
		write('\"');
	}
	
	@Override
	void writeStr(String str, boolean quoted) throws IOException {
		if(quoted) throw new Error(errors.unreachable);
		writeStrDirect(str);
	}
	
	@Override void writeNewLine() throws IOException { writeStrDirect(lang.config.newLine); }
	
	private boolean skipNewLine() throws IOException {
		while(readCharOptional()) {
			var c = rbuf.get(0);
			if(c != '\n' && c != '\r') return true;
		}
		return false;
	}
	
	private boolean needsEscaping(String str) {
		if(str.isEmpty() || str.equals("null")) return true;
		for(int i = 0; i < str.length(); i++) {
			if(needsEscaping(str.charAt(i))) return true;
		}
		return false;
	}
	
	private boolean needsEscaping(char c) {
		switch(c) {
		case '\n': case '\r': case '\"': case ',': return true;
		default: return false;
		}
	}
}
