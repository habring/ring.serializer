package ring.serializer.lang;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.util.Objects;

import ring.serializer.CharLanguage;
import ring.util.ErrorMessages;

abstract class AbstractCharLanguage<T> extends CharLanguage {
	static final ErrorMessages errors = new ErrorMessages();
	
	static final NeedsFixupReadValueType fixupStrings = new NeedsFixupReadValueType(AbstractCharLanguage::fixupStrings);
	
	protected final T config;
	
	AbstractCharLanguage(T config, LanguageFeature...features) { super(features); this.config = Objects.requireNonNull(config); }
	
	@Override public int hashCode() { return config.hashCode(); }
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if(obj == this) return true;
		return obj != null && getClass() == obj.getClass() && config.equals(((AbstractCharLanguage<T>)obj).config);
	}
	
	static Object fixupStrings(Object value, Class<?> targetType) {
		Objects.requireNonNull(targetType);
		if(value.getClass() == String.class) {
			if("null".equals(value)) return null;
			if(targetType == String.class) return value;
			if(targetType == Boolean.class) {
				if("true".equals(value)) return true;
				if("false".equals(value)) return false;
			}
			if(targetType == Long.class) return Long.parseLong((String)value);
			if(targetType == Double.class) return Double.parseDouble((String)value);
		}
		if(value.getClass() == targetType) return value;
		throw new ClassCastException(errors.typeMismatch(String.class,value.getClass()));
	}
}
