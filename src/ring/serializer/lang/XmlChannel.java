package ring.serializer.lang;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import static ring.serializer.token.STokenType.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ring.io.CharChannel;
import ring.serializer.token.SToken;

class XmlChannel extends AbstractCharIndentedChannel<SpacingConfig> {
	private static final String objTag = "object", fldTag = "field", fldAttr = "name", listTag = "list", elemTag = "element";
	private final List<SToken> rnxt = new ArrayList<>();
	private boolean wPrevWasEndListOrObj;
	
	XmlChannel(XmlLanguage lang, CharChannel ch) { super(lang,ch); }
	
	@Override public SToken read() throws IOException { return read0(false); }
	
	private SToken read0(boolean mayStartWithValue) throws IOException {
//		This parser does not in all cases validate that an opening tag always has a matching closing tag.
//		To implement this, it would be necessary to keep track of all the opened tags. This is not done for memory efficiency.
//		In fact, only the <element>-tag is affected by that, because all other tags have a representation in SToken.
//		Eliminating the <element>-tags is not possible because lists of primitive values could not be serialized without them.
		if(!rnxt.isEmpty()) return rnxt.remove(rnxt.size()-1);
		if(mayStartWithValue) {
			var value = readStr('<');
			if(!value.isBlank()) return SToken.createPrimitiveValue(value);
			var la = read0(false);
			if(la == null) throw new IllegalArgumentException(errors.unexpectedEoi);
			if(la.type == startObject || la.type == startList) return la;
			rnxt.add(la); return SToken.createPrimitiveValue(value);
		}
		if(!skipWhitespace()) return null;
		checkRbuf('<'); readWhitespace();
		boolean endTag = false;
		var c = rbuf.get(0);
		if(c == '/') { endTag = true; readWhitespace(); }
		else if(c == '!') { skipComment(); return read(); }
		var tagName = readTagName(); readWhitespace();
		SToken ret;
		switch(tagName) {
		case objTag: case listTag:
			var isObj = tagName.equals(objTag);
			c = rbuf.get(0);
			if(!endTag && c == '/') {
				rnxt.add(new SToken(isObj?endObject:endList));
				readChar();
			}
			checkRbuf('>');
			if(endTag) return new SToken(isObj?endObject:endList);
			return new SToken(isObj?startObject:startList);
		case elemTag:
			checkRbuf('>'); readChar(); ret = read0(!endTag);
			if(!endTag || ret.type == endList) return ret;
			rnxt.add(ret); return new SToken(elemSeperator);
		case fldTag:
			if(endTag) {
				checkRbuf('>');
				ret = read();
				if(ret.type == endObject) return ret;
				rnxt.add(ret); return new SToken(fieldSeperator);
			}
			var attrName = readTagName();
			if(!attrName.equals(fldAttr)) throw new IllegalArgumentException("expected attribute "+fldAttr+", got "+attrName);
			checkRbuf('='); readChar(); checkRbuf('\"');
			readChar(); var attrValue = readStr('\"'); checkRbuf('\"');
			rbuf.position(1); readWhitespace(); checkRbuf('>');
			readChar(); rnxt.add(read0(true));
			return SToken.createFieldName(attrValue);
		default: throw new IllegalArgumentException("invalid tag name "+tagName);
		}
	}
	
	@Override
	boolean write(SToken tok, SToken la) throws IOException {
		switch(tok.type) {
		case startObject: case startList:
			if(la.type == endObject || la.type == endList) {
				write('<'); writeTagName(tok.type==startObject?objTag:listTag); write('/'); write('>');
				wPrevWasEndListOrObj = true;
				return true;
			}
			else {
				write0(tok); incIndent();
				if(tok.type == startObject || la.type != primitiveValue) writeNewLine();
			}
			break;
		case fieldSeperator: write0(tok); writeNewLine(); break;
		case elemSeperator:
			decIndent();
			if(wPrevWasEndListOrObj) writeNewLine();
			write0(tok); incIndent();
			if(la.type != primitiveValue) writeNewLine();
			break;
		case endObject: case endList:
			decIndent();
			if(wPrevWasEndListOrObj) writeNewLine();
			write0(tok);
			wPrevWasEndListOrObj = true; return false;
		default:
			write0(tok);
			if(tok.type == fieldName && la.type != primitiveValue) writeNewLine();
			break;
		}
		wPrevWasEndListOrObj = false; return false;
	}
	
	private void write0(SToken tok) throws IOException {
		switch(tok.type) {
		case startObject: writeStartTag(objTag); break;
		case endObject: writeEndFld(); writeNewLine(); writeEndTag(objTag); break;
		case fieldName: writeStartFld(tok.fieldName); break;
		case fieldSeperator: writeEndFld(); break;
		case elemSeperator: writeEndTag(elemTag); writeNewLine(); writeStartTag(elemTag); break;
		case startList: writeStartTag(listTag); incIndent(); writeNewLine(); writeStartTag(elemTag); break;
		case endList: writeEndTag(elemTag); decIndent(); writeNewLine(); writeEndTag(listTag); break;
		case primitiveValue: writePrimValue(tok,false); break;
		default: throw new Error(errors.unreachable);
		}
	}
	
	private void writeStartFld(String name) throws IOException {
		write('<'); writeStrDirect(fldTag); write(' '); writeStrDirect(fldAttr);
		write('='); write('\"'); writeStr(name,false); write('\"'); write('>');
		incIndent();
	}
	
	private void writeEndFld() throws IOException { writeEndTag(fldTag); decIndent(); }
	
	private void writeStartTag(String str) throws IOException {
		write('<'); writeTagName(str); write('>');
	}
	
	private void writeEndTag(String str) throws IOException { write('<'); write('/'); writeTagName(str); write('>'); }
	
	private String readTagName() throws IOException {
		var c = rbuf.get(0);
		var b = new StringBuilder().append(c);
		if(!isValidTagStartChar(c)) throw new IllegalArgumentException("invalid tag name "+b);
		while(true) {
			c = readChar();
			if(!isValidTagChar(c)) break;
			b.append(c);
		}
		rbuf.position(0); return b.toString();
	}
	
	private void writeTagName(String str) throws IOException {
		for(int i = 0; i < str.length(); i++) {
			var c = str.charAt(i);
			var isValid = i == 0 ? isValidTagStartChar(c) : isValidTagChar(c);
			if(!isValid) throw new IllegalArgumentException("invalid tag name "+str);
		}
		writeStrDirect(str);
	}
	
	@Override
	String readStr(char endChar) throws IOException {
		var b = new StringBuilder();
		var c = rbuf.get(0);
		while(true) {
			if(c == endChar) { rbuf.position(0); return b.toString(); }
			else if(c == '&') readEscaped(b);
			else b.append(c);
			c = readChar();
		}
	}
	
	@Override
	void writeStr(String str, boolean quoted) throws IOException {
		if(quoted) throw new Error(errors.unreachable);
		if(str.equals("null")) {
//			the string "null" cannot be distinguished from the value null when reading, therefore it is escaped
			writeUnicode('n'); writeStrDirect("ull"); return;
		}
		for(int i = 0; i < str.length(); i++) {
			var c = str.charAt(i);
			if(isValid(c)) {
				switch(c) {
				case '<': writeStrDirect("&lt;"); break;
				case '>': writeStrDirect("&gt;"); break;
				case '&': writeStrDirect("&amp;"); break;
				case '\'': writeStrDirect("&apos;"); break;
				case '\"': writeStrDirect("&quot;"); break;
				default: write(c); break;
				}
			}
			else if(Character.isHighSurrogate(c)) {
				var c2 = str.charAt(i+1);
				if(!Character.isSurrogatePair(c,c2)) throw new IllegalArgumentException("invalid surrogate pairs in string "+str);
				writeUnicode(Character.toCodePoint(c,c2));
				i++;
			}
			else writeUnicode(c);
		}
	}
	
	private void readEscaped(StringBuilder target) throws IOException {
		var c = readChar();
		switch(c) {
		case '#': target.appendCodePoint(readUnicode()); break;
		case 'l': readStrDirect("&lt;",2); target.append('<'); break;
		case 'g': readStrDirect("&gt;",2); target.append('>'); break;
		case 'a':
			c = readChar();
			if(c == 'm') { readStrDirect("&amp;",3); target.append('&'); }
			else if(c == 'p') { readStrDirect("&apos;",3); target.append('\''); }
			else throw new IllegalArgumentException("expected primitive value &amp; or &apos;, got &a"+c);
			break;
		case 'q': readStrDirect("&quot;",2); target.append('\"'); break;
		}
	}
	
	@Override
	int readUnicode() throws IOException {
		var b = new StringBuilder();
		while(rbuf.get(0) != ';') b.append(readChar());
		return Integer.parseInt(b.toString());
	}
	
	@Override
	void writeUnicode(int pt) throws IOException {
		writeStrDirect("&#"); writeStrDirect(Integer.toString(pt)); write(';');
	}
	
	private void skipComment() throws IOException {
		char c = readChar(), c2 = readChar();
		if(c != '-' || c2 != '-') throw new IllegalArgumentException("invalid start of comment <!"+c+""+c2);
		while(true) {
			c = readChar();
			if(c != '-') continue;
			c = readChar();
			if(c != '-') continue;
			c = readChar();
			if(c == '>') break;
			throw new IllegalArgumentException("invalid end of comment --"+c);
		}
	}
	
	private static boolean isValidTagChar(char c) {
		if(isValidTagStartChar(c)) return true;
		if('0' <= c && c <= '9') return true;
		if(c == '-' || c == '.') return true;
		return false;
	}
	
	private static boolean isValidTagStartChar(char c) {
		if('a' <= c && c <= 'z') return true;
		if('A' <= c && c <= 'Z') return true;
		if(c > 0x7f || c == '_') return true;
		return false;
	}
	
	private static boolean isValid(char c) {
		if(c <= 0x1f) return c == '\t' || c == '\n' || c == '\r';
		else return !Character.isSurrogate(c) && c != 0x7f;
	}
}
