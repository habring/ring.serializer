package ring.serializer.lang;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import ring.io.CharChannel;
import ring.io.STokenChannel;

/**Simplified XML (without attributes).<br>
 * https://en.wikipedia.org/wiki/XML*/
public final class XmlLanguage extends AbstractCharLanguage<SpacingConfig> {
	public XmlLanguage() { this(true); }
	
	public XmlLanguage(boolean indent) { super(new SpacingConfig(indent),fixupStrings); }
	
	public XmlLanguage(String newLine, String indent) { super(new SpacingConfig(newLine,indent),fixupStrings); }
	
	@Override public STokenChannel createChannel(CharChannel ch) { return new XmlChannel(this,ch); }
}
