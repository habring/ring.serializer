package ring.serializer.lang;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import static ring.serializer.token.STokenType.endOfInput;
import static ring.serializer.token.STokenType.primitiveValue;

import java.io.IOException;
import java.nio.CharBuffer;
import java.util.Objects;

import ring.io.CharChannel;
import ring.io.STokenChannel;
import ring.serializer.Language;
import ring.serializer.token.SToken;
import ring.util.ErrorMessages;

abstract class AbstractCharChannel<T> implements STokenChannel {
	static final ErrorMessages errors = new ErrorMessages();
	final AbstractCharLanguage<T> lang;
	final CharChannel ch;
	final CharBuffer rbuf = CharBuffer.allocate(1).position(1);
	/**previous written token*/ private SToken wtok;
	private final CharBuffer wbuf = CharBuffer.allocate(1).position(1);
	
	AbstractCharChannel(AbstractCharLanguage<T> lang, CharChannel ch) {
		this.lang = lang; this.ch = Objects.requireNonNull(ch);
	}
	
	@Override public final Language language() { return lang; }
	
	@Override public final boolean canRead() { return ch.canRead(); }
	
	@Override public final boolean canWrite() { return ch.canWrite(); }
	
	@Override
	public final void write(SToken tok) throws IOException {
		tok.checkValid();
		if(wtok == null) wtok = tok;
		else wtok = write(wtok,tok) ? null : tok;
		if(tok.type == endOfInput) { wtok = null; checkEOI(); writeNewLine(); }
	}
	
	/**Writes the given token (with respect to the given lookahead).
	 * @return True if the lookahead should be consumed.*/
	abstract boolean write(SToken tok, SToken la) throws IOException;
	
	final void readStrDirect(String expected, int offset) throws IOException {
		for(int i = offset; i < expected.length(); i++) {
			char c = readChar();
			if(c == expected.charAt(i)) continue;
			throw new IllegalArgumentException("expected primitive value "+expected+", got "+expected.substring(0,i)+c);
		}
	}
	
	final void writeStrDirect(String str) throws IOException {
		for(int i = 0; i < str.length(); i++) write(str.charAt(i));
	}
	
	final char checkRbuf(char expected) {
		var c = rbuf.get(0);
		if(c == expected) return expected;
		throw new IllegalArgumentException("expected "+expected+", got "+c);
	}
	
	final char readChar() throws IOException {
		if(readCharOptional()) return rbuf.get(0);
		throw new IllegalArgumentException(errors.unexpectedEoi);
	}
	
	final boolean readCharOptional() throws IOException { return ch.read(rbuf.position(0)) > 0; }
	
	final void write(char c) throws IOException { wbuf.array()[0] = c; ch.write(wbuf.position(0)); }
	
	@Override
	public void flush() throws IOException {
		if(wtok != null) throw new IllegalStateException();
		ch.flush();
	}
	
	@Override public final void close() throws IOException { flush(); ch.close(); }
	
//	---------- High-Level Functions ----------
	
	void checkEOI() {}
	
	void writeNewLine() throws IOException { write('\n'); }
	
	final char readWhitespace() throws IOException {
		if(!skipWhitespace()) throw new IllegalArgumentException(errors.unexpectedEoi);
		return rbuf.get(0);
	}
	
	final boolean skipWhitespace() throws IOException {
		var hasChar = rbuf.hasRemaining();
		if(!hasChar && !readCharOptional()) return false;
		char c = rbuf.get(0);
		while(true) {
			if(!Character.isWhitespace(c)) break;
			skipSpecialWhitespace(c);
			if(!readCharOptional()) return false;
			else c = rbuf.get(0);
		}
		rbuf.position(1); return true;
	}
	
	void skipSpecialWhitespace(char startChar) throws IOException {}
	
	SToken readPrimValue() throws IOException {
		switch(rbuf.get(0)) {
		case '\"': return SToken.createPrimitiveValue(readStr('\"'));
		case 'n': readStrDirect("null",1); return SToken.createPrimitiveValue(null);
		case 't': readStrDirect("true",1); return SToken.createPrimitiveValue(true);
		case 'f': readStrDirect("false",1); return SToken.createPrimitiveValue(false);
		default: return readNumber();
		}
	}
	
	void writePrimValue(SToken tok, boolean quoteString) throws IOException {
		errors.checkToken(primitiveValue,tok);
		var v = tok.primitiveValue;
		if(v == null) writeStrDirect("null");
		else {
			var cls = v.getClass();
			if(cls == Boolean.class) writeStrDirect((boolean)v?"true":"false");
			else if(cls == Long.class) writeStrDirect(Long.toString((Long)v));
			else if(cls == Double.class) writeStrDirect(Double.toString((Double)v));
			else if(cls == String.class) writeStr((String)v,quoteString);
			else throw new IllegalArgumentException(errors.invalidPrimType(tok));
		}
	}
	
	SToken readNumber() throws IOException {
		var b = new StringBuilder(); var hasDot = false;
		while(true) {
			var c = rbuf.get(0);
			switch(c) {
			case '.': hasDot = true;
			case '-': case 'e': case 'E':
			case '0': case '1': case '2': case '3': case '4':
			case '5': case '6': case '7': case '8': case '9':
				b.append(c); readChar(); break;
			default: //lookahead char is present
				rbuf.position(0); var str = b.toString();
				if(str.isEmpty()) throw new IllegalArgumentException("empty number constant");
				return hasDot ? SToken.createPrimitiveValue(Double.parseDouble(str)) :
					SToken.createPrimitiveValue(Long.parseLong(str));
			}
		}
	}
	
	String readStr(char endChar) throws IOException {
		var b = new StringBuilder();
		while(true) {
			char c = readChar();
			if(c == endChar) return b.toString();
			else if(c == '\\') {
				switch(c = readChar()) {
				case '\"': case '\\': case '/': b.append(c); break;
				case 'b': b.append('\b'); break;
				case 'f': b.append('\f'); break;
				case 'n': b.append('\n'); break;
				case 'r': b.append('\r'); break;
				case 't': b.append('\t'); break;
				case 'u': b.appendCodePoint(readUnicode()); break;
				}
			}
			else b.append(c);
		}
	}
	
	void writeStr(String str, boolean quoted) throws IOException {
		if(quoted) write('\"');
		for(int i = 0; i < str.length(); i++) {
			var c = str.charAt(i);
			switch(c) {
			case '\"': case '\\': write('\\'); write(c); break;
			case '\b': writeStrDirect("\\b"); break;
			case '\f': writeStrDirect("\\f"); break;
			case '\n': writeStrDirect("\\n"); break;
			case '\r': writeStrDirect("\\r"); break;
			case '\t': writeStrDirect("\\t"); break;
			default:
				if(c <= 0x1f || Character.isSurrogate(c)) writeUnicode(c);
				else write(c);
				break;
			}
		}
		if(quoted) write('\"');
	}
	
	int readUnicode() throws IOException {
		var buf = new char[4];
		for(int i = 0; i < 4; i++) buf[i] = readChar();
		return Integer.parseInt(String.valueOf(buf),16);
	}
	
	void writeUnicode(int pt) throws IOException {
		write('\\'); write('u');
		var str = String.format("%04x",pt);
		for(int i = 0; i < str.length(); i++) write(str.charAt(i));
	}
}
