package ring.serializer.lang;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import ring.io.ByteChannel;
import ring.io.STokenChannel;
import ring.serializer.ByteLanguage;

/**Just a basic binary serialization.*/
public final class BinaryLanguage extends ByteLanguage {
	@Override public STokenChannel createChannel(ByteChannel ch) { return new BinaryChannel(this,ch); }
	
	@Override public int hashCode() { return getClass().hashCode(); }
	
	@Override public boolean equals(Object obj) { return obj != null && getClass() == obj.getClass(); }
}
