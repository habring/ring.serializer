package ring.serializer.lang;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.util.Objects;

class SpacingConfig {
	static final String defaultNewLine = "\n", defaultIndent = "  ";
	final String newLine, indent;
	
	SpacingConfig(boolean indent) { this(indent?defaultNewLine:"",indent?defaultIndent:""); }
	
	SpacingConfig(String newLine, String indent) {
		this.newLine = newLine; this.indent = indent;
		for(int i = 0; i < newLine.length(); i++) {
			var c = newLine.charAt(i);
			if(c != '\n' && c != '\r') throw new IllegalArgumentException("invalid newline");
		}
		for(int i = 0; i < indent.length(); i++) {
			var c = indent.charAt(i);
			if(c != ' ' && c != '\t') throw new IllegalArgumentException("invalid indent");
		}
	}
	
	boolean isHumanReadable() { return !newLine.isEmpty() || !indent.isEmpty(); }
	
	@Override public int hashCode() { return newLine.hashCode() ^ indent.hashCode(); }
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null || getClass() != obj.getClass()) return false;
		var c = (SpacingConfig)obj; return newLine.equals(c.newLine) && indent.equals(c.indent);
	}
	
	static class EscSpacingConfig extends SpacingConfig {
		final Escaping escaping;
		
		EscSpacingConfig(Escaping escaping, boolean indent) {
			super(indent); this.escaping = Objects.requireNonNull(escaping);
		}
		
		EscSpacingConfig(Escaping escaping, String newLine, String indent) {
			super(newLine,indent); this.escaping = Objects.requireNonNull(escaping);
		}
		
		@Override public int hashCode() { return super.hashCode() ^ escaping.hashCode(); }
		
		@Override public boolean equals(Object obj) { return super.equals(obj) && escaping == ((EscSpacingConfig)obj).escaping; }
	}
}
