package ring.serializer.lang;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import static ring.serializer.token.STokenType.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import ring.io.CharChannel;
import ring.serializer.lang.ConfigLanguage.Config;
import ring.serializer.token.SToken;

class ConfigChannel extends AbstractCharIndentedChannel<Config> {
	private final LinkedList<SToken> rnxt = new LinkedList<>();
	private final List<Elem> path = new ArrayList<>();
	private boolean wFirst;
	
	private static class Elem {
		final String name;
		ElemType type = ElemType.normal;
		boolean tabMarker; int listIdx = -1;
		Elem(String name) { this.name = name; }
		@Override public String toString() { return name != null ? name : "["+listIdx+"]"; }
	}
	
	private static enum ElemType { normal, table, list, listElem, plist }
	
	ConfigChannel(ConfigLanguage lang, CharChannel ch) { super(lang,ch); }
	
	@Override
	public SToken read() throws IOException {
		if(!rnxt.isEmpty()) return rnxt.removeLast();
		if(!skipWhitespace()) {
			if(!path.isEmpty()) { rPopAll(-1); return read(); }
			return null;
		}
		if(path.isEmpty()) { push("<root>"); }
		if(readTableOrKey()) return read();
		readValue(); return read();
	}
	
	@Override
	boolean write(SToken tok, SToken la) throws IOException {
		if(path.isEmpty()) { push("<root>"); wFirst = true; }
		switch(tok.type) {
		case startObject:
			if(la.type == endObject) {
				writeTable();
				int i = indexOfTable(path.size(),true);
				if(i != path.size()-1) { writeKey(); writeStrDirect("{}"); }
				break;
			}
			if(peek().type != ElemType.listElem) {
				setType(ElemType.table);
				if(path.size() == 1) peek().tabMarker = true;
			}
			break;
		case fieldName: push(tok.fieldName); break;
		case fieldSeperator: break;
		case endObject:
			if(path.size() == 1) pop();
			break;
		case startList:
			if(la.type == primitiveValue || la.type == endList) {
				writeTable(); writeKey(); writeStrDirect("[");
				setType(ElemType.plist);
			}
			else { setType(ElemType.list); pushListElem(0); }
			break;
		case endList:
			if(peek().type == ElemType.plist) { writeStrDirect("]"); pop(); }
			else { pop(); pop(); }
			break;
		case elemSeperator:
			if(peek().type == ElemType.plist) writeStrDirect(lang.config.isHumanReadable()?", ":",");
			else pushListElem(pop().listIdx+1);
			break;
		case primitiveValue:
			if(path.size() <= 1) throw new IllegalArgumentException("a primitive value cannot be the root element");
			if(peek().type == ElemType.plist) writePrimValue(tok,valueNeedsQuoting(tok.primitiveValue,true));
			else { writeTable(); writeKey(); writePrimValue(tok,valueNeedsQuoting(tok.primitiveValue,false)); pop(); }
			break;
		default: throw new Error(errors.unreachable);
		}
		return false;
	}
	
	private boolean readTableOrKey() throws IOException {
		var firstName = readName(true);
		var isTable = firstName.tabMarker;
		firstName.tabMarker = false;
		int i = isTable ? 0 : Math.max(0,indexOfTable(path.size(),true));
		if(isTable && peek().tabMarker) { //empty table == empty object
			rnxt.addFirst(new SToken(startObject));
			rnxt.addFirst(new SToken(endObject));
		}
		replaceName(i,firstName);
		for(i++; ; i++) {
			rbuf.position(0); readWhitespace();
			var c = rbuf.get(0);
			if(c == '.') readChar();
			else if(c == (isTable?']':'=')) break;
			rbuf.position(0); readWhitespace();
			replaceName(i,readName(false));
		}
		if(isTable) {
			checkRbuf(']');
			for(int j = i+1; j < path.size(); j++) path.get(j).tabMarker = false;
			path.get(i).tabMarker = true;
		}
		else { checkRbuf('='); readWhitespace(); }
		return isTable;
	}
	
	private void replaceName(int i, Elem newElem) {
		if(nameEquals(i+1,newElem)) return;
		var wasPopped = path.size() > i+1;
		var oldElem = wasPopped ? path.get(i+1) : null;
		rPopAll(i);
		switch(newElem.type) {
		case normal:
			rnxt.addFirst(new SToken(wasPopped?fieldSeperator:startObject));
			rnxt.addFirst(SToken.createFieldName(newElem.name));
			break;
		case listElem:
			if(wasPopped ? oldElem.listIdx+1 != newElem.listIdx : newElem.listIdx != 0) {
				throw new IllegalArgumentException("list indices out of order");
			}
			rnxt.addFirst(new SToken(wasPopped?elemSeperator:startList));
			break;
		default: throw new Error(errors.unreachable);
		}
		path.add(newElem);
	}
	
	private void rPopAll(int i) {
		while(path.size() > i+1) {
			var e = pop();
			if(path.size() <= i+1) break;
			switch(e.type) {
			case normal: rnxt.addFirst(new SToken(endObject)); break;
			case listElem: rnxt.addFirst(new SToken(endList)); break;
			default: throw new Error(errors.unreachable);
			}
		}
	}
	
	private void readValue() throws IOException {
		switch(rbuf.get(0)) {
		case '{':
			readWhitespace(); checkRbuf('}');
			rnxt.addFirst(new SToken(startObject));
			rnxt.addFirst(new SToken(endObject));
			return;
		case '[':
			rnxt.addFirst(new SToken(startList));
			for(var isFirst = true; ; isFirst = false) {
				if(readWhitespace() == ']') break;
				if(!isFirst) rnxt.addFirst(new SToken(elemSeperator));
				rnxt.addFirst(readPrimValue0(',',']'));
				if(rbuf.get(0) == ']') break;
			}
			rnxt.addFirst(new SToken(endList));
			return;
		default: rnxt.addFirst(readPrimValue0('#','#')); return;
		}
	}
	
	private SToken readPrimValue0(char endChar1, char endChar2) throws IOException {
		if(lang.config.quoteStrings || rbuf.get(0) == '\"') return readPrimValue();
		var b = new StringBuilder();
		var c = rbuf.get(0);
		while(c != '\n' && c != '\r' && c != endChar1 && c != endChar2) {
			b.append(c);
			if(!readCharOptional()) break;
			c = rbuf.get(0);
		}
		return SToken.createPrimitiveValue(b.toString());
	}
	
	private void writeTable() throws IOException {
		int i = indexOfTable(path.size(),false);
		if(i < 0) throw new Error(errors.unreachable);
		var e = path.get(i);
		if(e.tabMarker) return;
		switch(lang.config.tabPolicy) {
		case all: break;
		case none: return;
		case rootOnly:
			if(i <= 1) break;
			else return;
		default: throw new Error(errors.unreachable);
		}
		for(int j = 0; j < i; j++) path.get(j).tabMarker = false;
		int indent = 0;
		for(int j = 1; j < path.size(); j++) {
			switch(path.get(j).type) {
			case table: case list: indent++; break;
			default: break;
			}
		}
		setIndent(indent);
		if(!wFirst && lang.config.isHumanReadable()) writeNewLine();
		writeStart(); write('['); writeKey(0,i+1); write(']');
		e.tabMarker = true; return;
	}
	
	private void writeKey() throws IOException {
		writeStart(); writeKey(indexOfTable(path.size(),true)+1,path.size());
		writeStrDirect(lang.config.isHumanReadable()?" = ":"=");
	}
	
	private void writeStart() throws IOException {
		if(!wFirst) writeNewLine();
		wFirst = false;
	}
	
	private void writeKey(int from, int to) throws IOException {
		if(from == 0) from++;
		if(from >= to) return;
		for(int i = from; i < to; i++) {
			writeName(path.get(i),i==from);
		}
	}
	
	private int indexOfTable(int len, boolean writtenOnly) {
		for(int i = len-1; i >= 0; i--) {
			var e = path.get(i);
			if(writtenOnly && !e.tabMarker) continue;
			if(e.type != ElemType.normal) return i;
		}
		return -1;
	}
	
	private Elem readName(boolean isFirst) throws IOException {
		var c = rbuf.get(0);
		var isTable = false;
		if(c == '[') {
			c = readWhitespace();
			if('0' <= c && c <= '9') return readListElem();
			else if(!isFirst) throw new IllegalArgumentException("invalid key");
			else if(c == '[') { readChar(); var ret = readListElem(); ret.tabMarker = true; return ret; }
			else isTable = true;
		}
		Elem ret;
		if(c == '\"') ret = new Elem(readStr('\"'));
		else {
			var b = new StringBuilder();
			while(!nameNeedsQuoting(c,b.length())) {
				b.append(c); c = readChar();
			}
			if(b.length() == 0) throw new IllegalArgumentException("empty key");
			ret = new Elem(b.toString()); rbuf.position(0);
		}
		ret.tabMarker = isTable; return ret;
	}
	
	private Elem readListElem() throws IOException {
		var c = rbuf.get(0);
		var b = new StringBuilder();
		while(c != ']') { b.append(c); c = readChar(); }
		readChar(); var ret = new Elem(null);
		ret.type = ElemType.listElem; ret.listIdx = Integer.parseInt(b.toString());
		return ret;
	}
	
	private boolean nameEquals(int i, Elem readElem) {
		if(i >= path.size()) return false;
		var e = path.get(i);
		if(e.listIdx != readElem.listIdx) return false;
		return Objects.equals(e.name,readElem.name);
	}
	
	private void writeName(Elem name, boolean isFirst) throws IOException {
		if(name.type == ElemType.listElem) {
			if(name.listIdx < 0) throw new Error(errors.unreachable);
			write('['); writeStrDirect(Integer.toString(name.listIdx)); write(']');
		}
		else {
			if(!isFirst) write('.');
			writeStr(name.name,nameNeedsQuoting(name));
		}
	}
	
	private void pushListElem(int i) {
		if(i < 0) throw new Error(errors.unreachable);
		var e = new Elem(null); path.add(e);
		e.type = ElemType.listElem; e.listIdx = i;
	}
	
	private void push(String name) { path.add(new Elem(Objects.requireNonNull(name))); }
	
	private void setType(ElemType type) {
		var e = peek();
		if(e.type != ElemType.normal || type == ElemType.normal) {
			throw new Error(errors.unreachable);
		}
		e.type = type;
	}
	
	private Elem peek() { return path.get(path.size()-1); }
	
	private Elem pop() { return path.remove(path.size()-1); }
	
	@Override
	void checkEOI() {
		setIndent(0); super.checkEOI();
		if(!path.isEmpty()) throw new IllegalArgumentException(errors.unexpectedEoi);
	}
	
	@Override public void flush() throws IOException { super.flush(); path.clear(); }
	
	@Override
	void skipSpecialWhitespace(char startChar) throws IOException {
		if(startChar != '#') return;
		while(readCharOptional()) {
			var c = rbuf.get(0);
			if(c == '\n' || c == '\r') break;
		}
	}
	
	private static boolean nameNeedsQuoting(Elem name) {
		if(name.type == ElemType.listElem) return false;
		for(int i = 0; i < name.name.length(); i++) {
			if(nameNeedsQuoting(name.name.charAt(i),i)) return true;
		}
		return name.name.isEmpty();
	}
	
	private static boolean nameNeedsQuoting(char c, int i) {
		if('a' <= c && c <= 'z') return false;
		if('A' <= c && c <= 'Z') return false;
		if('0' <= c && c <= '9' && i != 0) return false;
		if(c == '_' || c == '-') return false;
		return true;
	}
	
	private boolean valueNeedsQuoting(Object value, boolean inPList) {
		if(value == null || value.getClass() != String.class) return false;
		if(lang.config.quoteStrings) return true;
		var str = (String)value;
		if(str.isEmpty()) return false;
		if(Character.isWhitespace(str.charAt(0)) || Character.isWhitespace(str.charAt(str.length()-1))) return true;
		if(str.charAt(0) == '{' || str.charAt(0) == '[') return true;
		if(inPList) {
			for(int i = 0; i < str.length(); i++) {
				switch(str.charAt(i)) {
				case '[': case ']': case ',': return true;
				}
			}
		}
		return false;
	}
}
