package ring.serializer.lang;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import static ring.serializer.lang.SpacingConfig.defaultIndent;
import static ring.serializer.lang.SpacingConfig.defaultNewLine;

import java.util.Objects;

import ring.io.CharChannel;
import ring.io.STokenChannel;
import ring.serializer.lang.ConfigLanguage.Config;
import ring.serializer.token.SToken;

/**This language is based on the toml specification (similar to ini-files).<br>
 * Note that some toml features (like multi-line strings or inline tables) are not implemented for simplicity reasons.<br>
 * In contrast to toml, strings without quotes are ok. During serialization, strings are put in quotes depending on the escaping.<br>
 * Ordering of keys is more strict than in toml, because of the way {@link SToken} streams work.<br>
 * In contrast to toml, array elements look like they look in java.<br>
 * https://github.com/toml-lang/toml */
public final class ConfigLanguage extends AbstractCharLanguage<Config> {
	/**Configures when to use tables and when to use normal keys.*/
	public static enum ConfigTablePolicy {
		/**no tables*/ none,
		/**only the (complex) fields of the root element are tables*/ rootOnly,
		/**every complex data type is a table*/ all;
		public static final ConfigTablePolicy defaultPolicy = rootOnly;
	}
	
	public ConfigLanguage() { this(false,true); }
	
	public ConfigLanguage(boolean quoteStrings, boolean indent) { this(ConfigTablePolicy.defaultPolicy,quoteStrings,indent); }
	
	public ConfigLanguage(boolean quoteStrings, String newLine, String indent) { this(ConfigTablePolicy.defaultPolicy,quoteStrings,newLine,indent); }
	
	public ConfigLanguage(ConfigTablePolicy tabPolicy) { this(tabPolicy,false,true); }
	
	public ConfigLanguage(ConfigTablePolicy tabPolicy, boolean quoteStrings, boolean indent) { this(new Config(tabPolicy,quoteStrings,defaultNewLine,indent?defaultIndent:"")); }
	
	public ConfigLanguage(ConfigTablePolicy tabPolicy, boolean quoteStrings, String newLine, String indent) { this(new Config(tabPolicy,quoteStrings,newLine,indent)); }
	
	private ConfigLanguage(Config config) { super(config,config.quoteStrings?new LanguageFeature[0]:new LanguageFeature[] { fixupStrings }); }
	
	@Override public STokenChannel createChannel(CharChannel ch) { return new ConfigChannel(this,ch); }
	
	static class Config extends SpacingConfig {
		final ConfigTablePolicy tabPolicy;
		final boolean quoteStrings;
		
		Config(ConfigTablePolicy tabPolicy, boolean quoteStrings, String newLine, String indent) {
			super(newLine,indent); this.tabPolicy = Objects.requireNonNull(tabPolicy); this.quoteStrings = quoteStrings;
			if(newLine.isEmpty()) throw new IllegalArgumentException("newline is empty");
		}
	}
}
