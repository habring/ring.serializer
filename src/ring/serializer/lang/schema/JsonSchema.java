package ring.serializer.lang.schema;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Type;
import java.util.*;
import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.Map.Entry;

import ring.serializer.Serializer;
import ring.serializer.SerializerConfig;
import ring.serializer.annot.SField;
import ring.serializer.factory.CollectionTypeDescriptorFactory;
import ring.serializer.factory.ReflectTypeDescriptorFactory;
import ring.serializer.lang.JsonLanguage;
import ring.serializer.types.FieldDescriptor;
import ring.util.ErrorMessages;
import ring.util.ReflectionUtil;

/**Generates a json schema for one (or multiple) given types.<br>
 * https://json-schema.org/understanding-json-schema*/
public final class JsonSchema {
	private static final ErrorMessages errors = new ErrorMessages();
	private static final ReflectionUtil reflect = new ReflectionUtil();
	final Serializer schemaSerializer;
	private final Serializer objSerializer;
	/**Prevents the user from generating the schema via normal serialization.*/
	private final JsonSchemaValue data = new JsonSchemaValue();
	
	public JsonSchema() { this(new JsonLanguage(),new SerializerConfig()); }
	
	public JsonSchema(JsonLanguage language, SerializerConfig objSerialization) {
		var config = new SerializerConfig().setLanguage(Objects.requireNonNull(language));
		config.factories.add(0,new ReflectTypeDescriptorFactory<>(Object.class,false));
		config.factories.add(0,new CollectionTypeDescriptorFactory(false));
		this.schemaSerializer = new Serializer(config); this.objSerializer = createSerializer(objSerialization);
		data.allSerializers.put(objSerializer,0);
	}
	
	private Serializer createSerializer(SerializerConfig config) {
		return new Serializer(new SerializerConfig(config).setLanguage(schemaSerializer.language()));
	}
	
	public void add(Class<?> cls) { getDefinition(cls,objSerializer); }
	
	public Class<?> rootDefinition() { return data.rootDefinition != null ? (Class<?>)data.rootDefinition.type : null; }
	
	public void setRootDefinition(Class<?> cls) { data.rootDefinition = getDefinition(cls,objSerializer); }
	
	JsonSchemaDefinition getDefinition(Type t, SerializerConfig config) { return getDefinition(t,createSerializer(config)); }
	
	JsonSchemaDefinition getDefinition(Type t, Serializer serializer) {
		var primType = reflect.primitiveMapInv.get(t);
		if(primType != null) return getDefinition(primType,serializer);
		int serializerKey = data.allSerializers.computeIfAbsent(serializer,k->data.allSerializers.size());
		var key = new SimpleImmutableEntry<>(t,serializer);
		var ret = data.defByType.get(key);
		if(ret != null) return ret;
		ret = setName(new JsonSchemaDefinition(this,t,null,serializer),serializerKey);
		if(data.defByName.containsKey(ret.name())) {
			throw new IllegalArgumentException(String.format("two types have the same name %s (%s and %s)",ret.name(),ret.type,data.defByName.get(ret.name()).type));
		}
		data.defByType.put(key,ret); data.defByName.put(ret.name(),ret);
		ret.build(); return ret;
	}
	
	JsonSchemaDefinition getDefinition(FieldDescriptor extTyped, Serializer serializer) {
		if(!extTyped.isExternalDynamicTyped()) throw new Error(errors.unreachable);
		var ret = new JsonSchemaDefinition(this,Object.class,"externalTyped:"+extTyped.prev().name+":"+extTyped.name,serializer);
		setName(ret,data.allSerializers.get(serializer));
		var ret2 = data.defByName.computeIfAbsent(ret.name(),k->ret);
		if(ret == ret2) ret.buildExtTyped(extTyped);
		return ret2;
	}
	
	private JsonSchemaDefinition setName(JsonSchemaDefinition def, int serializerKey) {
		if(serializerKey > 0) def.setName("Serializer"+serializerKey+":"+def.name());
		return def;
	}
	
	@Override
	public String toString() {
		try(var out = new StringWriter();
				var tmp = schemaSerializer.charLanguage().createChannel(out)) {
			schemaSerializer.serialize(JsonSchemaValue.class,data,tmp);
			return out.toString();
		} catch(IOException exc) { throw new Error(errors.unreachable,exc); }
	}
	
	private static class JsonSchemaValue {
		final Map<Entry<Type,Serializer>,JsonSchemaDefinition> defByType = new HashMap<>();
		final Map<String,JsonSchemaDefinition> defByName = new HashMap<>();
		final Map<Serializer,Integer> allSerializers = new HashMap<>();
		JsonSchemaDefinition rootDefinition;
		
		@SField("$schema")
		String schemaUrl() { return "https://json-schema.org/draft/2019-09/schema"; }
		
		@SField("$ref")
		String rootRef() { return rootDefinition != null ? rootDefinition.reference() : null; }
		
		@SField(JsonSchemaDefinition.key)
		Map<String,JsonSchemaDefinition> definitions() {
			var l = new ArrayList<>(defByName.entrySet());
			l.sort((a,b)->a.getKey().compareToIgnoreCase(b.getKey()));
			var m = new LinkedHashMap<String,JsonSchemaDefinition>();
			for(var e : l) m.put(e.getKey(),e.getValue());
			return Collections.unmodifiableMap(m);
		}
	}
}
