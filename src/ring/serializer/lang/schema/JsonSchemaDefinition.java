package ring.serializer.lang.schema;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.lang.reflect.Type;
import java.util.*;
import java.util.AbstractMap.SimpleEntry;
import java.util.Map.Entry;

import ring.serializer.Serializer;
import ring.serializer.annot.SClass;
import ring.serializer.annot.SField;
import ring.serializer.token.SObject;
import ring.serializer.types.*;
import ring.util.ErrorMessages;

class JsonSchemaDefinition {
	private static final ErrorMessages errors = new ErrorMessages();
	public static final String key = "$defs";
	private final JsonSchema owner;
	public final Type type;
	private final Serializer objSerializer;
	private String name;
	@SField(replaceParent=true) private SerializableValue serializableValue;
	
	@SuppressWarnings("unused") private JsonSchemaDefinition() { owner = null; type = null; objSerializer = null; }
	
	public JsonSchemaDefinition(JsonSchema owner, Type type, String name, Serializer objSerializer) {
		this.owner = Objects.requireNonNull(owner); this.type = Objects.requireNonNull(type);
		this.objSerializer = Objects.requireNonNull(objSerializer);
		setName(name);
	}
	
	public String reference() { return "#/"+key+"/"+name; }
	
	public String name() { return name; }
	
	public void setName(String name) {
		this.name = validateName(name!=null?name:objSerializer.getDescriptor(type).toString());
	}
	
	public void build() {
		if(serializableValue != null) throw new IllegalStateException();
		var type = objSerializer.getDescriptor(this.type);
		switch(type.kind()) {
		case primitive: serializableValue = new PrimitiveValue((PrimitiveTypeDescriptor)type); break;
		case list: serializableValue = new ListValue(this,(ListTypeDescriptor)type); break;
		case map: serializableValue = new MapValue(this,(MapTypeDescriptor)type); break;
		case convert: serializableValue = new ConvertValue(this,(ConvertTypeDescriptor)type); break;
		case object: serializableValue = ObjectValue.create(this,(ObjectTypeDescriptor)type); break;
		case sobject: serializableValue = new SerializableValue();
		default: throw new Error(errors.unreachable);
		}
	}
	
	public void buildExtTyped(FieldDescriptor f) {
		if(serializableValue != null) throw new IllegalStateException();
		serializableValue = ExtTypedValue.create(this,f);
	}
	
	private static String validateName(String s) {
		if(s.isEmpty()) throw new IllegalArgumentException("empty name");
		var b = new StringBuilder();
		if(!isValidNameStartChar(s.charAt(0))) b.append('z');
		for(int i = 0; i < s.length(); i++) {
			if(isValidNameChar(s.charAt(i))) b.append(s.charAt(i));
			else b.append('_');
		}
		return b.toString();
	}
	
	private static boolean isValidNameChar(char c) {
		if(isValidNameStartChar(c) || ('0' <= c && c <= '9')) return true;
		switch(c) {
		case '-': case '_': case ':': case '.': return true;
		}
		return false;
	}
	
	private static boolean isValidNameStartChar(char c) {
		return ('A' <= c && c <= 'Z') || ('a' <= c && c <= 'z');
	}
	
	@SClass(disableDynamicTypesafety=true) private static class SerializableValue {}
	
	private static class RefValue extends SerializableValue {
		final JsonSchemaDefinition value;
		RefValue() { value = null; }
		RefValue(JsonSchemaDefinition value) { this.value = Objects.requireNonNull(value); }
		@SField("$ref") String reference() { return value == null ? null : value.reference(); }
	}
	
	private static class BoolValue extends SerializableValue {
		@SField(replaceParent=true) final Boolean value;
		@SuppressWarnings("unused") BoolValue() { value = null; }
		BoolValue(boolean value) { this.value = value; }
	}
	
	private static class IfElse extends SerializableValue {
		@SField("if") final SerializableValue if_;
		@SField("then") final SerializableValue then_;
		@SField("else") final SerializableValue else_;
		@SuppressWarnings("unused") IfElse() { if_ = then_ = else_ = null; }
		IfElse(SerializableValue if_, SerializableValue then_, SerializableValue else_) {
			this.if_ = Objects.requireNonNull(if_); this.then_ = Objects.requireNonNull(then_); this.else_ = Objects.requireNonNull(else_);
		}
	}
	
	private static class AnyOf extends SerializableValue {
		@SField final List<SerializableValue> anyOf;
		AnyOf() { anyOf = null; }
		AnyOf(List<SerializableValue> anyOf) { this.anyOf = Objects.requireNonNull(anyOf); }
	}
	
	private static class AllOf extends SerializableValue {
		@SField final List<SerializableValue> allOf;
		@SuppressWarnings("unused") AllOf() { allOf = null; }
		AllOf(List<SerializableValue> allOf) { this.allOf = Objects.requireNonNull(allOf); }
	}
	
	private static class OneOf extends SerializableValue {
		@SField final List<SerializableValue> oneOf;
		@SuppressWarnings("unused") OneOf() { oneOf = null; }
		OneOf(List<SerializableValue> oneOf) { this.oneOf = Objects.requireNonNull(oneOf); }
	}
	
	private static class Not extends SerializableValue {
		@SField final SerializableValue not;
		@SuppressWarnings("unused") Not() { not = null; }
		Not(SerializableValue not) { this.not = Objects.requireNonNull(not); }
	}
	
	private static abstract class BasicTypedValue extends SerializableValue {
		@SField final List<String> type;
		BasicTypedValue() { type = null; }
		BasicTypedValue(Collection<String> types) {
			var s = new HashSet<>(types); s.add("null"); var l = new ArrayList<>(s); l.sort(null);
			this.type = l;
			if(this.type.size() <= 1) throw new Error(errors.unreachable);
		}
	}
	
	private static class PrimitiveValue extends BasicTypedValue {
		@SuppressWarnings("unused") PrimitiveValue() {}
		PrimitiveValue(PrimitiveTypeDescriptor type) {
			super(Collections.singleton(getType(type.type)));
		}
		static String getType(Class<?> cls) {
			if(cls == Boolean.class) return "boolean";
			else if(cls == Long.class || cls == Double.class) return "number";
			else if(cls == String.class) return "string";
			else throw new IllegalArgumentException(errors.invalidPrimType(cls));
		}
	}
	
	private static class ListValue extends AnyOf {
		@SuppressWarnings("unused") ListValue() {}
		/**{@link ListTypeDescriptor#simplifySingleElements()} is ignored here for simplicity.*/
		ListValue(JsonSchemaDefinition owner, ListTypeDescriptor type) { super(toList(owner,type)); }
		private static List<SerializableValue> toList(JsonSchemaDefinition owner, ListTypeDescriptor type) {
			var elemType = owner.owner.getDefinition(type.elementType,owner.objSerializer);
			return Arrays.asList(new RefValue(elemType),new ArrayValue(elemType));
		}
		private static class ArrayValue extends SerializableValue {
			@SField final String type = "array";
			@SField final RefValue items;
			@SuppressWarnings("unused") ArrayValue() { items = null; }
			ArrayValue(JsonSchemaDefinition elemType) { this.items = new RefValue(elemType); }
		}
	}
	
	private static class MapValue extends BasicTypedValue {
		@SField final RefValue propertyNames;
		@SField final RefValue additionalProperties;
		@SuppressWarnings("unused") MapValue() { propertyNames = null; additionalProperties = null; }
		MapValue(JsonSchemaDefinition owner, MapTypeDescriptor type) {
			super(Collections.singleton("object"));
			propertyNames = new RefValue(owner.owner.getDefinition(type.keyType,owner.objSerializer));
			additionalProperties = new RefValue(owner.owner.getDefinition(type.valueType,owner.objSerializer));
		}
	}
	
	private static class ConvertValue extends SerializableValue {
		final JsonSchemaDefinition value;
		@SuppressWarnings("unused") ConvertValue() { value = null; }
		ConvertValue(JsonSchemaDefinition owner, ConvertTypeDescriptor type) {
			var config = type.internalSerializer != null ? type.internalSerializer.config() : owner.objSerializer.config();
			this.value = owner.owner.getDefinition(type.internalType,config);
		}
		@SField("$ref") String reference() { return value == null ? null : value.reference(); }
	}
	
	private static class ObjectValue extends BasicTypedValue {
		@SField final Map<String,SerializableValue> properties;
		@SField final boolean additionalProperties = false;
		
		@SuppressWarnings("unused") ObjectValue() { properties = null; }
		ObjectValue(Map<String,SerializableValue> properties) {
			super(Collections.singleton("object")); this.properties = Objects.requireNonNull(properties);
		}
		
		static SerializableValue create(JsonSchemaDefinition owner, ObjectTypeDescriptor type) {
			SerializableValue if_;
			if(type.fieldReplacesParent()) if_ = PropertyValue.create(owner,type.fields().get(0),false);
			else {
				var props = new LinkedHashMap<String,SerializableValue>();
				var extTyped = new ArrayList<FieldDescriptor>();
				for(var f : type.fields()) {
					if(f.isExternalDynamicTyped()) extTyped.add(f);
					props.put(f.name,f.isExternalDynamicTyped()?new SerializableValue():PropertyValue.create(owner,f,true));
				}
				if_ = new ObjectValue(props);
				if(!extTyped.isEmpty()) {
					var allOf = new ArrayList<SerializableValue>(); allOf.add(if_);
					for(var f : extTyped) allOf.add(new RefValue(owner.owner.getDefinition(f,owner.objSerializer)));
					if_ = new AllOf(allOf);
				}
			}
			var subTypes1 = new HashSet<>(owner.objSerializer.getAllKnownSubtypes(type.type));
			var subTypes2 = new ArrayList<SerializableValue>();
			for(var subType : subTypes1) {
				var skip = false;
				for(var p = subType.getSuperclass(); p != type.type; p = p.getSuperclass()) {
					if(subTypes1.contains(p)) { skip = true; break; }
				}
				if(skip) continue;
				var tmp = SubTypeValue.create(owner,subType);
				if(tmp != null) subTypes2.add(tmp);
			}
			if(subTypes2.isEmpty()) return if_;
			else if(subTypes2.size() == 1) return new IfElse(if_,new BoolValue(true),subTypes2.get(0));
			else return new IfElse(if_,new BoolValue(true),new OneOf(subTypes2));
		}
	}
	
	private static class SubTypeValue extends RefValue {
		@SField final List<String> required;
		@SuppressWarnings("unused") SubTypeValue() { required = null; }
		SubTypeValue(JsonSchemaDefinition ref, String required) {
			super(ref); this.required = Arrays.asList(Objects.requireNonNull(required));
		}
		static SubTypeValue create(JsonSchemaDefinition owner, Class<?> subType) {
			var type = (ObjectTypeDescriptor)owner.objSerializer.getDescriptor(subType);
			if(!type.hasDynamicTypeInfo()) return null;
			return new SubTypeValue(owner.owner.getDefinition(subType,owner.objSerializer),type.fields().get(0).name);
		}
	}
	
	private static class ExtTypedValue {
		static SerializableValue create(JsonSchemaDefinition owner, FieldDescriptor f) {
			if(!f.isExternalDynamicTyped()) throw new Error(errors.unreachable);
			var defaultKey = (String)f.prev().getValue(f.parent().createInstance());
			var oneOf = new ArrayList<SerializableValue>();
			var allTypes = new ArrayList<Entry<List<String>,Type>>();
			for(var t : new HashSet<>(owner.objSerializer.getNameToTypeMapping().values())) {
				var names = new ArrayList<>(owner.objSerializer.getNamesForType(t));
				names.sort(String::compareToIgnoreCase);
				allTypes.add(new SimpleEntry<>(names,t));
			}
			allTypes.sort((a,b)->a.getKey().get(0).compareToIgnoreCase(b.getKey().get(0)));
			allTypes.add(0,new SimpleEntry<>(Collections.singletonList(null),null));
			var allTypeNames = new ArrayList<String>();
			SerializableValue defaultKeyValue = null;
			for(var e : allTypes) {
				allTypeNames.addAll(e.getKey());
				var if_ = new SinglePropertyObjectValue(f.prev().name,ConstValue.create(e.getKey()));
				if(e.getValue() == null) oneOf.add(if_);
				else {
					var thenValue = new RefValue(owner.owner.getDefinition(e.getValue(),owner.objSerializer));
					var then_ = new SinglePropertyObjectValue(f.name,thenValue);
					oneOf.add(new IfElse(if_,then_,new BoolValue(false)));
					if(e.getKey().contains(defaultKey) && defaultKey != null) defaultKeyValue = then_;
				}
			}
			allTypeNames.sort((a,b)->a==null?-1:b==null?1:a.compareToIgnoreCase(b));
			var if_ = new SinglePropertyRequiredObjectValue(f.prev().name,ConstValue.create(allTypeNames));
			SerializableValue else_ = new BoolValue(true);
			if(defaultKeyValue != null) else_ = new IfElse(new Not(new RequiredValue(f.prev().name)),defaultKeyValue,else_);
			return new IfElse(if_,new OneOf(oneOf),else_);
		}
		private static class SinglePropertyObjectValue extends SerializableValue {
			@SField final Map<String,SerializableValue> properties;
			SinglePropertyObjectValue() { properties = null; }
			SinglePropertyObjectValue(String name, SerializableValue value) { properties = Map.of(name,value); }
		}
		private static class SinglePropertyRequiredObjectValue extends SinglePropertyObjectValue {
			@SField Set<String> required() { return properties.keySet(); }
			@SuppressWarnings("unused") SinglePropertyRequiredObjectValue() {}
			SinglePropertyRequiredObjectValue(String name, SerializableValue value) { super(name,value); }
		}
		private static class ConstValue extends SerializableValue {
			@SField("const") final String value;
			@SuppressWarnings("unused") ConstValue() { value = null; }
			ConstValue(String value) { this.value = value; }
			static SerializableValue create(List<String> value) {
				return value.size() == 1 ? new ConstValue(value.get(0)) : new EnumValue(value);
			}
			private static class EnumValue extends SerializableValue {
				@SField("enum") final List<String> value;
				@SuppressWarnings("unused") EnumValue() { value = null; }
				EnumValue(List<String> value) { this.value = Objects.requireNonNull(value); }
			}
		}
		private static class RequiredValue extends SerializableValue {
			@SField final List<String> required;
			@SuppressWarnings("unused") RequiredValue() { required = null; }
			RequiredValue(String required) { this.required = Arrays.asList(Objects.requireNonNull(required)); }
		}
	}
	
	private static class PropertyValue extends RefValue {
		final String title;
		PropertyValue() { title = null; }
		PropertyValue(JsonSchemaDefinition owner, FieldDescriptor f) {
			super(owner.owner.getDefinition(f.genericType,owner.objSerializer));
			var tmp = owner.objSerializer.getDescriptor(value.type);
			title = String.format("(%s)%s",tmp,f);
		}
		public String title() { return title; }
		static PropertyValue create(JsonSchemaDefinition owner, FieldDescriptor f, boolean inclTitle) {
			if(!f.canSetValue() && f.genericType == f.type && owner.objSerializer.hasDescriptor(f.type)) {
				return inclTitle ? new TitledConstPropertyValue(owner,f) : new ConstPropertyValue(owner,f);
			}
			return inclTitle ? new TitledPropertyValue(owner,f) : new PropertyValue(owner,f);
		}
		private static class ConstPropertyValue extends PropertyValue {
			@SField("const") final SObject value;
			ConstPropertyValue() { value = null; }
			@SuppressWarnings({ "rawtypes", "unchecked" })
			ConstPropertyValue(JsonSchemaDefinition owner, FieldDescriptor f) {
				super(owner,f);
				var tmp = f.getValue(f.parent().createInstance());
				value = owner.objSerializer.toSObject((Class)f.genericType,tmp);
			}
		}
		private static interface Titled { @SField String title(); }
		private static class TitledPropertyValue extends PropertyValue implements Titled {
			@SuppressWarnings("unused") TitledPropertyValue() {}
			TitledPropertyValue(JsonSchemaDefinition owner, FieldDescriptor f) { super(owner,f); }
		}
		private static class TitledConstPropertyValue extends ConstPropertyValue implements Titled {
			@SuppressWarnings("unused") TitledConstPropertyValue() {}
			TitledConstPropertyValue(JsonSchemaDefinition owner, FieldDescriptor f) { super(owner,f); }
		}
	}
}
