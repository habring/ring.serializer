package ring.serializer.lang;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CodingErrorAction;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

import ring.io.ByteChannel;
import ring.io.STokenChannel;
import ring.serializer.Language;
import ring.serializer.token.SToken;
import ring.serializer.token.STokenType;
import ring.util.ErrorMessages;

class BinaryChannel implements STokenChannel {
	private static final ErrorMessages errors = new ErrorMessages();
	private static final byte nullType = 0, boolType = 1, longType = 2, doubleType = 3, strType = 4;
	private final Language lang;
	private final ByteChannel ch;
	private final ByteBuffer buf = ByteBuffer.allocate(Byte.MAX_VALUE);
	private final CharBuffer cbuf = CharBuffer.allocate(Byte.MAX_VALUE);
	private boolean isReading = true;
	
	BinaryChannel(BinaryLanguage lang, ByteChannel ch) { this.lang = Objects.requireNonNull(lang); this.ch = Objects.requireNonNull(ch); }
	
	@Override public Language language() { return lang; }
	
	private void switchMode() throws IOException {
		if(!isReading) flush();
		isReading = !isReading;
	}
	
	@Override public boolean canRead() { return ch.canRead(); }
	
	@Override
	public SToken read() throws IOException {
		if(!isReading) switchMode();
		buf.position(0).limit(1);
		var arr = buf.array();
		int n = ch.read(buf);
		if(n <= 0) return null;
		var tok = new SToken(STokenType.getById(arr[0]));
		buf.position(0);
		if(tok.type == STokenType.fieldName) tok.fieldName = readString();
		else if(tok.type == STokenType.primitiveValue) {
			arr[0] = -1; n = ch.read(buf); buf.position(0);
			switch(arr[0]) {
			case nullType: break;
			case boolType: tok.primitiveValue = readBool(); break;
			case longType: tok.primitiveValue = readLong(); break;
			case doubleType: tok.primitiveValue = readDouble(); break;
			case strType: tok.primitiveValue = readString(); break;
			default: throw new IOException("invalid subtype "+arr[0]);
			}
		}
		return tok;
	}
	
	private boolean readBool() throws IOException {
		buf.limit(1); int n = ch.read(buf);
		if(n <= 0) throw new IOException("cannot read boolean");
		return buf.array()[0] != 0;
	}
	
	private long readLong() throws IOException {
		var arr = buf.array(); buf.limit(8);
		while(buf.hasRemaining()) {
			int n = ch.read(buf);
			if(n <= 0) throw new IOException("cannot read long");
		}
		long l = 0;
		for(int i = 0; i < 8; i++) {
			l <<= 8; l |= arr[i]&0xff;
		}
		return l;
	}
	
	private double readDouble() throws IOException { return Double.longBitsToDouble(readLong()); }
	
	private String readString() throws IOException {
		var dec = StandardCharsets.UTF_8.newDecoder();
		dec.onMalformedInput(CodingErrorAction.REPORT).onUnmappableCharacter(CodingErrorAction.REPORT);
		var arr = buf.array(); var b = new StringBuilder();
		while(true) {
			buf.position(0).limit(1); cbuf.position(0).limit(cbuf.capacity());
			int n = ch.read(buf); if(n != 1) throw new IOException("cannot read string");
			var end = arr[0] >= 0;
			if(end) buf.limit(arr[0]-1);
			else buf.limit(buf.capacity()-1);
			buf.position(0);
			while(buf.hasRemaining()) {
				n = ch.read(buf);
				if(n <= 0) throw new IOException("cannot read string");
			}
			buf.position(0);
			var result = dec.decode(buf,cbuf,end);
			if(result.isUnderflow()) {}
			else if(result.isOverflow()) throw new Error(errors.unreachable);
			else throw new IOException("invalid encoded string");
			b.append(cbuf.array(),0,cbuf.position());
			if(end) return b.toString();
		}
	}
	
	@Override public boolean canWrite() { return ch.canWrite(); }
	
	@Override
	public void write(SToken tok) throws IOException {
		if(isReading) switchMode();
		tok.checkValid(); buf.position(0);
		var arr = buf.array();
		arr[0] = tok.type.id; int limit = 1;
		if(tok.type == STokenType.fieldName) limit = writeHeadless(tok.fieldName,1);
		else if(tok.type == STokenType.primitiveValue) {
			if(tok.primitiveValue == null) { arr[1] = nullType; limit++; }
			else {
				var cls = tok.primitiveValue.getClass();
				if(cls == Boolean.class) limit = write((boolean)tok.primitiveValue);
				else if(cls == Long.class) limit = write((long)tok.primitiveValue);
				else if(cls == Double.class) limit = write((double)tok.primitiveValue);
				else if(cls == String.class) limit = write((String)tok.primitiveValue);
				else throw new IllegalArgumentException(errors.invalidPrimType(tok));
			}
		}
		buf.limit(limit); writeBuf();
	}
	
	private int write(boolean b) {
		var arr = buf.array(); arr[1] = boolType; arr[2] = (byte)(b ? 1 : 0);
		return 3;
	}
	
	private int write(long l) { return write(longType,l); }
	
	private int write(double d) { return write(doubleType,Double.doubleToLongBits(d)); }
	
	private int write(byte type, long l) {
		var arr = buf.array(); arr[1] = type;
		for(int i = 7; i >= 0; i--) {
			arr[i+2] = (byte)(l&0xff); l >>>= 8;
		}
		return 10;
	}
	
	private int write(String str) throws IOException {
		buf.array()[1] = strType; return writeHeadless(str,2);
	}
	
	private int writeHeadless(String str, int offset) throws IOException {
		buf.limit(offset); writeBuf();
		var enc = StandardCharsets.UTF_8.newEncoder();
		enc.onMalformedInput(CodingErrorAction.REPORT).onUnmappableCharacter(CodingErrorAction.REPORT);
		buf.position(1).limit(buf.capacity());
		for(var chBuf = CharBuffer.wrap(str); ; ) {
			var result = enc.encode(chBuf,buf,true);
			if(result.isUnderflow()) { buf.array()[0] = (byte)buf.position(); return buf.position(); }
			else if(result.isOverflow()) { buf.array()[0] = -1; writeBuf(); buf.position(1); }
			else throw new IllegalArgumentException(str);
		}
	}
	
	private void writeBuf() throws IOException {
		buf.position(0);
		while(buf.remaining() > 0) ch.write(buf);
		buf.position(0);
	}
	
	@Override public void flush() throws IOException { ch.flush(); }
	
	@Override public void close() throws IOException { ch.close(); }
}
