package ring.serializer.lang;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import static ring.serializer.token.STokenType.*;

import java.io.IOException;

import ring.io.CharChannel;
import ring.serializer.token.SToken;

class JsonChannel extends AbstractCharIndentedChannel<SpacingConfig> {
	public JsonChannel(JsonLanguage lang, CharChannel ch) { super(lang,ch); }
	
	@Override
	public SToken read() throws IOException {
		if(!skipWhitespace()) return null;
		var c = rbuf.get(0);
		switch(c) {
		case '{': return new SToken(startObject);
		case '}': return new SToken(endObject);
		case ',': return new SToken(fieldSeperator);
		case '[': return new SToken(startList);
		case ']': return new SToken(endList);
		default:
			if(c == '\"') {
				var str = readStr('\"');
				if(!skipWhitespace()) return SToken.createPrimitiveValue(str);
				else if(rbuf.get(0) == ':') return SToken.createFieldName(str);
				else { rbuf.position(0); return SToken.createPrimitiveValue(str); }
			}
			else return readPrimValue();
		}
	}
	
	@Override
	public boolean write(SToken tok, SToken la) throws IOException {
		switch(tok.type) {
		case startObject: case startList:
			if(la.type == endObject || la.type == endList) { write0(tok); write0(la); return true; }
			else { write0(tok); incIndent(); writeNewLine(); }
			break;
		case fieldSeperator: case elemSeperator: write0(tok); writeNewLine(); break;
		case endObject: case endList: decIndent(); writeNewLine(); write0(tok); break;
		default: write0(tok); break;
		}
		return false;
	}
	
	private void write0(SToken tok) throws IOException {
		switch(tok.type) {
		case startObject: write('{'); break;
		case endObject: write('}'); break;
		case fieldName:
			writeStr(tok.fieldName,true);
			write(':'); if(lang.config.isHumanReadable()) write(' ');
			break;
		case fieldSeperator: case elemSeperator: write(','); break;
		case startList: write('['); break;
		case endList: write(']'); break;
		case primitiveValue: writePrimValue(tok,true); break;
		default: throw new Error(errors.unreachable);
		}
	}
	
	@Override
	public void skipSpecialWhitespace(char startChar) throws IOException {
		if(startChar != '/') return;
		var c = readChar();
		if(c == '/') { //start single line comment
			while(readCharOptional()) {
				c = rbuf.get(0);
				if(c == '\n' || c == '\r') break;
			}
		}
		else if(c == '*') { //start multi line comment
			var foundStar = false;
			while(true) {
				c = readChar();
				if(foundStar && c == '/') break;
				foundStar = c == '*';
			}
		}
		else throw new IllegalArgumentException("invalid start of comment /"+c);
	}
}
