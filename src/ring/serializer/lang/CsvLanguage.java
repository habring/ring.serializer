package ring.serializer.lang;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import ring.io.CharChannel;
import ring.io.STokenChannel;
import ring.serializer.lang.CsvLanguage.CsvConfig;
import ring.serializer.lang.SpacingConfig.EscSpacingConfig;

/**https://en.wikipedia.org/wiki/Comma-separated_values<br>
 * https://tools.ietf.org/html/rfc4180*/
public final class CsvLanguage extends AbstractCharLanguage<CsvConfig> {
	public CsvLanguage() { this(false); }
	
	public CsvLanguage(boolean headless) { this(Escaping.ifNeeded,"\n",headless); }
	
	public CsvLanguage(Escaping escaping, String newLine) { this(escaping,newLine,false); }
	
	public CsvLanguage(Escaping escaping, String newLine, boolean headless) {
		super(new CsvConfig(escaping,newLine,headless),NeedsSkippedFields.instance,fixupStrings);
	}
	
	@Override public STokenChannel createChannel(CharChannel ch) { return new CsvChannel(this,ch); }
	
	static class CsvConfig extends EscSpacingConfig {
		final boolean headless;
		
		CsvConfig(Escaping escaping, String newLine, boolean headless) {
			super(escaping,newLine,""); this.headless = headless;
			for(int i = 0; i < newLine.length(); i++) {
				var c = newLine.charAt(i);
				if(c != '\n' && c != '\r') throw new IllegalArgumentException("invalid newline");
			}
		}
	}
}
