package ring.serializer.lang;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.io.IOException;

import ring.io.CharChannel;

abstract class AbstractCharIndentedChannel<T extends SpacingConfig> extends AbstractCharChannel<T> {
	private int currIndent;
	
	AbstractCharIndentedChannel(AbstractCharLanguage<T> lang, CharChannel ch) { super(lang,ch); }
	
	final void incIndent() { currIndent++; }
	
	final void decIndent() { currIndent = Math.max(0,currIndent-1); }
	
	final void setIndent(int indent) { currIndent = indent; }
	
	@Override
	void checkEOI() {
		if(currIndent != 0) throw new IllegalArgumentException(errors.unexpectedEoi);
	}
	
	@Override
	final void writeNewLine() throws IOException {
		writeStrDirect(lang.config.newLine);
		for(int i = 0; i < currIndent; i++) writeStrDirect(lang.config.indent);
	}
}
