package ring.serializer;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.util.*;
import java.util.function.BiFunction;

import ring.io.STokenChannel;
import ring.serializer.token.SToken;

/**The base class for all languages used in the {@link Serializer}.<br>
 * This class cannot be subclassed directly, use {@link ByteLanguage} or {@link CharLanguage}.*/
public abstract class Language {
	final boolean needsSkippedFields;
	final List<Class<? extends Annotation>> annotationClasses;
	final BiFunction<Object,Class<?>,Object> fixupReadValueType;
	
	Language(LanguageFeature...features) {
		var tmp = new HashMap<Class<?>,LanguageFeature>();
		for(var f : features) {
			if(tmp.put(f.getClass(),f) != null) throw new IllegalArgumentException("feature "+f.getClass()+" is used twice");
		}
		this.needsSkippedFields = tmp.containsKey(NeedsSkippedFields.class);
		var annot = (NeedsAnnotations)tmp.get(NeedsAnnotations.class);
		this.annotationClasses = annot == null ? Collections.emptyList() : annot.annotationClasses;
		var fixupType = (NeedsFixupReadValueType)tmp.get(NeedsFixupReadValueType.class);
		this.fixupReadValueType = fixupType == null ? null : fixupType.fixupReadValueType;
	}
	
	/**Wraps the given input stream to read tokens from it.*/
	public abstract STokenChannel createChannel(InputStream in);
	
	/**Wraps the given output stream to write tokens to it.*/
	public abstract STokenChannel createChannel(OutputStream out);
	
	@Override public abstract int hashCode();
	
	@Override public abstract boolean equals(Object obj);
	
	/**Base class for all additional {@link Language} features.*/
	public static abstract class LanguageFeature {
		LanguageFeature() {}
	}
	
	/**If this is used on a {@link Language}, the skipped fields will be represented
	 * by using {@link SToken#skipValue} instead of just skipping them.*/
	public static final class NeedsSkippedFields extends LanguageFeature {
		public static final NeedsSkippedFields instance = new NeedsSkippedFields();
		
		private NeedsSkippedFields() {}
	}
	
	/**Some {@link Language}s may be ambiguous in the sense that one object can be serialized in multiple ways.<br>
	 * In these (and only in these!) cases, this interface should be used.<br>
	 * Note that there must be a default way for serialization that will be chosen when no annotations are present.*/
	public static final class NeedsAnnotations extends LanguageFeature {
		final List<Class<? extends Annotation>> annotationClasses;
		
		/**@param annotationClasses Annotations that the implementation of a {@link Language} will use.*/
		public NeedsAnnotations(List<Class<? extends Annotation>> annotationClasses) {
			var tmp = new LinkedHashSet<>(annotationClasses);
			if(tmp.contains(null)) throw new NullPointerException();
			if(tmp.isEmpty()) throw new IllegalArgumentException("empty");
			this.annotationClasses = Collections.unmodifiableList(new ArrayList<>(tmp));
		}
	}
	
	/**Some {@link Language}s may have the problem that the type of serialized primitive values
	 * cannot be determined with 100 percent certainty during deserialization.<br>
	 * In these (and only in these!) cases, this interface should be used.*/
	public static final class NeedsFixupReadValueType extends LanguageFeature {
		final BiFunction<Object,Class<?>,Object> fixupReadValueType;
		
		/**@param fixupReadValueType Converts a primitive value to a different type.*/
		public NeedsFixupReadValueType(BiFunction<Object,Class<?>,Object> fixupReadValueType) {
			this.fixupReadValueType = Objects.requireNonNull(fixupReadValueType);
		}
	}
	
	/**Tests if the raw {@link SToken} streams are compatible between two languages.*/
	public static boolean isSTokenCompatible(Language readLang, Language writeLang) {
		if(!writeLang.annotationClasses.isEmpty()) return false;
		if(readLang.needsSkippedFields != writeLang.needsSkippedFields) return false;
		return readLang.fixupReadValueType == null || Objects.equals(readLang.fixupReadValueType,writeLang.fixupReadValueType);
	}
	
	/**@see #isSTokenCompatible(Language,Language)*/
	public static void checkSTokenCompatible(Language readLang, Language writeLang) {
		if(!isSTokenCompatible(readLang,writeLang)) throw new UnsupportedOperationException("the two languages are not SToken compatible");
	}
}
