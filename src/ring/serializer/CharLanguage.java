package ring.serializer;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;

import ring.io.CharChannel;
import ring.io.InputStreamCharChannel;
import ring.io.OutputStreamCharChannel;
import ring.io.STokenChannel;

/**A {@link Language} that reads and writes from/to a {@link CharChannel}.*/
public abstract class CharLanguage extends Language {
	public CharLanguage(LanguageFeature...features) { super(features); }
	
	@Override public final STokenChannel createChannel(InputStream in) { return createChannel(new InputStreamCharChannel(in)); }
	
	@Override public final STokenChannel createChannel(OutputStream out) { return createChannel(new OutputStreamCharChannel(out)); }
	
	/**Wraps the given reader to read tokens from it.*/
	public final STokenChannel createChannel(Reader in) { return createChannel(new InputStreamCharChannel(in)); }
	
	/**Wraps the given writer to write tokens to it.*/
	public final STokenChannel createChannel(Writer out) { return createChannel(new OutputStreamCharChannel(out)); }
	
	/**Wraps the given channel to read and write tokens from/to it.*/
	public abstract STokenChannel createChannel(CharChannel ch);
}
