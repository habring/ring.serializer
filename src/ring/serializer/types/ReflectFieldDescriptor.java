package ring.serializer.types;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.lang.annotation.Annotation;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**A {@link FieldDescriptor} implementation that uses reflection to access {@link Field}s or getter-/setter-{@link Method}s.<br>
 * The given fields and methods are checked at construction time for their signature. From that point on, {@link MethodHandle}s are used.<br>
 * Note that this implementation provides support for overriding methods. So e.g. if you have a method m() in class A and you override
 * this method in class B, but still use the {@link ReflectFieldDescriptor} you created for class A, it will automatically
 * detect that class B overrides it and use the overridden method.*/
public final class ReflectFieldDescriptor extends FieldDescriptor {
	private final Handle handle;
	
	public ReflectFieldDescriptor(Field f) { this(null,f); }
	
	public ReflectFieldDescriptor(Method getter, Method setter) { this(null,getter,setter); }
	
	public ReflectFieldDescriptor(String name, Field f) { this(name,0,new Handle(null,f)); }
	
	public ReflectFieldDescriptor(String name, Method getter, Method setter) { this(name,0,new Handle(getter,setter)); }
	
	private ReflectFieldDescriptor(String name, int flags, Handle handle) { this(name,flags,handle,null,-1); }
	
	private ReflectFieldDescriptor(String name, int flags, Handle handle, ObjectTypeDescriptor parent, int parentIdx) {
		super(name!=null?name:handle.name,handle.type,flags,parent,parentIdx,handle.annotations);
		this.handle = Objects.requireNonNull(handle);
		if(canSetValue() && handle.isStatic) throw new Error(handle.element+" must be read only");
	}
	
	@Override public Class<?> declaringClass() { return handle.declaringCls; }
	
	@Override
	FieldDescriptor setParent0(ObjectTypeDescriptor parent, int parentIdx) {
		return new ReflectFieldDescriptor(name,flags,handle.setParentType(parent.type),parent,parentIdx);
	}
	
	@Override public FieldDescriptor rename(String name) { return new ReflectFieldDescriptor(name,flags,handle); }
	
	@Override FieldDescriptor setFlags(int flags) { return new ReflectFieldDescriptor(name,flags,handle); }
	
	@Override
	Object getValue0(Object obj) {
		try {
			if(handle.isStatic) return handle.getter.invoke();
			else return handle.getter.invoke(obj);
		} catch(RuntimeException|Error exc) { throw exc; }
		catch(Throwable t) { throw new Error(t); }
	}
	
	@Override public boolean canSetValue() { return handle.setter != null; }
	
	@Override
	void setValue0(Object obj, Object value) {
		try {
			if(handle.isStatic) throw new Error(errors.unreachable);
			handle.setter.invoke(obj,value);
		} catch(RuntimeException|Error exc) { throw exc; }
		catch(Throwable t) { throw new Error(t); }
	}
	
	public static boolean isGetter(Method m) { return isGetter0(m) == null; }
	
	public static Method checkIsGetter(Method m) {
		var err = isGetter0(m);
		if(err == null) return m;
		throw new IllegalArgumentException(err);
	}
	
	private static String isGetter0(Method m) {
		if(m.getParameterCount() != 0) return "getter "+m+" must have no parameters";
		if(m.getReturnType() == void.class) return "getter "+m+" must have a return type";
		return null;
	}
	
	public static boolean isSetter(Method m) { return isSetter0(m) == null; }
	
	public static Method checkIsSetter(Method m) {
		var err = isSetter0(m);
		if(err == null) return m;
		throw new IllegalArgumentException(err);
	}
	
	private static String isSetter0(Method m) {
		if(Modifier.isStatic(m.getModifiers())) return "setter "+m+" must not be static";
		if(m.getParameterCount() != 1) return "setter "+m+" must have one parameter";
		if(m.getReturnType() != void.class) return "setter "+m+" must return void";
		return null;
	}
	
	public static boolean isGetterSetter(Method getter, Method setter) { return isGetterSetter0(getter,setter) == null; }
	
	public static void checkIsGetterSetter(Method getter, Method setter) {
		var err = isGetterSetter0(getter,setter);
		if(err != null) throw new IllegalArgumentException(err);
	}
	
	private static String isGetterSetter0(Method getter, Method setter) {
		var err = isGetter0(getter);
		if(err == null && setter != null) {
			err = isSetter0(setter);
			if(err == null) err = isGetterSetter1(getter,setter);
		}
		return err;
	}
	
	private static String isGetterSetter1(Method getter, Method setter) {
		var setterName = toSetterName(getter.getName());
		if(!setter.getName().equals(setterName)) return "setter "+setter+" for getter "+getter+" must be named "+setterName;
		if(Modifier.isStatic(getter.getModifiers())) return getter+" must not be static when a setter is present";
		Class<?> type1 = getter.getReturnType(), type2 = setter.getParameterTypes()[0];
		Type gtype1 = getter.getGenericReturnType(), gtype2 = setter.getGenericParameterTypes()[0];
		if(type1 != type2 || !gtype1.equals(gtype2)) return errors.typeMismatch(type1,type2,setter);
		Class<?> parentType1 = getter.getDeclaringClass(), parentType2 = setter.getDeclaringClass();
		if(parentType1 != parentType2) return errors.typeMismatch(parentType1,parentType2,"declaring class of "+setter);
		return null;
	}
	
	private static String toSetterName(String getterName) {
		return "set" + Character.toUpperCase(getterName.charAt(0)) + getterName.substring(1);
	}
	
	public static Method findSetterForGetter(Method getter) {
		checkIsGetter(getter);
		for(var setter : checkIsGetter(getter).getDeclaringClass().getDeclaredMethods()) {
			if(isGetterSetter(getter,setter)) return setter;
		}
		return null;
	}
	
	private static class Handle {
		final String name;
		final Type type;
		final Class<?> parentType, declaringCls;
		final MethodHandle getter, setter;
		final java.lang.reflect.AnnotatedElement element;
		final List<Annotation> annotations;
		final boolean isStatic;
		
		Handle(Class<?> parentType, Field f) {
			try {
				this.name = f.getName();
				this.type = f.getGenericType();
				this.parentType = parentType;
				this.declaringCls = f.getDeclaringClass();
				this.isStatic = Modifier.isStatic(f.getModifiers());
				f.setAccessible(true);
				this.getter = MethodHandles.lookup().unreflectGetter(f);
				if(isStatic || Modifier.isFinal(f.getModifiers())) this.setter = null;
				else this.setter = MethodHandles.lookup().unreflectSetter(f);
				this.element = f; this.annotations = List.of(f.getAnnotations());
			} catch(IllegalAccessException exc) { throw new Error(exc); }
		}
		
		Handle(Method getter, Method setter) {
			this.name = getter.getName();
			this.type = getter.getGenericReturnType();
			this.declaringCls = getter.getDeclaringClass();
			this.isStatic = Modifier.isStatic(getter.getModifiers());
			this.getter = reflect.unreflect(checkIsGetter(getter));
			this.parentType = isStatic ? getter.getParameterTypes()[0] : declaringCls;
			if(setter == null) this.setter = null;
			else {
				checkIsGetterSetter(getter,setter);
				this.setter = reflect.unreflect(setter);
			}
			var l = new ArrayList<Annotation>();
			for(var m = getter; m != null; m = reflect.getParent(m)) l.addAll(List.of(m.getAnnotations()));
			this.element = getter; this.annotations = List.copyOf(l);
		}
		
		Handle setParentType(Class<?> parentType) {
			if(this.parentType == Objects.requireNonNull(parentType)) return this;
			if(element instanceof Field f) return new Handle(parentType,f); //fields are never overridden
			else if(element instanceof Method m) {
				var getter = reflect.resolveOverride(m,parentType);
				if(!getter.getGenericReturnType().equals(type)) throw new IllegalArgumentException(getter+" and "+m+" have different return types");
				Method setter = null;
				if(this.setter != null) {
					setter = findSetterForGetter(getter);
					if(setter == null) throw new IllegalArgumentException(parentType+" must override setter for "+getter);
				}
				return getter.equals(m) ? this : new Handle(getter,setter);
			}
			else throw new Error(errors.unreachable);
		}
		
		boolean equals(Handle h) { return element == h.element && setter == h.setter; }
	}
}
