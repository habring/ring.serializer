package ring.serializer.types;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.lang.annotation.Annotation;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Constructor;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Objects;

/**Describes how to serialize an object of a certain type.<br>
 * Note that the type of the object must exactly match the type given to this descriptor.
 * So subclasses need their own type descriptor.<br>
 * {@link TypeDescriptor}s are immutable.*/
public abstract class TypeDescriptor extends AnnotatedElement {
	private final MethodHandle constr;
	
	/**The kind of type descriptor. (a 1:1 mapping to the according java classes)*/
	public static enum Kind { primitive, list, map, convert, object, sobject }
	
	TypeDescriptor(String name, Type type, Class<?> instantiableType, List<Annotation> annotations) { this(name,type,instantiableType,annotations,false); }
	
	TypeDescriptor(String name, Type type, Class<?> instantiableType, List<Annotation> annotations, boolean isAbstract) {
		super(name,type,annotations,0);
		if(isAbstract) { this.constr = null; return; }
		instantiableType = reflect.primitiveMap.getOrDefault(instantiableType,instantiableType);
		Constructor<?> constr = null;
		try {
			if(!instantiableType.isPrimitive()) constr = instantiableType.getDeclaredConstructor();
		} catch (NoSuchMethodException exc) {
			throw new IllegalArgumentException(errors.needNoArgConstr(instantiableType),exc);
		}
		this.constr = instantiableType.isPrimitive() ? MethodHandles.zero(instantiableType) : reflect.unreflect(constr);
	}
	
	TypeDescriptor(TypeDescriptor type, String name, int flags) {
		super(name,type.genericType,type.getAnnotations(),flags);
		if(type.getClass() != getClass()) throw new Error(errors.unreachable);
		this.constr = type.constr;
	}
	
	/**The kind of type descriptor. (a 1:1 mapping to the according java classes)*/
	public abstract Kind kind();
	
	public final boolean isAbstract() { return constr == null; }
	
	@Override public abstract TypeDescriptor rename(String name);
	
	public abstract boolean isAssignableFrom(TypeDescriptor type);
	
	public abstract boolean isInstance(Object obj);
	
	protected void checkIsSubclass(Class<?> type) {
		if(!this.type.isAssignableFrom(type)) throw new ClassCastException(errors.typeMismatch(this.type,type));
	}
	
	public Object createInstance() {
		if(constr == null) return transformInstance(null);
		Object tmp;
		try { tmp = constr.invoke();
		} catch (RuntimeException|Error exc) { throw exc; }
		catch (Throwable t) { throw new Error(t); }
		return transformInstance(Objects.requireNonNull(tmp));
	}
	
	public Object checkValid(Object obj) {
		if(!isAbstract() && type.isInstance(obj)) return obj;
		if(isAbstract() && obj == null) return null;
		throw new IllegalArgumentException();
	}
	
	Object transformInstance(Object obj) { return obj; }
	
	public String defaultName() { return defaultName(genericType); }
	
	@Override public final String toString() { return defaultName(); }
	
	private static String defaultName(Type type) {
		if(type instanceof Class<?> cls) return reflect.primitiveMap.getOrDefault(cls,cls).getSimpleName();
		var baseType = reflect.getBaseType(type);
		var pars = reflect.getTypeParameters(type);
		var b = new StringBuilder(defaultName(baseType)).append('<');
		for(int i = 0; i < pars.length; i++) {
			b.append(defaultName(pars[i]));
			if(i < pars.length-1) b.append(',');
		}
		return b.append('>').toString();
	}
}
