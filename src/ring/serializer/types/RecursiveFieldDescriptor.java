package ring.serializer.types;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**This class is used by the {@link #andThen(FieldDescriptor)}-method.*/
public class RecursiveFieldDescriptor extends FieldDescriptor {
	private final List<FieldDescriptor> l;
	
	RecursiveFieldDescriptor(FieldDescriptor f) { this(f.name,0,Collections.singletonList(f)); }
	
	private RecursiveFieldDescriptor(String name, int flags, List<FieldDescriptor> l) { this(name,flags,l,null,-1); }
	
	private RecursiveFieldDescriptor(String name, int flags, List<FieldDescriptor> l, ObjectTypeDescriptor root, int parentIdx) {
		super(name,l.get(l.size()-1).genericType,flags,root,parentIdx,l.get(l.size()-1).getAnnotations());
		for(int i = 1; i < l.size(); i++) {
			l.get(i).checkExistsIn(l.get(i-1).type);
		}
		this.l = l;
	}
	
	public List<FieldDescriptor> fields() { return l; }
	
	private FieldDescriptor leaf() { return l.get(l.size()-1); }
	
	@Override public Class<?> declaringClass() { return leaf().declaringClass(); }
	
	@Override FieldDescriptor setParent0(ObjectTypeDescriptor parent, int parentIdx) { return new RecursiveFieldDescriptor(name,flags,l,parent,parentIdx); }
	
	@Override public FieldDescriptor rename(String name) { return new RecursiveFieldDescriptor(name,flags,l); }
	
	@Override FieldDescriptor setFlags(int flags) { return new RecursiveFieldDescriptor(name,flags,l); }
	
	@Override
	public FieldDescriptor andThen(FieldDescriptor f) {
		var l = new ArrayList<>(this.l);
		if(f instanceof RecursiveFieldDescriptor tmp) l.addAll(tmp.l);
		else l.add(f);
		return new RecursiveFieldDescriptor(f.name,flags,List.copyOf(l));
	}
	
	private Object resolve(Object obj) {
		for(int i = 0; i < l.size()-1; i++) {
			obj = l.get(i).getValue(obj);
		}
		return obj;
	}
	
	@Override Object getValue0(Object obj) { return leaf().getValue(resolve(obj)); }
	
	@Override public boolean canSetValue() { return leaf().canSetValue(); }
	
	@Override void setValue0(Object obj, Object value) { leaf().setValue(resolve(obj),value); }
}
