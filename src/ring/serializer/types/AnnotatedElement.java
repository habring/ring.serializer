package ring.serializer.types;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.*;

import ring.util.ErrorMessages;
import ring.util.ReflectionUtil;

/**Similar to {@link java.lang.reflect.AnnotatedElement}.*/
public abstract class AnnotatedElement {
	protected static final ErrorMessages errors = new ErrorMessages();
	protected static final ReflectionUtil reflect = new ReflectionUtil();
	public final String name;
	public final Class<?> type;
	/**In contrast to {@link #type}, this may also be a generic type.*/
	public final Type genericType;
	private final List<Annotation> l;
	private final Map<Class<?>,Annotation> m;
	final int flags;
	
	AnnotatedElement(String name, Type type, List<Annotation> annotations, int flags) {
		this.name = name;
		if(name != null && name.strip().length() != name.length()) {
			throw new IllegalArgumentException("name must not start or end with whitespace");
		}
		this.type = reflect.getBaseType(type); this.genericType = Objects.requireNonNull(type);
		this.l = List.copyOf(annotations);
		var m = new HashMap<Class<?>,Annotation>();
		for(var e : this.l) {
			if(m.put(e.annotationType(),e) != null) throw new IllegalArgumentException("annotation "+e.annotationType()+" is found twice");
		}
		this.m = Collections.unmodifiableMap(m);
		this.flags = flags;
	}
	
	/**@see java.lang.reflect.AnnotatedElement#getAnnotation(Class)*/
	public final <T extends Annotation> T getAnnotation(Class<T> annotationClass) { return annotationClass.cast(m.get(annotationClass)); }
	
	/**@see java.lang.reflect.AnnotatedElement#getAnnotations()*/
	public final List<Annotation> getAnnotations() { return l; }
	
	public abstract AnnotatedElement rename(String name);
}
