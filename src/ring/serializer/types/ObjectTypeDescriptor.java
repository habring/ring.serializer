package ring.serializer.types;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import static ring.serializer.types.FieldDescriptor.extDynTypeIfoErr;

import java.lang.invoke.MethodHandle;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.function.Consumer;

import ring.serializer.SerializerConfig;
import ring.serializer.factory.TypeDescriptorFactory;

/**Type descriptor for "normal" objects.*/
public final class ObjectTypeDescriptor extends TypeDescriptor {
	private final List<FieldDescriptor> l;
	private final Map<String,FieldDescriptor> m;
	private final Consumer<Object> init;
	private final Method checkValid;
	private final MethodHandle checkValidHandle;
	
	public ObjectTypeDescriptor(String name, Class<?> type) { this(name,type,Collections.emptyList(),null,null); }
	
	private ObjectTypeDescriptor(String name, Class<?> type, List<FieldDescriptor> l, Consumer<Object> init, Method checkValid) {
		super(getName(name,type,l = new ArrayList<>(l)), type, type, List.of(type.getAnnotations()),
				Modifier.isAbstract(type.getModifiers())||type.isInterface());
		if(isAbstract()) { init = null; checkValid = null; }
		l = new ArrayList<>(l);
		var m = new HashMap<String,FieldDescriptor>();
		this.l = Collections.unmodifiableList(l); this.m = Collections.unmodifiableMap(m);
		for(int i = 0; i < l.size(); i++) {
			var e = l.get(i).setParent(this,i);
			l.set(i,e);
			if(m.put(e.name,e) != null) throw new IllegalArgumentException(errors.duplicateEntry(e.name));
		}
		this.init = init; this.checkValid = checkValid;
		this.checkValidHandle = checkValid == null ? null : reflect.unreflect(checkValid);
		if(checkValid != null) {
			if(checkValid.getReturnType() != void.class) throw new IllegalArgumentException(checkValid+" must return void");
			var params = checkValid.getParameterTypes();
			var isStatic = Modifier.isStatic(checkValid.getModifiers());
			if(!isStatic && params.length != 0) throw new IllegalArgumentException(checkValid+" must not have parameters");
			if(isStatic && (params.length != 1 || params[0].isAssignableFrom(type))) {
				throw new IllegalArgumentException(checkValid+" must have one parameter of type "+type);
			}
		}
		if(hasDynamicTypeInfo() && fieldReplacesParent()) throw new Error(errors.unreachable);
		if(!l.isEmpty() && l.get(l.size()-1).isExternalDynamicTypeInfo()) throw new IllegalArgumentException(extDynTypeIfoErr+"not be the last one");
	}
	
	private static String getName(String name, Class<?> type, List<FieldDescriptor> fields) {
		if(fields.size() > 0 && fields.get(0).isDynamicTypeInfo()) {
			var obj = new ObjectTypeDescriptor(null,type).createInstance();
			var dynType = (String)fields.get(0).getValue0(obj);
			if(name == null || name.equals(dynType)) return dynType;
			throw new IllegalArgumentException("dynamic types must be named according to the value of their dynamic type field");
		}
		else return name;
	}
	
	@Override public Kind kind() { return Kind.object; }
	
	public boolean hasDynamicTypeInfo() { return l.size() > 0 && l.get(0).isDynamicTypeInfo(); }
	
	public boolean fieldReplacesParent() { return l.size() > 0 && l.get(0).replacesParent(); }
	
	public boolean hasLeftoverField() { return leftoverField() != null; }
	
	public FieldDescriptor leftoverField() {
		if(l.isEmpty()) return null;
		var ret = l.get(l.size()-1); return ret.isLeftover() ? ret : null;
	}
	
	@Override public ObjectTypeDescriptor rename(String name) { return new ObjectTypeDescriptor(name,type,l,init,checkValid); }
	
	@Override
	public boolean isAssignableFrom(TypeDescriptor type) {
		if(this == type) return true;
		return type instanceof ObjectTypeDescriptor o && this.type.isAssignableFrom(o.type);
	}
	
	@Override public boolean isInstance(Object obj) { return obj != null && type == obj.getClass(); }
	
	@Override
	Object transformInstance(Object obj) {
		if(init != null) init.accept(obj);
		return obj;
	}
	
	public List<FieldDescriptor> fields() { return l; }
	
	public Map<String,FieldDescriptor> fieldsByName() { return m; }
	
	/**An initializer that is called after the constructor to initialize some fields to a different value than the constructor.<br>
	 * This is meant to be used by custom {@link TypeDescriptorFactory}-implementations to enable different {@link SerializerConfig}s having different initialization.<br>
	 * Therefore, this {@link Consumer} is typically not part of the underlying class.
	 * This is the reason why this is a {@link Consumer}- and not a {@link Method}-object, like {@link #validator()}.*/
	public Consumer<Object> initializer() { return init; }
	
	public Method validator() { return checkValid; }
	
	/**Checks if the given object is valid. Called mainly during deserialization.
	 * @see #setValidator(Method)*/
	@Override
	public Object checkValid(Object obj) {
		checkValidType(obj);
		if(checkValidHandle == null) return obj;
		try { checkValidHandle.invoke(obj); return obj;
		} catch(RuntimeException|Error exc) { throw exc; }
		catch(Throwable t) { throw new Error(t); }
	}
	
	boolean isValidType(Object obj) { return isAbstract() ? obj == null : obj.getClass() == type; }
	
	Object checkValidType(Object obj) {
		if(isValidType(obj)) return obj;
		throw new ClassCastException(errors.typeMismatch(type,obj.getClass()));
	}
	
	public ObjectTypeDescriptor addField(FieldDescriptor f) { return addFields(Collections.singletonList(f)); }
	
	public ObjectTypeDescriptor addFields(List<FieldDescriptor> l) {
		if(l.isEmpty()) return this;
		var tmp = new ArrayList<>(this.l); tmp.addAll(l); return new ObjectTypeDescriptor(name,type,tmp,init,checkValid);
	}
	
	public ObjectTypeDescriptor addExternalTypedField(FieldDescriptor type, FieldDescriptor value) {
		return addFields(Arrays.asList(type.setExternalDynamicTypeInfo(),value.setExternalDynamicTyped()));
	}
	
	public ObjectTypeDescriptor clearFields() { return new ObjectTypeDescriptor(name,type,List.of(),init,checkValid); }
	
	public boolean isInitializer(Method m) { return isValidator(m); }
	
	public boolean isValidator(Method m) { return isValidator0(m) == null; }
	
	public Method checkInitializer(Method m) { return checkValidator(m); }
	
	public Method checkValidator(Method m) {
		var err = isValidator0(m);
		if(err == null) return m;
		throw new IllegalArgumentException(err);
	}
	
	private String isValidator0(Method m) {
		if(m.getReturnType() != void.class) return m+" must return void";
		Class<?> paramType;
		if(Modifier.isStatic(m.getModifiers())) {
			if(m.getParameterCount() != 1) return m+" must have one parameter";
			paramType = m.getParameterTypes()[0];
		}
		else {
			if(m.getParameterCount() != 0) return m+" must have no parameter";
			paramType = m.getDeclaringClass();
		}
		if(!paramType.isAssignableFrom(type)) return errors.typeMismatch(type,paramType,m);
		return null;
	}
	
	public ObjectTypeDescriptor setInitializer(Consumer<Object> init) { return new ObjectTypeDescriptor(name,type,l,Objects.requireNonNull(init),checkValid); }
	
	public ObjectTypeDescriptor setValidator(Method m) { return new ObjectTypeDescriptor(name,type,l,init,m!=null?checkValidator(m):null); }
}
