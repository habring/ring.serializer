package ring.serializer.types;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.util.List;
import java.util.Objects;
import java.util.function.Function;

import ring.serializer.Serializer;

/**The given type should be converted to a different one which is then serialized.<br>
 * The main use of this class is to make types serializable where you cannot change the source code.<br>
 * Note that this may have a significant performance impact (especially if stuff gets copied).<br>
 * Use {@link FieldDescriptor#replacesParent()} instead if possible. Use this class only if you have no other chance.*/
public final class ConvertTypeDescriptor extends TypeDescriptor {
	public final Class<?> internalType;
	private final Function<Object,Object> serialize;
	private final Function<Object,Object> deserialize;
	/**The serializer to use for the converted type or null to use the current one.*/
	public final Serializer internalSerializer;
	private final boolean serializeNull;
	
	public <T,I> ConvertTypeDescriptor(String name, Class<T> type, Class<I> internalType, Function<T,I> serialize, Function<I,T> deserialize) {
		this(name,type,internalType,serialize,deserialize,null);
	}
	
	public <T,I> ConvertTypeDescriptor(String name, Class<T> type, Class<I> internalType,
			Function<T,I> serialize, Function<I,T> deserialize, Serializer internalSerializer) {
		this(name,type,internalType,serialize,deserialize,internalSerializer,false);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public <T,I> ConvertTypeDescriptor(String name, Class<T> type, Class<I> internalType,
			Function<T,I> serialize, Function<I,T> deserialize, Serializer internalSerializer, boolean serializeNull) {
		this(name,type,internalType,(Function)serialize,(Function)deserialize,internalSerializer,serializeNull,0);
	}
	
	private ConvertTypeDescriptor(String name, Class<?> type, Class<?> internalType,
			Function<Object,Object> serialize, Function<Object,Object> deserialize,
			Serializer internalSerializer, boolean serializeNull, int ignore) {
		super(name,type,internalType,List.of(type.getAnnotations())); this.internalType = Objects.requireNonNull(internalType);
		this.serialize = Objects.requireNonNull(serialize); this.deserialize = Objects.requireNonNull(deserialize);
		this.internalSerializer = internalSerializer; this.serializeNull = serializeNull;
		if(type == internalType && internalSerializer == null) {
			throw new IllegalArgumentException("a convert type descriptor must convert to a different type or use a different serializer");
		}
	}
	
	@Override public Kind kind() { return Kind.convert; }
	
	@Override
	public ConvertTypeDescriptor rename(String name) {
		return new ConvertTypeDescriptor(name,type,internalType,serialize,deserialize,internalSerializer,serializeNull,0);
	}
	
	@Override
	public boolean isAssignableFrom(TypeDescriptor type) {
		if(this == type) return true;
		return type instanceof ConvertTypeDescriptor c && this.type == c.type && internalType == c.internalType &&
				serialize == c.serialize && deserialize == c.deserialize && internalSerializer == c.internalSerializer;
	}
	
	@Override public boolean isInstance(Object obj) { return obj != null && obj.getClass() == type; }
	
	public Object serialize(Object obj) {
		if(serializeNull) {
			if(obj != null) checkType(type,obj);
			var ret = serialize.apply(obj);
			if(ret != null) checkType(internalType,ret);
			return ret;
		}
		else {
			if(obj == null) return null;
			checkType(type,obj);
			var ret = serialize.apply(obj);
			checkType(internalType,ret);
			return ret;
		}
	}
	
	public Object deserialize(Object obj) {
		if(serializeNull) {
			if(obj != null) checkType(internalType,obj);
			var ret = deserialize.apply(obj);
			if(ret != null) checkType(type,ret);
			return ret;
		}
		else {
			if(obj == null) return null;
			checkType(internalType,obj);
			var ret = deserialize.apply(obj);
			checkType(type,ret);
			return ret;
		}
	}
	
	@Override Object transformInstance(Object obj) { return deserialize(obj); }
	
	private static void checkType(Class<?> expected, Object got) {
		if(expected != got.getClass()) throw new ClassCastException(errors.typeMismatch(expected,got.getClass()));
	}
}
