package ring.serializer.types;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.util.List;

/**Declares a type as primitive type.*/
public final class PrimitiveTypeDescriptor extends TypeDescriptor {
	public PrimitiveTypeDescriptor(String name, Class<?> type) { super(name,type,type,List.of(type.getDeclaredAnnotations())); }
	
	@Override public Kind kind() { return Kind.primitive; }
	
	@Override public PrimitiveTypeDescriptor rename(String name) { return new PrimitiveTypeDescriptor(name,type); }
	
	@Override
	public boolean isAssignableFrom(TypeDescriptor type) {
		if(this == type) return true;
		return type instanceof PrimitiveTypeDescriptor && this.type == type.type;
	}
	
	@Override public boolean isInstance(Object obj) { return obj != null && type == obj.getClass(); }
}
