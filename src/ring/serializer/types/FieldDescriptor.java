package ring.serializer.types;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Objects;

import ring.serializer.Serializer;
import ring.serializer.token.SMap;
import ring.serializer.token.SObject;

/**The base class for all fields in an {@link ObjectTypeDescriptor}.<br>
 * {@link FieldDescriptor}s are immutable.*/
public abstract class FieldDescriptor extends AnnotatedElement {
	static final String dynTypeIfoErr = "dynamic type info fields must ", extDynTypeIfoErr = "external "+dynTypeIfoErr;
	
	private static enum Flag {
		dynamicTypeInfo, externalDynamicTypeInfo, externalDynamicTyped,
		skipIfDefaultValue, leftover, replaceParent;
		static final int allFlags = (1<<values().length)-1;
		final int value;
		
		Flag() { value = 1<<ordinal(); }
		
		int addTo(int flags) { return flags|value; }
		
		boolean isPresent(int flags) { return (flags&value) != 0; }
	}
	
	private final ObjectTypeDescriptor parent;
	private final int parentIdx;
	
	FieldDescriptor(String name, Type type, int flags, ObjectTypeDescriptor parent, int parentIdx, List<Annotation> annotations) {
		super(Objects.requireNonNull(name),type,annotations,flags);
		this.parent = parent; this.parentIdx = parentIdx;
		if((flags&Flag.allFlags) != flags) throw new Error(errors.unreachable);
		if(isDynamicTypeInfo()) {
			if(type != String.class) throw new IllegalArgumentException(dynTypeIfoErr+"be of type "+String.class);
			if(canSetValue()) throw new IllegalArgumentException(dynTypeIfoErr+"not be writable");
			if(parentIdx > 0) throw new IllegalArgumentException(dynTypeIfoErr+"be the first one in an object");
		}
		if(isExternalDynamicTypeInfo()) {
			if(isExternalDynamicTyped()) throw new IllegalArgumentException(extDynTypeIfoErr+"must not be external typed");
			if(type != String.class) throw new IllegalArgumentException(extDynTypeIfoErr+"be of type "+String.class);
			if(parent != null && parentIdx+1 < parent.fields().size() && !next().isExternalDynamicTyped()) {
				throw new IllegalArgumentException(extDynTypeIfoErr+"be followed by a dynamically typed field");
			}
		}
		if(isExternalDynamicTyped()) {
			if(parentIdx <= 0 ? parentIdx == 0 : !prev().isExternalDynamicTypeInfo()) {
				throw new IllegalArgumentException(extDynTypeIfoErr+"be followed by a dynamically typed field");
			}
		}
		if(isLeftover()) {
			var err = "a leftover field ";
			if(parent != null && parentIdx != parent.fields().size()-1) {
				throw new IllegalArgumentException(err+"must be the last declared field");
			}
			if(isDynamicTypeInfo() || isExternalDynamicTypeInfo() || isExternalDynamicTyped()) {
				throw new IllegalArgumentException(err+"must not interfere with dynamic typing");
			}
			if(!skipIfDefaultValue()) throw new Error(errors.unreachable);
			if(super.type != SObject.class && super.type != SMap.class) {
				throw new IllegalArgumentException(err+" must be a "+SObject.class.getSimpleName());
			}
		}
		if(replacesParent()) {
			var err = "a field that is used as root ";
			if(isDynamicTypeInfo()) throw new IllegalArgumentException(err+"must not be a dynamic type info");
			if(skipIfDefaultValue()) throw new IllegalArgumentException(err+"cannot be skipped if it has its default value");
			if(isLeftover()) throw new IllegalArgumentException(err+"cannot be a leftover field");
			if(parent != null && parent.fields().size() != 1) throw new IllegalArgumentException(err+"must be the only one in the according object type");
		}
	}
	
	public static FieldDescriptor createDummyR(String name, Class<?> parentType, Type fldType) {
		return createDummy(name,parentType,fldType,false);
	}
	
	public static FieldDescriptor createDummyRW(String name, Class<?> parentType, Type fldType) {
		return createDummy(name,parentType,fldType,true);
	}
	
	public static FieldDescriptor createDummyLeftover(String name, Class<?> parentType) {
		return createDummyRW(name,parentType,SObject.class).setLeftover();
	}
	
	private static FieldDescriptor createDummy(String name, Class<?> parentType, Type fldType, boolean canSetValue) {
		return new FunctionFieldDescriptor<>(name,parentType,fldType,x->null,canSetValue?(x,y)->{}:null,List.of());
	}
	
	/**The {@link ObjectTypeDescriptor} this field is declared in.
	 * @throws IllegalStateException If this field is not added to a parent yet.*/
	public ObjectTypeDescriptor parent() {
		if(parent != null) return parent;
		throw new IllegalStateException();
	}
	
	/**The index this field is found at in its {@link #parent()}.
	 * @throws IllegalStateException If this field is not added to a parent yet.*/
	public int parentIndex() {
		if(parentIdx >= 0) return parentIdx;
		throw new IllegalStateException();
	}
	
	public FieldDescriptor prev() { return getOrNull(parentIndex()-1); }
	
	public FieldDescriptor next() { return getOrNull(parentIndex()+1); }
	
	private FieldDescriptor getOrNull(int i) {
		return i < 0 || i >= parent().fields().size() ? null : parent().fields().get(i);
	}
	
	public Object getValue(Object obj) { return getValue0(parent().checkValidType(obj)); }
	
	public abstract boolean canSetValue();
	
	public void setValue(Object obj, Object value) {
		if(!canSetValue()) throw new UnsupportedOperationException();
		setValue0(parent().checkValidType(obj),value);
	}
	
	public Type getExternalDynamicTypeInfo(Object obj, Serializer serializer) {
		if(!isExternalDynamicTypeInfo()) throw new UnsupportedOperationException();
		var name = (String)getValue(obj);
		return serializer.isValidTypeName(name) ? serializer.getTypeForName(name) : SObject.class;
	}
	
	public Type validateExternalDynamicTypeInfo(Object obj, Serializer serializer) {
		var expectedType = getExternalDynamicTypeInfo(obj,serializer);
		var value = next().getValue(obj);
		if(value == null || value.getClass() == expectedType) return expectedType;
		var name = serializer.getNameForType(value.getClass());
		setValue(obj,name); return value.getClass();
	}
	
	/**Returns a field that first acceses this field and then the given one.*/
	public FieldDescriptor andThen(FieldDescriptor f) { return new RecursiveFieldDescriptor(this).andThen(f); }
	
	FieldDescriptor setParent(ObjectTypeDescriptor parent, int parentIdx) {
		if(this.parent == parent) return this;
		if(this.parent != null) checkExistsIn(parent.type);
		if(parent.fields().get(parentIdx) != this) throw new Error(errors.unreachable);
		var ret = setParent0(parent,parentIdx); ret.checkExistsIn(parent.type); return ret;
	}
	
	@Override public abstract FieldDescriptor rename(String name);
	
	/**If this is set as the first field in an object, dynamic type-safety is enabled.<br>
	 * Note that the field must be of type {@link String} and must not be writable.*/
	public boolean isDynamicTypeInfo() { return Flag.dynamicTypeInfo.isPresent(flags); }
	
	public FieldDescriptor setDynamicTypeInfo() { return setFlags(Flag.dynamicTypeInfo.addTo(flags)); }
	
	/**This field stores dynamic type information for the field following after it.
	 * The field after this field must therefore be {@link #isExternalDynamicTyped()}.
	 * The type of this field must be {@link String} and may or may not be writable.
	 * Note that enabling this AND {@link #isDynamicTypeInfo()} on one field is indeed legal.*/
	public boolean isExternalDynamicTypeInfo() { return Flag.externalDynamicTypeInfo.isPresent(flags); }
	
	public FieldDescriptor setExternalDynamicTypeInfo() { return setFlags(Flag.externalDynamicTypeInfo.addTo(flags)); }
	
	/**This field stores a dynamically typed object.
	 * The field before this must be {@link #isExternalDynamicTypeInfo()} to store the type.
	 * {@link SObject} is used if the type name is unknown.*/
	public boolean isExternalDynamicTyped() { return Flag.externalDynamicTyped.isPresent(flags); }
	
	public FieldDescriptor setExternalDynamicTyped() { return setFlags(Flag.externalDynamicTyped.addTo(flags)); }
	
	/**Checks if the field has a value equal to its default one (the value it has after creating a new instance.<br>
	 * Skips the field when serializing if this is the case.<br>
	 * Note that dynamic type info fields are skipped if static and dynamic type are the same if this is set to true.*/
	public boolean skipIfDefaultValue() { return Flag.skipIfDefaultValue.isPresent(flags); }
	
	public FieldDescriptor setSkipIfDefaultValue() { return setFlags(Flag.skipIfDefaultValue.addTo(flags)); }
	
	/**If an object contains only one field, this field can replace its parent object.<br>
	 * This means that this field (and its content) will take the place where the parent object would be.<br>
	 * Useful e.g. if you want to serialize a list type directly without a parent object in the result.*/
	public boolean replacesParent() { return Flag.replaceParent.isPresent(flags); }
	
	public FieldDescriptor setReplaceParent() { return setFlags(Flag.replaceParent.addTo(flags)); }
	
	/**This field stores the values of all additional fields which have no {@link FieldDescriptor}.
	 * */
	public boolean isLeftover() { return Flag.leftover.isPresent(flags); }
	
	public FieldDescriptor setLeftover() { return setFlags(Flag.leftover.addTo(Flag.skipIfDefaultValue.addTo(flags))); }
	
	abstract FieldDescriptor setFlags(int flags);
	
	public boolean existsIn(Class<?> type) { return parent().type.isAssignableFrom(type); }
	
	void checkExistsIn(Class<?> type) {
		if(!existsIn(type)) throw new ClassCastException(errors.typeMismatch(parent().type,type));
	}
	
	public abstract Class<?> declaringClass();
	
	abstract FieldDescriptor setParent0(ObjectTypeDescriptor parent, int parentIdx);
	
	abstract Object getValue0(Object obj);
	
	abstract void setValue0(Object obj, Object value);
	
	@Override public String toString() { return parent+"."+name; }
}
