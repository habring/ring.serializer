package ring.serializer.types;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import ring.serializer.token.SList;
import ring.serializer.token.SMap;
import ring.serializer.token.SObject;
import ring.serializer.token.SValue;

public final class SObjectTypeDescriptor extends TypeDescriptor {
	public static final SObjectTypeDescriptor instance = new SObjectTypeDescriptor();
	@SuppressWarnings("unused")
	private static final List<SObject> genericListType = null;
	@SuppressWarnings("unused")
	private static final Map<String,SObject> genericMapType = null;
	public final ListTypeDescriptor listType = new ListTypeDescriptor(getGenericType("genericListType"),SList.class);
	public final MapTypeDescriptor mapType = new MapTypeDescriptor(getGenericType("genericMapType"),SMap.class);
	
	private static Type getGenericType(String fldName) {
		try { return SObjectTypeDescriptor.class.getDeclaredField(fldName).getGenericType();
		} catch (Throwable t) { throw new Error(t); }
	}
	
	private SObjectTypeDescriptor() { super("raw",SObject.class,SValue.class,Collections.emptyList()); }
	
	@Override public Kind kind() { return Kind.sobject; }
	
	@Override public TypeDescriptor rename(String name) { throw new UnsupportedOperationException(); }
	
	@Override public boolean isAssignableFrom(TypeDescriptor type) { return this == type; }
	
	@Override public boolean isInstance(Object obj) { return type.isInstance(obj); }
}
