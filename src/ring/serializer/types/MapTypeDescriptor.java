package ring.serializer.types;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

/**Represents a {@link Map}.*/
public final class MapTypeDescriptor extends TypeDescriptor {
	public final Class<?> keyType, valueType;
	
	public MapTypeDescriptor(Type genericType, Class<?> instantiableType) {
		super(null,genericType,instantiableType,Arrays.asList(instantiableType.getDeclaredAnnotations()));
		var pars = reflect.getTypeParameters(genericType);
		if(this.type != Map.class || pars.length != 2) {
			throw new ClassCastException(errors.typeMismatch(Map.class,this.type));
		}
		checkIsSubclass(instantiableType);
		this.keyType = Objects.requireNonNull(pars[0]);
		this.valueType = Objects.requireNonNull(pars[1]);
	}
	
	@Override public Kind kind() { return Kind.map; }
	
	@Override public MapTypeDescriptor rename(String name) { throw new UnsupportedOperationException(); }
	
	@Override
	public boolean isAssignableFrom(TypeDescriptor type) {
		if(this == type) return true;
		return type instanceof MapTypeDescriptor m && keyType == m.keyType && valueType == m.valueType;
	}
	
	@Override public boolean isInstance(Object obj) { return type.isInstance(obj); }
	
	public int sizeOf(Object obj) { return ((Map<?,?>)obj).size(); }
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Iterator<Entry<?,?>> iterator(Object obj) { return (Iterator)((Map<?,?>)obj).entrySet().iterator(); }
	
	public boolean containsKey(Object obj, Object key) { return ((Map<?,?>)obj).containsKey(key); }
	
	public Object get(Object obj, Object key) { return ((Map<?,?>)obj).get(key); }

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Object put(Object obj, Object key, Object value) {
		if(!keyType.isInstance(key)) throw new ClassCastException(errors.typeMismatch(keyType,key.getClass()));
		if(value != null && !valueType.isInstance(value)) throw new ClassCastException(errors.typeMismatch(valueType,value.getClass()));
		return ((Map)obj).put(key,value);
	}
	
	public void clear(Object obj) { ((Map<?,?>)obj).clear(); }
}
