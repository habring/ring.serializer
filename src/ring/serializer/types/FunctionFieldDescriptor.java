package ring.serializer.types;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**Represents a field that does not have a direct representation in the according class.<br>
 * Similar to {@link ConvertTypeDescriptor}, but for fields.<br>
 * Use this class only if you have no other chance.*/
public final class FunctionFieldDescriptor<T,P> extends FieldDescriptor {
	private final Class<T> objCls;
	private final Function<T,P> getter;
	private final BiConsumer<T,P> setter;
	
	public FunctionFieldDescriptor(String name, Class<T> parentType, Class<P> fldCls, Function<T,P> getter, BiConsumer<T,P> setter) {
		this(name,parentType,fldCls,getter,setter,List.of());
	}
	
	public FunctionFieldDescriptor(String name, Class<T> parentType, Class<P> fldCls, Function<T,P> getter, BiConsumer<T,P> setter, List<Annotation> annotations) {
		this(name,parentType,(Type)fldCls,getter,setter,annotations);
	}
	
	public FunctionFieldDescriptor(String name, Class<T> parentType, Type fldType, Function<T,P> getter, BiConsumer<T,P> setter, List<Annotation> annotations) {
		this(name,0,parentType,fldType,getter,setter,annotations);
	}
	
	private FunctionFieldDescriptor(String name, int flags, Class<T> parentType, Type fldType,
			Function<T,P> getter, BiConsumer<T,P> setter, List<Annotation> annotations) {
		this(name,flags,parentType,fldType,getter,setter,null,-1,annotations);
	}
	
	private FunctionFieldDescriptor(String name, int flags, Class<T> parentType, Type fldType,
			Function<T,P> getter, BiConsumer<T,P> setter, ObjectTypeDescriptor parent, int parentIdx, List<Annotation> annotations) {
		super(name,fldType,flags,parent,parentIdx,annotations); this.objCls = Objects.requireNonNull(parentType);
		this.getter = Objects.requireNonNull(getter); this.setter = setter;
	}
	
	@Override public Class<?> declaringClass() { return objCls; }
	
	@Override
	FieldDescriptor setParent0(ObjectTypeDescriptor parent, int parentIdx) {
		return new FunctionFieldDescriptor<>(name,flags,objCls,genericType,getter,setter,parent,parentIdx,getAnnotations());
	}
	
	@Override public FieldDescriptor rename(String name) { return new FunctionFieldDescriptor<>(name,flags,objCls,genericType,getter,setter,getAnnotations()); }
	
	@Override FieldDescriptor setFlags(int flags) { return new FunctionFieldDescriptor<>(name,flags,objCls,genericType,getter,setter,getAnnotations()); }
	
	@SuppressWarnings("unchecked")
	@Override Object getValue0(Object obj) { return getter.apply((T)obj); }
	
	@Override public boolean canSetValue() { return setter != null; }
	
	@Override
	@SuppressWarnings("unchecked")
	void setValue0(Object obj, Object value) { setter.accept((T)obj,(P)value); }
}
