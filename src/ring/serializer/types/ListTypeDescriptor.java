package ring.serializer.types;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.lang.reflect.Type;
import java.util.*;

/**Represents a {@link List} or {@link Set}.*/
public final class ListTypeDescriptor extends TypeDescriptor {
	private static final int flagSimplifySingleElements = 1;
	private static final int allFlags = 0b1;
	public final Class<?> elementType;
	
	public ListTypeDescriptor(Type genericType, Class<?> instantiableType) {
		super(null,genericType,instantiableType,Arrays.asList(instantiableType.getDeclaredAnnotations()));
		var pars = reflect.getTypeParameters(genericType);
		if((this.type != List.class && this.type != Set.class) || pars.length != 1) {
			throw new ClassCastException(errors.typeMismatch(List.class,this.type));
		}
		checkIsSubclass(instantiableType);
		this.elementType = Objects.requireNonNull(pars[0]);
	}
	
	private ListTypeDescriptor(ListTypeDescriptor type, int flags) {
		super(type,null,flags); this.elementType = type.elementType;
		if((flags&allFlags) != flags) throw new Error(errors.unreachable);
	}
	
	@Override public Kind kind() { return Kind.list; }
	
	public boolean simplifySingleElements() { return (flags&flagSimplifySingleElements) != 0; }
	
	public ListTypeDescriptor setSimplifySingleElements() { return new ListTypeDescriptor(this,flags|flagSimplifySingleElements); }
	
	@Override public ListTypeDescriptor rename(String name) { throw new UnsupportedOperationException(); }
	
	@Override
	public boolean isAssignableFrom(TypeDescriptor type) {
		if(this == type) return true;
		return type instanceof ListTypeDescriptor l && elementType == l.elementType && this.type == l.type;
	}
	
	@Override public boolean isInstance(Object obj) { return type.isInstance(obj); }
	
	public int sizeOf(Object obj) { return ((Collection<?>)obj).size(); }
	
	public Iterator<?> iterator(Object obj) { return ((Collection<?>)obj).iterator(); }
	
	public boolean contains(Object obj, Object elem) { return ((Collection<?>)obj).contains(elem); }
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void add(Object obj, Object elem) {
		if(elem != null && !elementType.isInstance(elem)) throw new ClassCastException(errors.typeMismatch(elementType,elem.getClass()));
		((Collection)obj).add(elem);
	}
	
	public void clear(Object obj) { ((Collection<?>)obj).clear(); }
}
