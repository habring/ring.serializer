package ring.serializer;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import static ring.serializer.token.STokenType.elemSeperator;
import static ring.serializer.token.STokenType.fieldSeperator;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.List;

import ring.io.STokenChannel;
import ring.serializer.token.SObject;
import ring.serializer.token.SToken;

abstract class SerializerBase extends SerializerTypeDescriptors {
	SerializerBase(SerializerConfig config) { super(config); }
	
//	---------- Internal Helper Methods ----------
	
	static SToken readTok(STokenChannel in) throws IOException {
		var tok = in.read();
		if(tok != null) { tok.checkValid(); return tok; }
		throw new IOException(errors.unexpectedEoi);
	}
	
	static void checkSeperator(SToken tok) {
		if(tok.type == fieldSeperator || tok.type == elemSeperator) return;
		throw new IllegalArgumentException(errors.invalidToken(tok,fieldSeperator,elemSeperator));
	}
	
	static SToken setAnnot(SToken tok, List<Annotation> annotations) { tok.annotations = annotations; return tok; }
	
	static SObject checkSKind(SObject obj, SObject.Kind kind) {
		if(obj.kind() == kind) return obj;
		throw new IllegalArgumentException(errors.typeMismatch(kind.type,obj.getClass()));
	}
	
//	---------- Internal API ----------
	abstract int hashCode0(Type staticType, Object obj, int maxDepth);
	abstract Boolean equals0(Type staticType, Object obj1, Object obj2, int maxDepth);
	abstract Object clone0(Object obj, int maxDepth);
	abstract Object copy0(Type staticType, Object src, Object dest, int maxDepth);
	abstract void readInput0(SToken la, STokenChannel in) throws IOException;
	abstract Object deserialize0(Type staticType, SToken la, STokenChannel in) throws IOException;
	abstract Object deserializeS0(Type staticType, SObject obj);
	abstract void serialize0(Type staticType, Object obj, STokenChannel out) throws IOException;
	abstract SObject serializeS0(Type staticType, Object obj);
}
