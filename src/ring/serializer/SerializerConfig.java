package ring.serializer;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.util.List;
import java.util.Objects;

import ring.serializer.factory.GenericTypeDescriptorFactory;
import ring.serializer.factory.TypeDescriptorFactories;
import ring.util.UpdateNotifyList;

/**Contains the configuration for a {@link Serializer}.*/
public class SerializerConfig {
	/**The type descriptor factories to use. The factories are used in the order specified for any new type.*/
	public final List<GenericTypeDescriptorFactory> factories = new UpdateNotifyList<>(this::invalidate);
	private Language language;
	private boolean isValid;
	
	public SerializerConfig() { factories.addAll(TypeDescriptorFactories.defaultInstances); }
	
	public SerializerConfig(SerializerConfig config) {
		this.factories.addAll(config.factories); this.language = config.language; this.isValid = config.isValid;
	}
	
	/**The language to use within the {@link Serializer}.*/
	public Language language() { return language; }
	
	/**Sets the language to use within the {@link Serializer}.*/
	public SerializerConfig setLanguage(Language l) { invalidate(); this.language = l; return this; }
	
	public final boolean isValid() { return isValid; }
	
	public SerializerConfig invalidate() { isValid = false; return this; }
	
	/**Checks if this configuration is valid.*/
	public SerializerConfig checkValid() {
		if(isValid) return this;
		factories.forEach(Objects::requireNonNull);
		if(factories.isEmpty()) throw new IllegalArgumentException("no factories defined");
		if(language == null) throw new IllegalArgumentException("no language defined");
		isValid = true; return this;
	}
	
	@Override public int hashCode() { return factories.hashCode() ^ Objects.hashCode(language); }
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null || getClass() != obj.getClass()) return false;
		var config = (SerializerConfig)obj;
		return factories.equals(config.factories) && Objects.equals(language,config.language);
	}
}
