package ring.serializer;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.io.IOException;

import ring.io.STokenChannel;
import ring.serializer.token.SObject;
import ring.serializer.token.SToken;
import ring.serializer.types.ConvertTypeDescriptor;

abstract class SerializerImplConv extends SerializerImplMap {
	SerializerImplConv(SerializerConfig config) { super(config); }
	
	final int hashCode0(ConvertTypeDescriptor type, Object obj, int maxDepth) {
		var internalObj = serialize(type,obj);
		return serializer(type).hashCode0(type.internalType,internalObj,maxDepth);
	}
	
	final Boolean equals0(ConvertTypeDescriptor type, Object obj1, Object obj2, int maxDepth) {
		var internalObj1 = serialize(type,obj1);
		var internalObj2 = serialize(type,obj2);
		return serializer(type).equals0(type.internalType,internalObj1,internalObj2,maxDepth);
	}
	
	final Object copy0(ConvertTypeDescriptor type, Object src, Object dest, int maxDepth) {
		var internalObj1 = serialize(type,src);
		var internalObj2 = serialize(type,dest);
		var tmp = copy0(type.internalType,internalObj1,internalObj2,maxDepth);
		return deserialize(type,tmp);
	}
	
	final Object deserialize0(ConvertTypeDescriptor type, SToken la, STokenChannel in) throws IOException {
		var internalObj = serializer(type).deserialize0(type.internalType,la,in);
		return deserialize(type,internalObj);
	}
	
	final Object deserializeS0(ConvertTypeDescriptor type, SObject obj) {
		var internalObj = serializer(type).deserializeS0(type.internalType,obj);
		return deserialize(type,internalObj);
	}
	
	final void serialize0(ConvertTypeDescriptor type, Object obj, STokenChannel out) throws IOException {
		var internalObj = serialize(type,obj);
		serializer(type).serialize0(type.internalType,internalObj,out);
	}
	
	final SObject serializeS0(ConvertTypeDescriptor type, Object obj) {
		var internalObj = serialize(type,obj);
		return serializeS0(type.internalType,internalObj);
	}
	
	private SerializerBase serializer(ConvertTypeDescriptor type) { return type.internalSerializer != null ? type.internalSerializer : this; }
	
	private static Object deserialize(ConvertTypeDescriptor type, Object internalObj) { return type.deserialize(internalObj); }
	
	private static Object serialize(ConvertTypeDescriptor type, Object obj) { return type.serialize(obj); }
}
