package ring.serializer;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.*;
import java.util.AbstractMap.SimpleEntry;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.BiConsumer;
import java.util.function.Function;

import ring.serializer.token.SList;
import ring.serializer.token.SMap;
import ring.serializer.token.SObject;
import ring.serializer.token.SValue;
import ring.serializer.types.*;
import ring.serializer.types.TypeDescriptor.Kind;
import ring.util.ClassCrawler;
import ring.util.ErrorMessages;
import ring.util.ReflectionUtil;

/**the internal data structures for {@link TypeDescriptor}s and {@link SerializerConfig}s*/
abstract class SerializerTypeDescriptors {
	protected static final ErrorMessages errors = new ErrorMessages();
	protected static final ReflectionUtil reflect = new ReflectionUtil();
	protected static final int narrowDepth = 2;
	private final SerializerConfig config;
	private final ConcurrentMap<Type,CacheValue> cache = new ConcurrentHashMap<>();
	private final ConcurrentMap<String,Type> nameCache = new ConcurrentHashMap<>();
	private final ConcurrentMap<Type,List<String>> nameCacheInv = new ConcurrentHashMap<>();
	
	SerializerTypeDescriptors(SerializerConfig config) {
		config.checkValid(); this.config = new SerializerConfig(config); this.config.checkValid();
	}
	
	static class CacheValue {
		final TypeDescriptor type; final Object defaultValue;
		final List<Annotation> annotations; final List<List<Annotation>> fldAnnotations;
		final ConcurrentMap<String,Class<?>> knownSubTypes;
		List<Class<?>> superTypesPub, knownSubTypesPub = List.of();
		int depth = -1;
		final RuntimeException exc;
		
		private CacheValue(RuntimeException exc) { this(null,null,Objects.requireNonNull(exc)); }
		
		private CacheValue(TypeDescriptor type, Language lang) { this(Objects.requireNonNull(type),lang,null); }
		
		private CacheValue(TypeDescriptor type, Language lang, RuntimeException exc) {
			this.type = type; this.defaultValue = type != null ? type.createInstance() : null;
			this.knownSubTypes = type != null ? new ConcurrentHashMap<>() : null; this.exc = exc;
			this.annotations = getAnnotations(type,lang);
			if((type != null) == (exc != null)) throw new Error(errors.unreachable);
			if(type instanceof ObjectTypeDescriptor tmp) {
				var l = new ArrayList<List<Annotation>>();
				for(var fld : tmp.fields()) l.add(getAnnotations(fld,lang));
				this.fldAnnotations = Collections.unmodifiableList(l);
			}
			else this.fldAnnotations = null;
		}
		
		private static List<Annotation> getAnnotations(AnnotatedElement elem, Language lang) {
			if(elem == null || lang == null) return null;
			var l = new ArrayList<Annotation>();
			for(var cls : lang.annotationClasses) {
				var tmp = elem.getAnnotation(cls);
				if(tmp != null) l.add(tmp);
			}
			return l.isEmpty() ? null : Collections.unmodifiableList(l);
		}
	}
	
	@Override public final int hashCode() { return config.hashCode(); }
	
	@Override public final boolean equals(Object obj) { return obj instanceof Serializer s && equals(s); }
	
	public final boolean equals(Serializer s) { return s != null && config.equals(((SerializerTypeDescriptors)s).config); }
	
	/**@return True if the two serializers are compatible in the sense that this {@link Serializer}
	 * can use the given {@link Serializer} (via {@link ConvertTypeDescriptor#internalSerializer}).*/
	public final boolean isCompatibleTo(Serializer serializer) { return isCompatibleTo((SerializerTypeDescriptors)serializer); }
	
	private boolean isCompatibleTo(SerializerTypeDescriptors serializer) { return language().equals(serializer.language()); }
	
	public final SerializerConfig config() { return new SerializerConfig(config); }
	
	public final Language language() {
		var ret = config.language();
		if(ret != null) return ret;
		throw new UnsupportedOperationException("this operation needs a "+Language.class+" to work");
	}
	
	public final String getNameForType(Type type) { return getNamesForType(type).get(0); }
	
	public final List<String> getNamesForType(Type type) {
		var t = getDescriptor0(type);
		if(t.type != null) type = t.type.genericType;
		var ret = nameCacheInv.get(type);
		if(ret != null && !ret.isEmpty()) return ret;
		if(t.type.name != null && t.type.kind() == Kind.sobject) return List.of(t.type.name);
		throw new IllegalArgumentException("name for type "+type+" not found");
	}
	
	public final boolean isValidTypeName(String name) {
		if(name == null) return false;
		var tmp = nameCache.get(name); return tmp != null && tmp != Object.class;
	}
	
	public final Map<String,Type> getNameToTypeMapping() { return Collections.unmodifiableMap(new HashMap<>(nameCache)); }
	
	public final Type getTypeForName(String name) {
		var ret = nameCache.get(name);
		if(ret == null) throw new IllegalArgumentException("type with name "+name+" not found");
		if(ret == Object.class) throw new IllegalArgumentException("type name "+name+" is ambiguous");
		return ret;
	}
	
	public final Collection<TypeDescriptor> getAllDescriptors() {
		var ret = new ArrayList<TypeDescriptor>();
		for(var e : cache.values()) {
			if(e.type != null) ret.add(e.type);
		}
		return Collections.unmodifiableList(ret);
	}
	
	final CacheValue getDescriptor0(Type type) {
		var ret = getDescriptor1(type);
		if(ret.exc == null) return ret;
		throw new IllegalArgumentException(type+" is not supported by this serializer",ret.exc);
	}
	
	final CacheValue getDescriptor1(Type type) {
		var ret = cache.get(type);
		if(ret != null) return ret;
		var pending = new ArrayList<Entry<SerializerTypeDescriptors,Type>>();
		ret = createDescriptor(type,type,(k,v)->pending.add(new SimpleEntry<>(k,v)));
		if(ret.type instanceof ObjectTypeDescriptor tmp) {
			var fields = tmp.fields();
			if(fields.size() > 0 && fields.get(0).isDynamicTypeInfo()) {
				var clsName = (String)fields.get(0).getValue(ret.type.createInstance());
				if(clsName != null) addKnownSubType(null,reflect.getBaseType(type),clsName);
			}
		}
		var ret_ = ret;
		if(ret.type != null) {
			addToNameCache(ret.type);
			ret = cache.computeIfAbsent(ret.type.genericType,k->ret_);
			cache.putIfAbsent(type,ret);
		}
		else ret = cache.computeIfAbsent(type,k->ret_);
		for(var e : pending) e.getKey().getDescriptor1(e.getValue());
		return ret;
	}
	
	private void addToNameCache(TypeDescriptor type) {
		if(type.name == null) return;
		addToNameCache(type.name,type.genericType);
		for(var tmp : cache.values()) {
			if(tmp.knownSubTypes == null || type.type != tmp.knownSubTypes.get(type.name)) continue;
			addToNameCache(tmp.type.name+"/"+type.name,type.genericType);
		}
	}
	
	private void addToNameCache(String name, Type type) {
		nameCache.compute(name,(k,v) -> {
			if(v == null || v.equals(type)) return type;
			nameCacheInv.computeIfPresent(v,(k2,v2) -> {
				v2 = new ArrayList<>(v2); v2.remove(name);
				return v2.isEmpty() ? null : Collections.unmodifiableList(v2);
			});
			return Object.class;
		});
		nameCacheInv.compute(type,(k,v) -> {
			var s = new LinkedHashSet<String>();
			if(v != null) s.addAll(v);
			s.add(name); return Collections.unmodifiableList(new ArrayList<>(s));
		});
	}
	
	private void addKnownSubType(Class<?> cls, Class<?> subCls, String subClsName) {
		if(cls != null) {
			var tmp = getDescriptor1(cls);
			if(tmp.knownSubTypes != null) {
				var oldSubCls = tmp.knownSubTypes.put(subClsName,subCls);
				if(oldSubCls != null && oldSubCls != subCls) {
					throw new Error("subclass name "+subClsName+" is used twice ("+subCls+" and "+oldSubCls+")");
				}
				if(oldSubCls == null) tmp.knownSubTypesPub = List.copyOf(tmp.knownSubTypes.values());
			}
		}
		else cls = subCls;
		if(cls.getSuperclass() != null) addKnownSubType(cls.getSuperclass(),subCls,subClsName);
		for(var e : cls.getInterfaces()) addKnownSubType(e,subCls,subClsName);
	}
	
	private CacheValue createDescriptor(Type type, Type originalType, BiConsumer<SerializerTypeDescriptors,Type> addPending) {
		type = wrapPrimitiveType(type); originalType = wrapPrimitiveType(originalType);
		if(type == Object.class) return new CacheValue(new IllegalArgumentException(type+" cannot be used in the serializer"));
		if(SObject.class.isAssignableFrom(reflect.getBaseType(type))) {
			if(type != SObject.class && type != SValue.class && type != SList.class && type != SMap.class) throw new Error(errors.unreachable);
			return new CacheValue(SObjectTypeDescriptor.instance,config.language());
		}
		if(reflect.getBaseType(type) == Class.class) {
			@SuppressWarnings("rawtypes")
			Function<String,Class> type2name = name -> name == null || name.isEmpty() ? SObject.class : (Class<?>)this.getTypeForName(name);
			return new CacheValue(new ConvertTypeDescriptor("type",Class.class,String.class,this::getNameForType,type2name),config.language());
		}
		if(reflect.getBaseType(type) == Type.class) {
			Function<String,Type> type2name = name -> name == null || name.isEmpty() ? SObject.class : this.getTypeForName(name);
			return new CacheValue(new ConvertTypeDescriptor("type",Type.class,String.class,this::getNameForType,type2name),config.language());
		}
		for(var factory : config.factories) {
			TypeDescriptor ret;
			try { ret = factory.create(type);
			} catch(RuntimeException exc) { return new CacheValue(exc); }
			if(ret == null) continue;
			if(ret.type != reflect.getBaseType(type)) throw new Error(errors.typeMismatch(type,ret.type,"factory: created wrong type"));
			switch(ret.kind()) {
			case list: addPending.accept(this,((ListTypeDescriptor)ret).elementType); break;
			case map:
				var m = (MapTypeDescriptor)ret;
				if(m.keyType != String.class) {
					var keyType = getDescriptor0(m.keyType).type;
					if(!isMapKeyType(keyType)) throw new Error("cannot convert from "+m.keyType+" to "+String.class);
				}
				addPending.accept(this,m.keyType); addPending.accept(this,m.valueType);
				break;
			case convert:
				if(type != originalType) {
					throw new Error(originalType+" uses "+ConvertTypeDescriptor.class+" more than once recursively, " +
							"combine the according functions if you really need to do that");
				}
				var c1 = (ConvertTypeDescriptor)ret;
				var serializer = this;
				if(c1.internalSerializer != null) {
					serializer = c1.internalSerializer;
					if(!isCompatibleTo(serializer)) throw new Error("incompatible serializers");
				}
				else if(c1.type == c1.internalType) {
					@SuppressWarnings({ "unchecked", "rawtypes" })
					var unused = new ConvertTypeDescriptor(null,(Class)c1.type,(Class)c1.type,x->x,x->x);
					unused.kind(); throw new Error(errors.unreachable);
				}
				var c2 = serializer.createDescriptor(c1.internalType,type,addPending);
				if(c2.exc != null) return new CacheValue(c2.exc);
				addPending.accept(serializer,c1.internalType);
				break;
			case object:
				for(var f : ((ObjectTypeDescriptor)ret).fields()) addPending.accept(this,f.genericType);
				for(var sub : new ClassCrawler().getAllSubclasses(ret.type)) addPending.accept(this,sub);
				break;
			case primitive: break;
			default: throw new Error(errors.unreachable);
			}
			return new CacheValue(ret,config.language());
		}
		return new CacheValue(new IllegalArgumentException("no factory found"));
	}
	
	protected abstract boolean isMapKeyType(TypeDescriptor t);
	
	private Type wrapPrimitiveType(Type t) { return t instanceof Class<?> cls ? reflect.primitiveMapInv.getOrDefault(cls,cls) : t; }
}
