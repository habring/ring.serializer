package ring.serializer;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.io.InputStream;
import java.io.OutputStream;

import ring.io.ByteChannel;
import ring.io.InputStreamByteChannel;
import ring.io.OutputStreamByteChannel;
import ring.io.STokenChannel;

/**A {@link Language} that reads and writes from/to a {@link ByteChannel}.*/
public abstract class ByteLanguage extends Language {
	public ByteLanguage(LanguageFeature...features) { super(features); }
	
	@Override public final STokenChannel createChannel(InputStream in) { return createChannel(new InputStreamByteChannel(in)); }
	
	@Override public final STokenChannel createChannel(OutputStream out) { return createChannel(new OutputStreamByteChannel(out)); }
	
	/**Wraps the given channel to read and write tokens from/to it.
	 * @see ByteChannel#wrap(java.nio.channels.ByteChannel)*/
	public final STokenChannel createChannel(java.nio.channels.ByteChannel ch) { return createChannel(ByteChannel.wrap(ch)); }
	
	/**Wraps the given channel to read and write tokens from/to it.*/
	public abstract STokenChannel createChannel(ByteChannel ch);
}
