package ring.serializer.token;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import static ring.serializer.token.STokenTypeFlags.*;

import java.util.HashSet;

/**The type of a {@link SToken}.*/
public enum STokenType {
	startObject(objFlag|startFlag), endObject(objFlag),
	fieldName(objFlag|fldFlag), fieldSeperator(objFlag|sepFlag),
	startList(listFlag|startFlag), endList(listFlag), elemSeperator(listFlag|sepFlag),
	primitiveValue(0), endOfInput(objFlag|listFlag);
	
	public final byte id;
	
	private STokenType(int id) {
		this.id = (byte)id;
		if(this.id != id) throw new Error();
	}
	
	public static STokenType getById(int id) {
		var ret = getById0((byte)id);
		return ret.id == id ? ret : null;
	}
	
	private static STokenType getById0(byte id) {
		if((id&listFlag) != 0) {
			if((id&startFlag) != 0) return startList;
			if((id&sepFlag) != 0) return elemSeperator;
			return (id&objFlag) != 0 ? endOfInput : endList;
		}
		else if((id&objFlag) != 0) {
			if((id&fldFlag) != 0) return fieldName;
			if((id&startFlag) != 0) return startObject;
			return (id&sepFlag) != 0 ? fieldSeperator : endObject;
		}
		else return primitiveValue;
	}
	
	static {
		var tmp = new HashSet<Byte>();
		for(var e : values()) {
			if(!tmp.add(e.id) || getById(e.id) != e) throw new Error();
		}
	}
}
