package ring.serializer.token;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.Objects;

import ring.serializer.Language;
import ring.serializer.Language.NeedsAnnotations;
import ring.serializer.Language.NeedsSkippedFields;
import ring.serializer.Serializer;
import ring.util.ErrorMessages;
import ring.util.ReflectionUtil;

/**The {@link Serializer} serializes an object into a stream of {@link SToken}s.<br><br>
 * language = object | list | {@link STokenType#primitiveValue}<br>
 * object = {@link STokenType#startObject} fieldList {@link STokenType#endObject}<br>
 * fieldList = [ fieldDecl { {@link STokenType#fieldSeperator} fieldDecl } ]<br>
 * fieldDecl = {@link STokenType#fieldName} language<br>
 * list = {@link STokenType#startList} elemList {@link STokenType#endList}<br>
 * elemList = [ language { {@link STokenType#elemSeperator} language } ]<br><br>
 * The underlying stream may or may not distinguish between {@link STokenType#fieldSeperator} and {@link STokenType#elemSeperator}.<br>
 * The {@link STokenType#endOfInput} is given as the last token of one serialization. It may be ignored by the implementation.<br>
 * An implementation may or may not produce a {@link STokenType#endOfInput} when reading. A parser must therefore be flexible at this point.*/
public final class SToken {
	private static final ErrorMessages errors = new ErrorMessages();
	private static final ReflectionUtil reflect = new ReflectionUtil();
	/**Marks an object to be skipped (if requested by the used {@link Language}).
	 * @see NeedsSkippedFields*/
	public static final Object skipValue = new Object();
	/**The type of this token.*/
	public STokenType type;
	/**The field name (in case this token represents a {@link STokenType#fieldName}.*/
	public String fieldName;
	/**The primitive value (in case this token represents a {@link STokenType#primitiveValue}.*/
	public Object primitiveValue;
	/**The annotations (in case the {@link Language} requested some).<br>
	 * Null and empty collections must be treated as a default value, as explained in {@link NeedsAnnotations}.*/
	public Collection<Annotation> annotations;
	
	public SToken(STokenType type) { this.type = type; }
	
	public static SToken createFieldName(String name) {
		var ret = new SToken(STokenType.fieldName); ret.fieldName = Objects.requireNonNull(name); return ret;
	}
	
	public static SToken createSkippedValue() { return createPrimitiveValue(skipValue); }
	
	public static SToken createPrimitiveValue(Object value) {
		var ret = new SToken(STokenType.primitiveValue); ret.primitiveValue = value; return ret;
	}
	
	public void checkValid() {
		if(type == null) throw new IllegalStateException("type == null");
		var needFname = type == STokenType.fieldName;
		if(needFname != (fieldName != null)) {
			throw new IllegalStateException("field name "+(needFname?"==":"!=")+" null");
		}
		if(type != STokenType.primitiveValue && primitiveValue != null) {
			throw new IllegalStateException("primitive value != null");
		}
		if(annotations != null) {
			switch(type) {
			case startObject: case fieldName: case startList: case primitiveValue: break;
			default: throw new IllegalStateException(errors.invalidToken(this,STokenType.startObject,STokenType.fieldName,STokenType.startList,STokenType.primitiveValue));
			}
		}
	}
	
	@Override public int hashCode() { return Objects.hashCode(type) ^ Objects.hashCode(fieldName) ^ Objects.hashCode(primitiveValue); }
	
	@Override public boolean equals(Object obj) { return obj instanceof SToken t && equals(t); }
	
	public boolean equals(SToken tok) {
		return tok != null && type == tok.type && Objects.equals(fieldName,tok.fieldName) && Objects.equals(primitiveValue,tok.primitiveValue);
	}
	
	@Override
	public String toString() {
		var b = new StringBuilder().append(type);
		if(type == STokenType.fieldName) b.append("(\"").append(fieldName).append("\")");
		else if(type == STokenType.primitiveValue) {
			if(primitiveValue instanceof String) b.append("(\"").append(primitiveValue).append("\")");
			else if(primitiveValue == null) b.append("(null)");
			else {
				var cls = primitiveValue.getClass();
				b.append('<').append(reflect.primitiveMap.getOrDefault(cls,cls).getSimpleName()).append(">(");
				b.append(primitiveValue).append(')');
			}
		}
		return b.toString();
	}
}
