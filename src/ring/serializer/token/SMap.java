package ring.serializer.token;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.util.*;

public final class SMap extends AbstractMap<String,SObject> implements SObject {
	private final Map<String,SObject> data = new LinkedHashMap<>();
	
	public SMap() {}
	
	public SMap(Map<String,? extends SObject> m) { putAll(m); }
	
	@Override public Kind kind() { return Kind.map; }
	
	@Override public boolean containsKey(Object key) { return data.containsKey(key); }
	
	@Override public SObject get(Object key) { return data.get(key); }
	
	@Override public SObject put(String key, SObject value) { return data.put(Objects.requireNonNull(key),value == null ? new SValue() : value); }
	
	@Override public SObject remove(Object key) { return data.remove(key); }
	
	@Override public void clear() { data.clear(); }
	
	@Override public Set<Entry<String,SObject>> entrySet() { return data.entrySet(); }
	
	@Override public int hashCode() { return data.hashCode(); }
	
	@Override public boolean equals(Object obj) { return obj instanceof SMap m && data.equals(m.data); }
	
	@Override public String toString() { return data.toString(); }
}
