package ring.serializer.token;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

public final class SList extends AbstractList<SObject> implements SObject {
	private final List<SObject> data = new ArrayList<>();
	
	public SList() {}
	
	public SList(List<? extends SObject> l) { addAll(l); }
	
	@Override public Kind kind() { return Kind.list; }
	
	@Override public int size() { return data.size(); }
	
	@Override public SObject get(int i) { return data.get(i); }
	
	@Override public SObject set(int i, SObject elem) { return data.set(i,elem == null ? new SValue() : elem); }
	
	@Override public void add(int i, SObject elem) { data.add(i,elem == null ? new SValue() : elem); }
	
	@Override public SObject remove(int i) { return data.remove(i); }
	
	@Override public void clear() { data.clear(); }
	
	@Override public int hashCode() { return data.hashCode(); }
	
	@Override public boolean equals(Object obj) { return obj instanceof SList l && data.equals(l.data); }
	
	@Override public String toString() { return data.toString(); }
}
