package ring.serializer.token;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.util.Objects;

public final class SValue implements SObject {
	private Object value;
	
	public SValue() {}
	
	public SValue(Object value) { setValue(value); }
	
	@Override public Kind kind() { return Kind.value; }
	
	public Object value() { return value; }
	
	public void setValue(Object value) { this.value = value; }
	
	@Override public int hashCode() { return Objects.hashCode(value); }
	
	@Override public boolean equals(Object obj) { return obj instanceof SValue v && Objects.equals(value,v.value); }
	
	@Override public String toString() { return Objects.toString(value); }
}
