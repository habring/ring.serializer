package ring.serializer.factory;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.util.Comparator;

import ring.serializer.annot.SField;

/**Default {@link Comparator} implementations.*/
public final class SFieldComparators {
	private SFieldComparators() { throw new UnsupportedOperationException(); }
	
	/**If the declaring class is the same, the {@link SField#order()} determines their order, if not the fields are considered equal.*/
	public static final class Default implements Comparator<SFieldImpl> {
		@Override
		public int compare(SFieldImpl a, SFieldImpl b) {
			return a.declaringClass != b.declaringClass ? 0 : Integer.compare(a.order,b.order);
		}
	}
	
	/**Compares fields by {@link SField#order()}.*/
	public static final class CompareByOrder implements Comparator<SFieldImpl> {
		@Override public int compare(SFieldImpl a, SFieldImpl b) { return Integer.compare(a.order,b.order); }
	}
	
	/**Compares by name only (ignoring the declaring class).*/
	public static final class CompareByName implements Comparator<SFieldImpl> {
		@Override public int compare(SFieldImpl a, SFieldImpl b) { return a.name.compareTo(b.name); }
	}
}
