package ring.serializer.factory;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.lang.reflect.Type;
import java.util.Objects;

import ring.serializer.types.TypeDescriptor;

/**Creates a {@link TypeDescriptor} for a given type.*/
public abstract class TypeDescriptorFactory<T> extends GenericTypeDescriptorFactory {
	public final Class<T> type;
	
	public TypeDescriptorFactory(Class<T> type) { this.type = Objects.requireNonNull(type); }
	
	@Override
	public final TypeDescriptor create(Type genericType) {
		if(!(genericType instanceof Class<?> cls)) return null;
		return type.isAssignableFrom(cls) || type == Object.class ? create(cls.asSubclass(type)) : null;
	}
	
	protected abstract TypeDescriptor create(Class<? extends T> type);
}
