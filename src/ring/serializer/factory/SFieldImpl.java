package ring.serializer.factory;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.util.Objects;

import ring.serializer.annot.SField;

/**Shadows the {@link SField} annotation. These two types are basically the same.<br>
 * This class only exists, because annotations cannot/should not be instantiated manually in java.*/
public final class SFieldImpl {
	public final Class<?> declaringClass;
	public final String name;
	public final int order;
	public final boolean replaceParent, externalTyped, leftover;
	
	public SFieldImpl(Class<?> declaringClass, String name, SField f) {
		this.declaringClass = Objects.requireNonNull(declaringClass);
		this.name = Objects.requireNonNull(name);
		this.order = f.order(); this.replaceParent = f.replaceParent();
		this.externalTyped = f.externalTyped(); this.leftover = f.isLeftover();
	}
	
	@Override
	public int hashCode() {
		return declaringClass.hashCode() ^ name.hashCode() ^ order ^
				Boolean.hashCode(replaceParent) ^ Boolean.hashCode(leftover);
	}
	
	@Override public boolean equals(Object obj) { return obj instanceof SFieldImpl f && equals(f); }
	
	public boolean equals(SFieldImpl f) {
		return f != null && declaringClass == f.declaringClass && name.equals(f.name) &&
				order == f.order && replaceParent == f.replaceParent && leftover == f.leftover;
	}
}
