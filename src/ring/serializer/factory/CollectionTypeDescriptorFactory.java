package ring.serializer.factory;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;

import ring.serializer.types.ListTypeDescriptor;
import ring.serializer.types.MapTypeDescriptor;
import ring.serializer.types.TypeDescriptor;

public final class CollectionTypeDescriptorFactory extends GenericTypeDescriptorFactory {
	private final boolean simplifySingleElements;
	
	public CollectionTypeDescriptorFactory(boolean simplifySingleElements) { this.simplifySingleElements = simplifySingleElements; }
	
	@Override
	public TypeDescriptor create(Type type) {
		if(type instanceof ParameterizedType) {
			var cls = TypeDescriptorFactories.reflect.getBaseType(type);
			if(cls == List.class) return setFlags(new ListTypeDescriptor(type,ArrayList.class));
			if(cls == Set.class) return setFlags(new ListTypeDescriptor(type,HashSet.class));
			if(cls == Map.class) return new MapTypeDescriptor(type,HashMap.class);
		}
		return null;
	}
	
	private ListTypeDescriptor setFlags(ListTypeDescriptor l) { return simplifySingleElements ? l.setSimplifySingleElements() : l; }
}
