package ring.serializer.factory;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.AbstractMap.SimpleEntry;
import java.util.Map.Entry;
import java.util.function.Consumer;

import ring.serializer.Serializer;
import ring.serializer.annot.*;
import ring.serializer.types.*;
import ring.util.ErrorMessages;
import ring.util.ReflectionUtil;

/**Implements {@link SClass} and {@link SField}.*/
public final class ReflectTypeDescriptorFactory<T> extends TypeDescriptorFactory<T> {
	private static final ReflectionUtil reflect = new ReflectionUtil();
	private static final ErrorMessages errors = new ErrorMessages();
	public static final ReflectTypeDescriptorFactory<Object> defaultInstance = new ReflectTypeDescriptorFactory<>(Object.class);
	private final boolean skipDefaultValues;
	
	public ReflectTypeDescriptorFactory(Class<T> type) { this(type,true); }
	
	public ReflectTypeDescriptorFactory(Class<T> type, boolean skipDefaultValues) {
		super(type); this.skipDefaultValues = skipDefaultValues;
	}
	
	@Override
	protected TypeDescriptor create(Class<? extends T> type) {
		if(type.isEnum()) return null;
		var fields1 = findAllFields(type);
		if(fields1.converter != null) return createConv(type,fields1.converter);
		var ret = new ObjectTypeDescriptor(null,type).setValidator(fields1.validator);
		var fields2 = fields1.fields;
		if(fields2.isEmpty() && !type.isAnnotationPresent(SClass.class) && !type.isAnnotationPresent(SSubClass.class)) return null;
		var rootCls = getRoots(type); Class<?> rootAnnotCls = null; SClass rootAnnot = null;
		for(var cls : rootCls) {
			var tmp = cls.getAnnotation(SClass.class);
			if(tmp == null) throw new Error(errors.unreachable);
			if(rootAnnot == null) { rootAnnotCls = cls; rootAnnot = tmp; }
			var err = " mismatch between "+rootAnnotCls+" and "+cls;
			if(!rootAnnot.typeKey().equals(tmp.typeKey())) throw new Error("typeKey"+err);
			if(rootAnnot.ordering() != tmp.ordering()) throw new Error("ordering"+err);
			if(rootAnnot.disableDynamicTypesafety() != tmp.disableDynamicTypesafety()) throw new Error("disable dynamic typesafety"+err);
			if(rootAnnot.ignoreLeftover() != tmp.ignoreLeftover()) throw new Error("ignore leftover"+err);
		}
		if((rootAnnot == null) != rootCls.isEmpty()) throw new Error(errors.unreachable);
		if(ret.isAbstract() && rootAnnotCls != type && type.getAnnotation(SSubClass.class) == null) return null;
		var cmp = getComparator(rootAnnot);
		fields2.sort((a,b)->cmp.compare(a.getKey(),b.getKey()));
		var dynTypeFld = rootCls.isEmpty() ? null : createTypeField(rootAnnotCls,type);
		if(dynTypeFld != null) ret = ret.addField(setFlags(null,dynTypeFld));
		if(ret.name == null) {
			ret = ret.rename(dynTypeFld == null ? defaultName(type) : (String)dynTypeFld.getValue(ret.createInstance()));
		}
		var fields3 = new ArrayList<FieldDescriptor>();
		for(int i = 0; i < fields2.size(); i++) {
			var e = fields2.get(i); var name = e.getKey().name;
			FieldDescriptor f;
			if(e.getValue() instanceof Field fld) f = new ReflectFieldDescriptor(name,fld);
			else if(e.getValue() instanceof Method m) f = new ReflectFieldDescriptor(name,m,ReflectFieldDescriptor.findSetterForGetter(m));
			else throw new Error(errors.unreachable);
			fields3.add(f=setFlags(e.getKey(),f));
			if(f.isExternalDynamicTyped() && fields3.size() > 1) {
				fields3.set(fields3.size()-2,fields3.get(fields3.size()-2).setExternalDynamicTypeInfo());
			}
		}
		if(rootAnnot != null && rootAnnot.ignoreLeftover()) fields3.add(FieldDescriptor.createDummyLeftover("leftover",type));
		return ret.addFields(fields3);
	}
	
	private <U extends T> TypeDescriptor createConv(Class<U> type, Member converter) {
		if(!Modifier.isStatic(converter.getModifiers())) throw new Error(SConvert.class+" member needs to be static in "+converter);
		Object conv;
		try {
			if(converter instanceof Field f) { f.setAccessible(true); conv = f.get(null); }
			else if(converter instanceof Method m) { m.setAccessible(true); conv = m.invoke(null); }
			else throw new Error("unexpected kind of "+SConvert.class+" "+converter);
		} catch(Exception exc) { throw new Error("exception while retrieving "+SConvert.class+" member "+converter,exc); }
		if(conv instanceof Serializer s) return new ConvertTypeDescriptor(defaultName(type),type,type,x->x,x->x,s);
		else if(conv instanceof ConvertTypeDescriptor c && c.type == type) return c.name != null ? c : c.rename(defaultName(type));
		else throw new Error("invalid type "+(conv!=null?conv.getClass():null)+" for "+SConvert.class+" in "+converter);
	}
	
	private static class Fields {
		final List<Entry<SFieldImpl,Member>> fields = new ArrayList<>();
		Method validator; Member converter;
	}
	
	private Fields findAllFields(Class<? extends T> type) {
		var ret = new Fields();
		var todo = getAllSuperclassesInOrder(type);
		for(var cls : todo) {
			if(ret.converter != null) { ret.fields.clear(); ret.validator = null; break; }
			for(var f : cls.getDeclaredFields()) add(ret,f,null);
			for(var m : cls.getDeclaredMethods()) {
				add(ret,m,ReflectFieldDescriptor::checkIsGetter);
				if(!m.isAnnotationPresent(SValidator.class)) continue;
				if(ret.validator == null) ret.validator = m;
				else throw new Error("type "+type+" has multiple validators");
			}
		}
		return ret;
	}
	
	private static List<Class<?>> getAllSuperclassesInOrder(Class<?> type) {
		var m = new HashMap<Integer,List<Class<?>>>();
		var todo = new ArrayList<Entry<Class<?>,Integer>>();
		todo.add(new SimpleEntry<>(type,0));
		while(todo.size() > 0) {
			var e = todo.remove(todo.size()-1);
			m.compute(e.getValue(),(k,v) -> {
				if(v == null) v = new ArrayList<>();
				v.add(e.getKey()); return v;
			});
			var p = e.getKey().getSuperclass();
			if(p != null) todo.add(new SimpleEntry<>(p,e.getValue()+1));
			for(var p2 : e.getKey().getInterfaces()) todo.add(new SimpleEntry<>(p2,e.getValue()+1));
		}
		var l = new ArrayList<>(m.entrySet());
		l.sort(Comparator.comparing(e->-e.getKey()));
		var ret = new ArrayList<Class<?>>();
		for(var x : l) {
			for(var y : x.getValue()) ret.add(y);
		}
		return ret;
	}
	
	private <F extends Member&AnnotatedElement> void add(Fields fields, F fld, Consumer<F> checkValid) {
		var convAnnot = fld.getAnnotation(SConvert.class);
		if(convAnnot != null) { fields.converter = fld; return; }
		var annot = fld.getAnnotation(SField.class);
		if(annot == null) return;
		if(checkValid != null) checkValid.accept(fld);
		var key = annot.value().isEmpty() ? fld.getName() : annot.value();
		fields.fields.add(Map.entry(new SFieldImpl(fld.getDeclaringClass(),key,annot),fld));
	}
	
	private static Set<Class<?>> getRoots(Class<?> type) {
		var ret = new HashSet<Class<?>>(); getRoots(type,ret); return ret;
	}
	
	private static void getRoots(Class<?> type, Set<Class<?>> ret) {
		var typeAnnot = type.getAnnotation(SClass.class);
		if(typeAnnot != null) ret.add(type);
		if(type.getSuperclass() != null) getRoots(type.getSuperclass(),ret);
		for(var e : type.getInterfaces()) getRoots(e,ret);
	}
	
	private static Comparator<SFieldImpl> getComparator(SClass annot) {
		return reflect.createInstance(annot != null ? annot.ordering() : SFieldComparators.Default.class);
	}
	
	private static FieldDescriptor createTypeField(Class<?> rootCls, Class<?> type) {
		var annot = rootCls.getAnnotation(SClass.class);
		var subAnnot = type.getAnnotation(SSubClass.class);
		if(annot == null) throw new Error(errors.unreachable);
		if(rootCls != type && (annot.disableDynamicTypesafety() ? subAnnot != null : subAnnot == null)) {
			throw new Error(type+" must "+(annot.disableDynamicTypesafety()?"not ":"")+"have the "+SSubClass.class+" annotation");
		}
		if(subAnnot == null) return null;
		var value = subAnnot.value().isEmpty() ? defaultName(type) : subAnnot.value();
		return new FunctionFieldDescriptor<>(annot.typeKey(),type,String.class,e->value,null).setDynamicTypeInfo();
	}
	
	private FieldDescriptor setFlags(SFieldImpl annot, FieldDescriptor f) {
		var skip = skipDefaultValues;
		if(annot != null) {
			if(annot.replaceParent) { f = f.setReplaceParent(); skip = false; }
			if(annot.externalTyped) f = f.setExternalDynamicTyped();
			if(annot.leftover) f = f.setLeftover();
		}
		return skip ? f.setSkipIfDefaultValue() : f;
	}
	
	private static String defaultName(Class<?> type) {
		var annot = type.getAnnotation(SClass.class);
		if(annot != null && !annot.value().isEmpty()) return annot.value();
		var subAnnot = type.getAnnotation(SSubClass.class);
		if(subAnnot != null && !subAnnot.value().isEmpty()) return subAnnot.value();
		var s = type.getSimpleName();
		int i;
		for(i = 0; i < s.length(); i++) {
			if(!Character.isUpperCase(s.charAt(i))) break;
		}
		if(i > 1) i--;
		return s.substring(0,i).toLowerCase() + s.substring(i);
	}
}
