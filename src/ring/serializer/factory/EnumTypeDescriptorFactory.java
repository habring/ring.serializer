package ring.serializer.factory;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;

import ring.serializer.annot.SField;
import ring.serializer.types.ConvertTypeDescriptor;
import ring.serializer.types.TypeDescriptor;

public class EnumTypeDescriptorFactory extends TypeDescriptorFactory<Enum<?>> {
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public EnumTypeDescriptorFactory() { super((Class)Enum.class); }
	
	@Override protected final TypeDescriptor create(Class<? extends Enum<?>> type) { return create0(type); }
	
	protected <T extends Enum<?>> TypeDescriptor create0(Class<T> type) { return create0(type,String.class,x->x,x->x); }
	
	protected final <T extends Enum<?>,I> ConvertTypeDescriptor create0(
			Class<T> type, Class<I> internalType, Function<String,I> serialize, Function<I,String> deserialize) {
		Objects.requireNonNull(internalType); Objects.requireNonNull(serialize); Objects.requireNonNull(deserialize);
		if(!type.isEnum()) return null;
		var names = getNames(type); var values = type.getEnumConstants(); var name2Enum = group(names,(k,i)->values[i]);
		return new ConvertTypeDescriptor(TypeDescriptorFactories.getName(type),type,internalType,e->serialize.apply(names[e.ordinal()]),a -> {
			var b = deserialize.apply(a);
			T c = b.isEmpty() && values.length > 0 ? values[0] : name2Enum.get(b);
			if(c == null) throw new IllegalArgumentException(b);
			return c;
		});
	}
	
	protected final <T extends Enum<?>> String[] getNames(Class<T> type) {
		var names = new String[type.getEnumConstants().length];
		var names2 = new HashSet<String>();
		for(var elem : type.getEnumConstants()) {
			var name = elem.name();
			try {
				var fld = type.getField(elem.name());
				var annot = fld.getAnnotation(SField.class);
				if(annot != null && !annot.value().isEmpty()) name = annot.value();
			} catch (Exception exc) { throw new Error(exc); }
			names[elem.ordinal()] = name;
			if(!names2.add(name)) throw new IllegalArgumentException("enum constant "+name+" is declared twice");
		}
		return names;
	}
	
	private static <K,V> Map<K,V> group(K[] key, BiFunction<K,Integer,V> key2Value) {
		var ret = new HashMap<K,V>(); Objects.requireNonNull(key2Value);
		for(int i = 0; i < key.length; i++) ret.put(key[i],key2Value.apply(key[i],i));
		return Map.copyOf(ret);
	}
}
