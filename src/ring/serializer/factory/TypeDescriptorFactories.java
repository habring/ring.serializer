package ring.serializer.factory;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.util.*;
import java.util.function.Function;

import ring.serializer.types.ConvertTypeDescriptor;
import ring.serializer.types.PrimitiveTypeDescriptor;
import ring.serializer.types.TypeDescriptor;
import ring.util.HexUtil;
import ring.util.ReflectionUtil;

public final class TypeDescriptorFactories {
	static final ReflectionUtil reflect = new ReflectionUtil();
	
	private TypeDescriptorFactories() { throw new UnsupportedOperationException(); }
	
	/**Converts all primitive types to four types that need to be treated by the underlying language:
	 * boolean, long and double; chars are converted into strings.*/
	public static final GenericTypeDescriptorFactory primitives = new SimpleTypeDescriptorFactory(
			prim("bool",Boolean.class), convByte(), convChar(), convShort(), convInt(), prim(Long.class), convFloat(), prim(Double.class)
	);
	
	/**Serializes strings as primitive type.*/
	public static final GenericTypeDescriptorFactory strings = new SimpleTypeDescriptorFactory(prim(String.class));
	
	/**Serializes byte-arrays as strings.*/
	public static final GenericTypeDescriptorFactory byteArrays = new SimpleTypeDescriptorFactory(convByteArray());
	
	/**Serializes enums as strings.*/
	public static final GenericTypeDescriptorFactory enums = new EnumTypeDescriptorFactory();
	
	/**Creates type descriptors for {@link List}s, {@link Set}s and {@link Map}s.*/
	public static final GenericTypeDescriptorFactory collections = new CollectionTypeDescriptorFactory(true);
	
	/**Contains all default type descriptor factories.*/
	public static final List<GenericTypeDescriptorFactory> defaultInstances = Collections.unmodifiableList(Arrays.asList(
			collections,enums,strings,primitives,byteArrays,ReflectTypeDescriptorFactory.defaultInstance));
	
	private static TypeDescriptor prim(Class<?> type) { return prim(getName(type),type); }
	
	private static TypeDescriptor prim(String name, Class<?> type) { return new PrimitiveTypeDescriptor(name,type); }
	
	private static TypeDescriptor convByte() {
		return convInt(Byte.class,e->(long)e,e -> {
			if((byte)(long)e == e) return (byte)(long)e;
			throw new ArithmeticException("integer overflow");
		});
	}
	
	private static TypeDescriptor convByteArray() {
		return new ConvertTypeDescriptor("ByteArray",byte[].class,String.class,HexUtil::toHexString,HexUtil::parseHexString);
	}
	
	private static TypeDescriptor convChar() {
		return new ConvertTypeDescriptor(getName(Character.class),Character.class,String.class,e->Character.toString(e),e -> {
			if(e.length() == 1) return e.charAt(0);
			if(e.isEmpty()) return '\0';
			throw new IllegalArgumentException("string \""+e+"\" is expected to be length 1 to be converted to a char");
		});
	}
	
	private static TypeDescriptor convShort() {
		return convInt(Short.class,e->(long)e,e -> {
			if((short)(long)e == e) return (short)(long)e;
			throw new ArithmeticException("integer overflow");
		});
	}
	
	private static TypeDescriptor convInt() {
		return convInt(Integer.class,e->(long)e,e -> {
			if((int)(long)e == e) return (int)(long)e;
			throw new ArithmeticException("integer overflow");
		});
	}
	
	private static <T> TypeDescriptor convFloat() {
		return new ConvertTypeDescriptor(getName(Float.class),Float.class,Double.class,e->(double)e,e->(float)(double)e);
	}
	
	private static <T> TypeDescriptor convInt(Class<T> type, Function<T,Long> serialize, Function<Long,T> deserialize) {
		return new ConvertTypeDescriptor(getName(type),type,Long.class,serialize,deserialize);
	}
	
	static String getName(Class<?> type) {
		type = reflect.primitiveMap.getOrDefault(type,type);
		return type.getSimpleName().toLowerCase();
	}
}
