package ring.serializer.factory;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.lang.reflect.Type;
import java.util.*;

import ring.serializer.types.TypeDescriptor;

/**A simple factory that just returns predefined instances.*/
public final class SimpleTypeDescriptorFactory extends GenericTypeDescriptorFactory {
	public final Map<Type,TypeDescriptor> data;
	
	public SimpleTypeDescriptorFactory(TypeDescriptor...data) { this(Arrays.asList(data)); }
	
	public SimpleTypeDescriptorFactory(Collection<TypeDescriptor> data) {
		var tmp = new HashMap<Type,TypeDescriptor>();
		for(var e : data) {
			if(tmp.put(e.genericType,e) != null) throw new IllegalArgumentException(e.genericType+" is given twice");
		}
		if(tmp.containsValue(null)) throw new NullPointerException();
		this.data = Collections.unmodifiableMap(tmp);
	}
	
	@Override public TypeDescriptor create(Type genericType) { return data.get(genericType); }
}
