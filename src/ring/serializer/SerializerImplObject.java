package ring.serializer;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import static ring.serializer.token.STokenType.*;

import java.io.IOException;
import java.lang.reflect.Type;

import ring.io.STokenChannel;
import ring.serializer.token.SMap;
import ring.serializer.token.SObject;
import ring.serializer.token.SToken;
import ring.serializer.token.SValue;
import ring.serializer.types.FieldDescriptor;
import ring.serializer.types.ObjectTypeDescriptor;

abstract class SerializerImplObject extends SerializerImplConv {
	private static final String dynTypeFirstError = "dynamic type info must be given as the first field";
	
	SerializerImplObject(SerializerConfig config) { super(config); }
	
	final int hashCode0(ObjectTypeDescriptor type, Object obj, int maxDepth) {
		int ret = 1;
		var tmp = getDescriptor0(obj.getClass());
		var dynType = (ObjectTypeDescriptor)tmp.type;
		for(var it = dynType.fields().iterator(); it.hasNext(); ) {
			var f = it.next();
			var value = f.getValue(obj);
			if(skip(type,tmp,f,obj,value)) continue;
			ret = 31*ret + hashCode0(getType(f,value),value,maxDepth);
		}
		return ret;
	}
	
	final Boolean equals0(ObjectTypeDescriptor type, Object obj1, Object obj2, int maxDepth) {
		if(obj1.getClass() != obj2.getClass()) return false;
		var tmp = getDescriptor0(obj1.getClass());
		var dynType = (ObjectTypeDescriptor)tmp.type;
		for(var it = dynType.fields().iterator(); it.hasNext(); ) {
			var f = it.next();
			var value1 = f.getValue(obj1); var value2 = f.getValue(obj2);
			var skip = skip(type,tmp,f,obj1,value1);
			if(skip != skip(type,tmp,f,obj2,value2)) return false;
			if(skip) continue;
			var eq = equals0(getType(f,value1),value1,value2,maxDepth);
			if(eq == null || !eq) return eq;
		}
		return true;
	}
	
	final void copy0(ObjectTypeDescriptor type, Object src, Object dest, int maxDepth) {
		var tmp = getDescriptor0(type.type);
		for(var f : type.fields()) {
			if(type.fieldsByName().containsKey(f.name) || !f.canSetValue()) continue;
			setValue(f,dest,clone0(f.getValue(tmp.defaultValue),maxDepth));
		}
		for(var f : type.fields()) {
			var ftype = getType(f,src);
			var oldValue = f.getValue(dest);
			var newValue = copy0(ftype,f.getValue(src),oldValue,maxDepth);
			setValue(f,dest,newValue);
		}
		type.checkValid(dest);
	}
	
	final void readObjectInput0(SToken la, STokenChannel in) throws IOException {
		errors.checkToken(startObject,la);
		var first = true;
		while(true) {
			var tok = readTok(in);
			if(tok.type == endObject) break;
			if(!first) { checkSeperator(tok); tok = readTok(in); }
			errors.checkToken(fieldName,tok);
			first = false;
			var la2 = readTok(in);
			if(la2.type == primitiveValue && la2.primitiveValue == SToken.skipValue) continue;
			readInput0(la2,in);
		}
	}
	
	final Object deserialize0(ObjectTypeDescriptor type, SToken la, STokenChannel in) throws IOException {
		if(type.fieldReplacesParent()) {
			var f = type.fields().get(0);
			var value = deserialize0(f.genericType,la,in);
			Object obj = type.createInstance();
			setValue(f,obj,value);
			type.checkValid(obj); return obj;
		}
		errors.checkToken(startObject,la);
		Object ret = null; var first = true; SMap leftover = null;
		while(true) {
			var tok = readTok(in);
			if(tok.type == endObject) break;
			if(!first) { checkSeperator(tok); tok = readTok(in); }
			errors.checkToken(fieldName,tok);
			var f = type.fieldsByName().get(tok.fieldName);
			if(f == null || f.isDynamicTypeInfo()) {
				if(f == null && type.hasLeftoverField()) {
					if(leftover == null) leftover = new SMap();
					var value = (SObject)deserialize0(SObject.class,null,in);
					leftover.put(tok.fieldName,value); first = false; continue;
				}
				if(f == null && !first) throw new IllegalArgumentException(fldNotFound(type,tok.fieldName));
				if(!first) throw new IllegalArgumentException(dynTypeFirstError);
				var subType = errors.checkToken(primitiveValue,readTok(in));
				if(!(subType.primitiveValue instanceof String str)) {
					throw new ClassCastException(errors.typeMismatch(String.class,subType.primitiveValue.getClass()));
				}
				type = getSubTypeByName(type.type,str);
				var f2 = type.fieldsByName().get(tok.fieldName);
				if(f2 == null || !f2.isDynamicTypeInfo()) throw new IllegalArgumentException(fldNotFound(type,tok.fieldName));
				first = false; continue;
			}
			first = false;
			if(ret == null) ret = type.createInstance();
			var la2 = readTok(in);
			if(la2.type == primitiveValue && la2.primitiveValue == SToken.skipValue) continue;
			var ftype = getType(f,ret);
			var value = deserialize0(ftype,la2,in);
			setValue(f,ret,value);
		}
		if(ret == null) ret = type.createInstance();
		if(leftover != null) setValue(type.leftoverField(),ret,leftover);
		type.checkValid(ret); return ret;
	}
	
	final Object deserializeS0(ObjectTypeDescriptor type, SObject obj) {
		if(type.fieldReplacesParent()) {
			var f = type.fields().get(0);
			var value = deserializeS0(f.genericType,obj);
			var ret = type.createInstance();
			setValue(f,ret,value);
			type.checkValid(ret); return ret;
		}
		Object ret = null; var first = true; SMap leftover = null;
		for(var e : ((SMap)checkSKind(obj,SObject.Kind.map)).entrySet()) {
			var f = type.fieldsByName().get(e.getKey());
			if(f == null || f.isDynamicTypeInfo()) {
				if(f == null && type.hasLeftoverField()) {
					if(leftover == null) leftover = new SMap();
					var value = (SObject)clone0(e.getValue(),-1);
					leftover.put(e.getKey(),value); continue;
				}
				if(f == null && !first) throw new IllegalArgumentException(fldNotFound(type,e.getKey()));
				if(!first) throw new IllegalArgumentException(dynTypeFirstError);
				var subType = ((SValue)checkSKind(e.getValue(),SObject.Kind.value)).value();
				if(!(subType instanceof String str)) {
					throw new ClassCastException(errors.typeMismatch(String.class,subType.getClass()));
				}
				type = getSubTypeByName(type.type,str);
				var f2 = type.fieldsByName().get(e.getKey());
				if(f2 == null || !f2.isDynamicTypeInfo()) throw new IllegalArgumentException(fldNotFound(type,e.getKey()));
				first = false; continue;
			}
			first = false;
			if(ret == null) ret = type.createInstance();
			if(e.getValue() instanceof SValue v && v.value() == SToken.skipValue) continue;
			var ftype = getType(f,ret);
			var value = deserializeS0(ftype,e.getValue());
			setValue(f,ret,value);
		}
		if(ret == null) ret = type.createInstance();
		if(leftover != null) setValue(type.leftoverField(),ret,leftover);
		type.checkValid(ret); return ret;
	}
	
	private final ObjectTypeDescriptor getSubTypeByName(Class<?> type, String subtypeName) {
		var tmp = getDescriptor0(type);
		if(tmp.knownSubTypes != null) {
			var cls = tmp.knownSubTypes.get(subtypeName);
			if(cls != null) return (ObjectTypeDescriptor)getDescriptor0(cls).type;
		}
		throw new IllegalArgumentException(String.format("subtype %s of type %s not found",subtypeName,type));
	}
	
	final void serialize0(ObjectTypeDescriptor type, Object obj, STokenChannel out) throws IOException {
		var tmp = getDescriptor0(obj.getClass());
		var dynType = (ObjectTypeDescriptor)tmp.type;
		dynType.checkValid(obj);
		if(dynType.fieldReplacesParent()) {
			var f = dynType.fields().get(0);
			var value = f.getValue(obj);
			serialize0(f.genericType,value,out);
			return;
		}
		out.write(setAnnot(new SToken(startObject),tmp.annotations)); var first = true;
		for(var f : dynType.fields()) {
			if(f.isExternalDynamicTypeInfo()) validateExternalDynamicTypeInfo(f,obj);
			var value = f.getValue(obj);
			if(f.isLeftover()) {
				for(var e : ((SMap)value).entrySet()) {
					if(!first) out.write(new SToken(fieldSeperator));
					out.write(SToken.createFieldName(e.getKey()));
					serialize0(SObject.class,e.getValue(),out);
					first = false;
				}
				continue;
			}
			var skip = skip(type,tmp,f,obj,value);
			if(skip && !language().needsSkippedFields) continue;
			if(!first) out.write(new SToken(fieldSeperator));
			out.write(setAnnot(SToken.createFieldName(f.name),tmp.fldAnnotations.get(f.parentIndex())));
			if(skip) out.write(SToken.createSkippedValue());
			else serialize0(getType(f,obj),value,out);
			first = false;
		}
		out.write(new SToken(endObject));
	}
	
	final SObject serializeS0(ObjectTypeDescriptor type, Object obj) {
		var tmp = getDescriptor0(obj.getClass());
		var dynType = (ObjectTypeDescriptor)tmp.type;
		dynType.checkValid(obj);
		if(dynType.fieldReplacesParent()) {
			var f = dynType.fields().get(0);
			var value = f.getValue(obj);
			return serializeS0(f.genericType,value);
		}
		var ret = new SMap();
		for(var f : dynType.fields()) {
			if(f.isExternalDynamicTypeInfo()) validateExternalDynamicTypeInfo(f,obj);
			var value = f.getValue(obj);
			if(f.isLeftover()) { ret.putAll((SMap)clone0(value,-1)); continue; }
			var skip = skip(type,tmp,f,obj,value);
			if(skip && !language().needsSkippedFields) continue;
			SObject value2;
			if(skip) value2 = new SValue(SToken.skipValue);
			else value2 = serializeS0(getType(f,obj),value);
			ret.put(f.name,value2);
		}
		return ret;
	}
	
	private boolean skip(ObjectTypeDescriptor staticType, CacheValue dynamicType, FieldDescriptor f, Object obj, Object value) {
		if(!f.skipIfDefaultValue()) return false;
		if(dynamicType.type != f.parent()) throw new Error(errors.unreachable);
		if(f.isDynamicTypeInfo()) return obj.getClass() == staticType.type;
		var defaultValue = f.getValue(dynamicType.defaultValue);
		var eq = equals0(f.genericType,value,defaultValue,narrowDepth);
		return eq != null && eq;
	}
	
	private Type getType(FieldDescriptor f, Object obj) {
		if(f.isExternalDynamicTyped()) return getExternalDynamicTypeInfo(f.prev(),obj);
		else return f.genericType;
	}
	
	private static String fldNotFound(ObjectTypeDescriptor type, String fieldName) { return "field "+fieldName+" not found on type "+type.type; }
	
	public final void setValue(FieldDescriptor f, Object obj, Object value) {
		if(!trySetValue(f,obj,value)) throw new IllegalArgumentException("cannot set field "+f);
	}
	
	public final boolean trySetValue(FieldDescriptor f, Object obj, Object value) {
		if(f.canSetValue()) { f.setValue(obj,value); return true; }
		var oldValue = f.getValue(obj);
		if(oldValue == value) return true;
		var newValue = copy0(f.genericType,value,oldValue,1);
		return newValue == oldValue || equals0(f.genericType,oldValue,value,-1);
	}
	
	abstract Type validateExternalDynamicTypeInfo(FieldDescriptor f, Object obj);
	
	abstract Type getExternalDynamicTypeInfo(FieldDescriptor f, Object obj);
}
