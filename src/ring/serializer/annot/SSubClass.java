package ring.serializer.annot;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**This annotation is very similar to {@link SClass}, except that it can only be used
 * in classes of a dynamic type hierarchy. To be specific, if this annotation is present
 * on a class, a superclass or superinterface is searched that has the {@link SClass} annotation
 * on it. An exception is thrown if none is found.
 * @see SClass*/
@Retention(RUNTIME)
@Target(TYPE)
@Documented
public @interface SSubClass {
	/**The name of this class. Leave empty to use the java class name.
	 * @see Class#getSimpleName()*/
	String value() default "";
}
