package ring.serializer.annot;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.Comparator;

import ring.serializer.factory.ReflectTypeDescriptorFactory;
import ring.serializer.factory.SFieldComparators;
import ring.serializer.factory.SFieldImpl;
import ring.serializer.types.FieldDescriptor;

/**Use this annotation on a class to explicitly mark it as serializable.<br>
 * Note that also a {@link SField} annotation somewhere in the class is enough.<br>
 * In a dynamic type hierarchy, use this annotation for the root and {@link SSubClass} for all other classes.<br>
 * Implementation is found in {@link ReflectTypeDescriptorFactory}.*/
@Retention(RUNTIME)
@Target(TYPE)
@Documented
public @interface SClass {
	/**The name of this class. Leave empty to use the java class name.
	 * @see Class#getSimpleName()*/
	String value() default "";
	
	/**The name of the virtual field where the type name is exposed.<br>
	 * The class annotated with this annotation does not have such a field
	 * unless it is also annotated with {@link SSubClass}.<br>
	 * Note that this field is always the first one, independent of the sorting order.*/
	String typeKey() default "type";
	
	/**The implementation will call {@link Class#getDeclaredFields()}, append {@link Class#getDeclaredMethods()}
	 * (fields of parent classes are before their children in breath-first order) and sort them.<br>
	 * The sorting order is defined by the given comparator. The default implementation
	 * reorders two fields only if they belong to the same class and have
	 * a different {@link SField#order()} (sorted low to high).<br>
	 * Unfortunately, there is no way in java to get fields and methods in
	 * the order they were declared in source. Currently (May 2020),
	 * most implementations will however use the declaration order.*/
	Class<? extends Comparator<SFieldImpl>> ordering() default SFieldComparators.Default.class;
	
	/**Disables dynamic type safety completely on this type. Use with caution!*/
	boolean disableDynamicTypesafety() default false;
	
	/**@see FieldDescriptor#isLeftover()
	 * @see FieldDescriptor#createDummyLeftover(String, Class)*/
	boolean ignoreLeftover() default false;
}
