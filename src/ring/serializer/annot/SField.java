package ring.serializer.annot;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import ring.serializer.factory.ReflectTypeDescriptorFactory;
import ring.serializer.types.FieldDescriptor;

/**Use this annotation on a field or method to declare it as field for the serializer.<br>
 * Implementation is found in {@link ReflectTypeDescriptorFactory}.*/
@Retention(RUNTIME)
@Target({ FIELD, METHOD })
@Documented
public @interface SField {
	/**A new name for the underlying field. Leave empty to use the name of the field itself.*/
	String value() default "";
	
	/**@see {@link SClass#ordering()}*/
	int order() default 0;
	
	/**@see FieldDescriptor#replacesParent()*/
	boolean replaceParent() default false;
	
	/**@see FieldDescriptor#isExternalDynamicTyped()*/
	boolean externalTyped() default false;
	
	/**@see FieldDescriptor#isLeftover()*/
	boolean isLeftover() default false;
}
