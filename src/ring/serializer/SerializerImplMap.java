package ring.serializer;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import static ring.serializer.token.STokenType.endObject;
import static ring.serializer.token.STokenType.fieldName;
import static ring.serializer.token.STokenType.fieldSeperator;
import static ring.serializer.token.STokenType.startObject;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ring.io.STokenChannel;
import ring.serializer.token.SMap;
import ring.serializer.token.SObject;
import ring.serializer.token.SToken;
import ring.serializer.types.ConvertTypeDescriptor;
import ring.serializer.types.MapTypeDescriptor;
import ring.serializer.types.ObjectTypeDescriptor;
import ring.serializer.types.TypeDescriptor;

abstract class SerializerImplMap extends SerializerImplList {
	SerializerImplMap(SerializerConfig config) { super(config); }
	
	/**@see Map#hashCode
	 * @see Map.Entry#hashCode*/
	final int hashCode0(MapTypeDescriptor type, Object obj, int maxDepth) {
		int ret = 0;
		for(var it = type.iterator(obj); it.hasNext(); ) {
			var elem = it.next();
			var h1 = elem.getKey().hashCode();
			var h2 = hashCode0(type.valueType,elem.getValue(),maxDepth);
			ret += h1 ^ h2;
		}
		return ret;
	}
	
	/**@see Map#equals
	 * @see Set#equals
	 * @see Map.Entry#equals*/
	final Boolean equals0(MapTypeDescriptor type, Object obj1, Object obj2, int maxDepth) {
		var s1 = type.sizeOf(obj1); var s2 = type.sizeOf(obj2);
		if(s1 != s2) return false;
		for(var it = type.iterator(obj1); it.hasNext(); ) {
			var elem1 = it.next();
			if(elem1.getValue() == null && !type.containsKey(obj2,elem1.getKey())) return false;
			var value2 = type.get(obj2,elem1.getKey());
			var eq = equals0(type.valueType,elem1.getValue(),value2,maxDepth);
			if(eq == null || !eq) return eq;
		}
		return true;
	}
	
	final void copy0(MapTypeDescriptor type, Object src, Object dest, int maxDepth) {
		for(var it2 = type.iterator(dest); it2.hasNext(); ) {
			var e2 = it2.next(); var key2 = transformKey(type,e2.getKey(),type);
			if(!type.containsKey(src,key2)) it2.remove();
		}
		for(var it1 = type.iterator(src); it1.hasNext(); ) {
			var e1 = it1.next(); var key = transformKey(type,e1.getKey(),type);
			var oldValue = type.get(dest,key);
			var newValue = copy0(type.valueType,e1.getValue(),oldValue,maxDepth);
			if(oldValue != newValue) type.put(dest,key,newValue);
		}
	}
	
	final Object deserialize0(MapTypeDescriptor type, SToken la, STokenChannel in) throws IOException {
		errors.checkToken(startObject,la);
		var obj = type.createInstance(); type.clear(obj); var first = true;
		while(true) {
			var tok = readTok(in);
			if(tok.type == endObject) return obj;
			if(!first) { checkSeperator(tok); tok = readTok(in); }
			errors.checkToken(fieldName,tok);
			if(type.containsKey(obj,tok.fieldName)) throw new IllegalArgumentException("field "+tok.fieldName+" is defined twice");
			var key = deserializeKey(type,tok.fieldName);
			var value = deserialize0(type.valueType,null,in);
			type.put(obj,key,value);
			first = false;
		}
	}
	
	final Object deserializeS0(MapTypeDescriptor type, SObject obj) {
		var ret = type.createInstance(); type.clear(ret);
		for(var e : ((SMap)checkSKind(obj,SObject.Kind.map)).entrySet()) {
			var key = deserializeKey(type,e.getKey());
			var value = deserializeS0(type.valueType,e.getValue());
			type.put(ret,key,value);
		}
		return ret;
	}
	
	final void serialize0(MapTypeDescriptor type, List<Annotation> annotations, Object obj, STokenChannel out) throws IOException {
		out.write(setAnnot(new SToken(startObject),annotations)); var first = true;
		for(var it = type.iterator(obj); it.hasNext(); ) {
			if(!first) out.write(new SToken(fieldSeperator));
			var elem = it.next(); var key = serializeKey(type,elem.getKey());
			out.write(SToken.createFieldName(key));
			serialize0(type.valueType,elem.getValue(),out);
			first = false;
		}
		out.write(new SToken(endObject));
	}
	
	final SObject serializeS0(MapTypeDescriptor type, Object obj) {
		var ret = new SMap();
		for(var it = type.iterator(obj); it.hasNext(); ) {
			var elem = it.next();
			var key = serializeKey(type,elem.getKey());
			var value = serializeS0(type.valueType,elem.getValue());
			ret.put(key,value);
		}
		return ret;
	}
	
	private Object transformKey(MapTypeDescriptor srcType, Object key, MapTypeDescriptor destType) {
		var tmp = serializeKey(srcType,key);
		return deserializeKey(destType,tmp);
	}
	
	@Override
	protected final boolean isMapKeyType(TypeDescriptor t) {
		if(t.genericType == String.class) return true;
		else if(t instanceof ConvertTypeDescriptor conv && conv.internalType == String.class) return true;
		else if(t instanceof ObjectTypeDescriptor obj && obj.fieldReplacesParent()) {
			return getDescriptor0(obj.fields().get(0).genericType).type.type == String.class;
		}
		else return false;
	}
	
	private Object deserializeKey(MapTypeDescriptor type, String key) {
		if(type.keyType == String.class) return key;
		var t = getDescriptor0(type.keyType).type;
		if(t instanceof ConvertTypeDescriptor conv) return conv.deserialize(key);
		var obj = (ObjectTypeDescriptor)t;
		var ret = obj.createInstance(); obj.fields().get(0).setValue(ret,key); return ret;
	}
	
	private String serializeKey(MapTypeDescriptor type, Object key) {
		if(type.keyType == String.class) return (String)key;
		// It could happen that a key is used twice here, but this is then the fault of the conversion.
		var t = getDescriptor0(type.keyType).type;
		if(t instanceof ConvertTypeDescriptor conv) return (String)conv.serialize(key);
		return (String)((ObjectTypeDescriptor)t).fields().get(0).getValue(key);
	}
}
