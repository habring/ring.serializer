package ring.serializer;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import static ring.serializer.token.STokenType.endOfInput;
import static ring.serializer.token.STokenType.primitiveValue;
import static ring.serializer.token.STokenType.startList;
import static ring.serializer.token.STokenType.startObject;

import java.io.IOException;
import java.lang.reflect.Type;

import ring.io.STokenChannel;
import ring.serializer.token.*;
import ring.serializer.types.*;
import ring.serializer.types.TypeDescriptor.Kind;

abstract class SerializerImpl extends SerializerImplObject {
	SerializerImpl(SerializerConfig config) { super(config); }
	
	@Override
	final int hashCode0(Type staticType, Object obj, int maxDepth) {
		if(obj == null) return 0;
		if(maxDepth == 0) return 1;
		var type = getDescriptor0(staticType!=null?staticType:obj.getClass()).type;
		switch(type.kind()) {
		case primitive: return obj.hashCode();
		case list: return hashCode0((ListTypeDescriptor)type,obj,maxDepth-1);
		case map: return hashCode0((MapTypeDescriptor)type,obj,maxDepth-1);
		case convert: return hashCode0((ConvertTypeDescriptor)type,obj,maxDepth-1);
		case object: return hashCode0((ObjectTypeDescriptor)type,obj,maxDepth-1);
		case sobject:
			switch(((SObject)obj).kind()) {
			case value: return obj.hashCode();
			case list: return hashCode0(SObjectTypeDescriptor.instance.listType,obj,maxDepth-1);
			case map: return hashCode0(SObjectTypeDescriptor.instance.mapType,obj,maxDepth-1);
			default: throw new Error(errors.unreachable);
			}
		default: throw new Error(errors.unreachable);
		}
	}
	
	@Override
	final Boolean equals0(Type staticType, Object obj1, Object obj2, int maxDepth) {
		if(obj1 == obj2) return true;
		if(obj1 == null || obj2 == null) return false;
		if(maxDepth == 0) return null;
		var type = getDescriptor0(staticType!=null?staticType:obj1.getClass()).type;
		if(staticType == null && obj1.getClass() != obj2.getClass()) {
			var type2 = getDescriptor0(obj2.getClass()).type;
			if(type != type2) return false;
		}
		switch(type.kind()) {
		case primitive: return obj1.equals(obj2);
		case list: return equals0((ListTypeDescriptor)type,obj1,obj2,maxDepth-1);
		case map: return equals0((MapTypeDescriptor)type,obj1,obj2,maxDepth-1);
		case convert: return equals0((ConvertTypeDescriptor)type,obj1,obj2,maxDepth-1);
		case object: return equals0((ObjectTypeDescriptor)type,obj1,obj2,maxDepth-1);
		case sobject:
			if(obj1.getClass() != obj2.getClass()) return false;
			switch(((SObject)obj1).kind()) {
			case value: return obj1.equals(obj2);
			case list: return equals0(SObjectTypeDescriptor.instance.listType,obj1,obj2,maxDepth-1);
			case map: return equals0(SObjectTypeDescriptor.instance.mapType,obj1,obj2,maxDepth-1);
			default: throw new Error(errors.unreachable);
			}
		default: throw new Error(errors.unreachable);
		}
	}
	
	@Override final Object clone0(Object obj, int maxDepth) { return copy0((Type)null,obj,null,maxDepth); }
	
	@Override
	final Object copy0(Type staticType, Object src, Object dest, int maxDepth) {
		if(src == null || maxDepth == 0) return src;
		if(staticType == null) staticType = src.getClass();
		var tmp = getDescriptor0(staticType);
		if(tmp.type.kind() == Kind.object && staticType != src.getClass()) {
			tmp = getDescriptor0(src.getClass());
			if(dest != null && src.getClass() != dest.getClass()) dest = null;
		}
		if(dest == null) dest = tmp.type.createInstance();
		else if(src == dest) return clone0(src,-1);
		switch(tmp.type.kind()) {
		case primitive: return src;
		case list: copy0((ListTypeDescriptor)tmp.type,src,dest,maxDepth-1); return dest;
		case map: copy0((MapTypeDescriptor)tmp.type,src,dest,maxDepth-1); return dest;
		case convert: return copy0((ConvertTypeDescriptor)tmp.type,src,dest,maxDepth-1);
		case object: copy0((ObjectTypeDescriptor)tmp.type,src,dest,maxDepth-1); return dest;
		case sobject:
			switch(((SObject)src).kind()) {
			case value:
				if(dest.getClass() != SValue.class) dest = new SValue();
				((SValue)dest).setValue(((SValue)src).value()); return dest;
			case list:
				if(dest.getClass() != SList.class) dest = new SList();
				copy0(SObjectTypeDescriptor.instance.listType,src,dest,maxDepth-1); return dest;
			case map:
				if(dest.getClass() != SMap.class) dest = new SMap();
				copy0(SObjectTypeDescriptor.instance.mapType,src,dest,maxDepth-1); return dest;
			default: throw new Error(errors.unreachable);
			}
		default: throw new Error(errors.unreachable);
		}
	}
	
	@Override
	final void readInput0(SToken la, STokenChannel in) throws IOException {
		if(la == null) {
			la = readTok(in);
			if(la.type == endOfInput) la = readTok(in);
		}
		switch(la.type) {
		case startObject: readObjectInput0(la,in); return;
		case startList: readListInput0(la,in); return;
		case primitiveValue: return;
		default: throw new IllegalArgumentException(errors.invalidToken(la,startObject,startList,primitiveValue));
		}
	}
	
	@Override
	final Object deserialize0(Type staticType, SToken la, STokenChannel in) throws IOException {
		var tmp = getDescriptor0(staticType);
		if(la == null) {
			la = readTok(in);
			if(la.type == endOfInput) la = readTok(in);
		}
		if(la.type == primitiveValue) {
			switch(tmp.type.kind()) {
			case list: case map: case object:
				var ret = parsePrimitive0(la,tmp.type.type,true);
				if(ret == null) return null;
				break;
			case sobject:
				if(la.primitiveValue == null) return null;
				break;
			default: break;
			}
		}
		switch(tmp.type.kind()) {
		case primitive: return parsePrimitive0(la,tmp.type.type,false);
		case list: return deserialize0((ListTypeDescriptor)tmp.type,la,in);
		case map: return deserialize0((MapTypeDescriptor)tmp.type,la,in);
		case convert: return deserialize0((ConvertTypeDescriptor)tmp.type,la,in);
		case object: return deserialize0((ObjectTypeDescriptor)tmp.type,la,in);
		case sobject:
			switch(la.type) {
			case primitiveValue: return new SValue(errors.checkToken(primitiveValue,la).primitiveValue);
			case startList: return deserialize0(SObjectTypeDescriptor.instance.listType,la,in);
			case startObject: return deserialize0(SObjectTypeDescriptor.instance.mapType,la,in);
			default: throw new IllegalArgumentException(errors.invalidToken(la,primitiveValue,startList,startObject));
			}
		default: throw new Error(errors.unreachable);
		}
	}
	
	@Override
	final Object deserializeS0(Type staticType, SObject obj) {
		if(obj == null || obj.kind() == SObject.Kind.value && ((SValue)obj).value() == null) return null;
		var type = getDescriptor0(staticType).type;
		switch(type.kind()) {
		case primitive: return ((SValue)checkSKind(obj,SObject.Kind.value)).value();
		case list: return deserializeS0((ListTypeDescriptor)type,obj);
		case map: return deserializeS0((MapTypeDescriptor)type,obj);
		case convert: return deserializeS0((ConvertTypeDescriptor)type,obj);
		case object: return deserializeS0((ObjectTypeDescriptor)type,obj);
		case sobject: return clone0(obj,-1);
		default: throw new Error(errors.unreachable);
		}
	}
	
	private Object parsePrimitive0(SToken tok, Class<?> targetType, boolean noError) {
		errors.checkToken(primitiveValue,tok);
		var ret = tok.primitiveValue;
		if(ret == null) return ret;
		if(language().fixupReadValueType != null) {
			ret = language().fixupReadValueType.apply(ret,targetType);
		}
		if(ret == null || targetType == ret.getClass() || noError) return ret;
		throw new ClassCastException(errors.typeMismatch(targetType,tok.primitiveValue.getClass()));
	}
	
	@Override
	final void serialize0(Type staticType, Object obj, STokenChannel out) throws IOException {
		var tmp = getDescriptor0(staticType);
		if(obj == null) { out.write(setAnnot(new SToken(primitiveValue),tmp.annotations)); return; }
		switch(tmp.type.kind()) {
		case primitive:
			if(tmp.type.type != obj.getClass()) throw new ClassCastException(errors.typeMismatch(tmp.type.type,obj.getClass()));
			out.write(setAnnot(SToken.createPrimitiveValue(obj),tmp.annotations));
			return;
		case list: serialize0((ListTypeDescriptor)tmp.type,tmp.annotations,obj,out); return;
		case map: serialize0((MapTypeDescriptor)tmp.type,tmp.annotations,obj,out); return;
		case convert: serialize0((ConvertTypeDescriptor)tmp.type,obj,out); return;
		case object: serialize0((ObjectTypeDescriptor)tmp.type,obj,out); return;
		case sobject:
			switch(((SObject)obj).kind()) {
			case value: out.write(SToken.createPrimitiveValue(((SValue)obj).value())); return;
			case list: serialize0(SObjectTypeDescriptor.instance.listType,tmp.annotations,obj,out); return;
			case map: serialize0(SObjectTypeDescriptor.instance.mapType,tmp.annotations,obj,out); return;
			default: throw new Error(errors.unreachable);
			}
		default: throw new Error(errors.unreachable);
		}
	}
	
	@Override
	final SObject serializeS0(Type staticType, Object obj) {
		if(obj == null) return null;
		var type = getDescriptor0(staticType).type;
		switch(type.kind()) {
		case primitive: return new SValue(obj);
		case list: return serializeS0((ListTypeDescriptor)type,obj);
		case map: return serializeS0((MapTypeDescriptor)type,obj);
		case convert: return serializeS0((ConvertTypeDescriptor)type,obj);
		case object: return serializeS0((ObjectTypeDescriptor)type,obj);
		case sobject: return (SObject)clone0(obj,-1);
		default: throw new Error(errors.unreachable);
		}
	}
}
