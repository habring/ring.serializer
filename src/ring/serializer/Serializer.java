package ring.serializer;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import static ring.serializer.token.STokenType.endOfInput;

import java.io.*;
import java.lang.reflect.Type;
import java.util.*;

import ring.io.ByteChannel;
import ring.io.CharChannel;
import ring.io.STokenChannel;
import ring.serializer.token.SObject;
import ring.serializer.token.SToken;
import ring.serializer.types.FieldDescriptor;
import ring.serializer.types.ObjectTypeDescriptor;
import ring.serializer.types.TypeDescriptor;

/**Provides methods to serialize and deserialize objects into a stream of {@link SToken}s.*/
public final class Serializer extends SerializerImpl {
	public Serializer(SerializerConfig config) { super(config); }
	
	public final ByteLanguage byteLanguage() {
		if(language() instanceof ByteLanguage ret) return ret;
		throw new UnsupportedOperationException("this operation needs a "+ByteLanguage.class+" to work");
	}
	
	public final CharLanguage charLanguage() {
		if(language() instanceof CharLanguage ret) return ret;
		throw new UnsupportedOperationException("this operation needs a "+CharLanguage.class+" to work");
	}
	
	/**Creates a new {@link Serializer} with the given {@link Language}.
	 * Users should store and reuse the resulting {@link Serializer} if it is used more than once.*/
	public final Serializer withLanguage(Language l) { return new Serializer(config().setLanguage(l)); }
	
	public final STokenChannel createChannel(InputStream in) { return language().createChannel(in); }
	
	public final STokenChannel createChannel(OutputStream out) { return language().createChannel(out); }
	
	public final STokenChannel createChannel(java.nio.channels.ByteChannel ch) { return byteLanguage().createChannel(ch); }
	
	public final STokenChannel createChannel(ByteChannel ch) { return byteLanguage().createChannel(ch); }
	
	public final STokenChannel createChannel(Reader in) { return charLanguage().createChannel(in); }
	
	public final STokenChannel createChannel(Writer out) { return charLanguage().createChannel(out); }
	
	public final STokenChannel createChannel(CharChannel ch) { return charLanguage().createChannel(ch); }
	
	public final boolean hasDescriptor(Type type) { return getDescriptor1(type).exc == null; }
	
	public final boolean contains(TypeDescriptor type) { var tmp = getDescriptor1(type.genericType); return tmp.exc == null && tmp.type == type; }
	
	public final boolean contains(FieldDescriptor field) { return contains(field.parent()); }
	
	public final TypeDescriptor getDescriptor(Type type) { return getDescriptor0(type).type; }
	
	public final TypeDescriptor getDescriptorOrNull(Type type) { return getDescriptor1(type).type; }
	
	/**@return A list of all super-types that the given type has (including all indirect super-types).*/
	public final List<Class<?>> getAllSupertypes(Class<?> type) { return getAllSupertypes0(type).superTypesPub; }
	
	/**@return A list of all known sub-types that the given type currently has
	 * (including all indirect sub-types). Mainly useful for schema generation.*/
	public final List<Class<?>> getAllKnownSubtypes(Class<?> type) { return getDescriptor1(type).knownSubTypesPub; }
	
	/**The maximum depth at which a given type is found.
	 * @return -1 if the type is not supported by this serializer, 0 if the type is a base type
	 * or the maximum amount of steps one needs to take to reach a base type.*/
	public final int getTypeHierarchyDepth(Class<?> type) { return getAllSupertypes0(type).depth; }
	
	public final boolean isSubType(Class<?> baseType, Class<?> subType) {
		return getAllKnownSubtypes(baseType).contains(Objects.requireNonNull(subType));
	}
	
	public final boolean isDirectSubType(Class<?> baseType, Class<?> subType) {
		if(baseType.isInterface()) {
			if(!Arrays.asList(subType.getInterfaces()).contains(baseType)) return false;
		}
		else if(subType.getSuperclass() != baseType) return false;
		return hasDescriptor(baseType) && hasDescriptor(subType);
	}
	
	private final CacheValue getAllSupertypes0(Class<?> type) {
		var tmp = getDescriptor1(type);
		if(tmp.exc != null) { tmp.superTypesPub = List.of(); tmp.depth = -1; return tmp; }
		if(tmp.superTypesPub != null) return tmp;
		var l = new HashSet<Class<?>>(); var x = type.getSuperclass(); int depth = 0;
		if(x != null && getAllKnownSubtypes(x).contains(type)) {
			l.add(x); var z = getAllSupertypes0(x); l.addAll(z.superTypesPub); depth = Math.max(depth,z.depth+1);
		}
		for(var y : type.getInterfaces()) {
			if(!getAllKnownSubtypes(y).contains(type)) continue;
			l.add(y); var z = getAllSupertypes0(y); l.addAll(z.superTypesPub); depth = Math.max(depth,z.depth+1);
		}
		tmp.superTypesPub = List.copyOf(l); tmp.depth = depth; return tmp;
	}
	
	public final List<FieldDescriptor> getAllParentFields(FieldDescriptor fld) { return getAllParentFields(fld,false); }
	
	public final List<FieldDescriptor> getDirectParentFields(FieldDescriptor fld) { return getAllParentFields(fld,true); }
	
	private List<FieldDescriptor> getAllParentFields(FieldDescriptor fld, boolean direct) {
		var l = List.<FieldDescriptor>of();
		if(!contains(fld)) return l;
		for(var parent : getAllSupertypes(fld.parent().type)) {
			if(direct && !isDirectSubType(parent,fld.parent().type)) continue;
			var tmp = getParentFieldOrNull(parent,fld);
			if(tmp == null) continue;
			if(l.isEmpty()) { l = List.of(tmp); continue; }
			if(l.size() == 1) l = new ArrayList<>(l);
			l.add(tmp);
		}
		return List.copyOf(l);
	}
	
	public final boolean overridesField(FieldDescriptor parent, FieldDescriptor child) {
		return getChildFieldOrNull(parent,child.parent()) == child;
	}
	
	public final FieldDescriptor getParentFieldOrNull(Class<?> parent, FieldDescriptor child) {
		var tmp = getDescriptor1(parent);
		return tmp.exc != null ? null : getParentFieldOrNull((ObjectTypeDescriptor)tmp.type,child);
	}
	
	public final FieldDescriptor getParentFieldOrNull(ObjectTypeDescriptor parent, FieldDescriptor child) {
		return isSubType(parent.type,child.parent().type) ? parent.fieldsByName().get(child.name) : null;
	}
	
	public final FieldDescriptor getChildFieldOrNull(FieldDescriptor parent, Class<?> child) {
		var tmp = getDescriptor1(child);
		return tmp.exc != null ? null : getChildFieldOrNull(parent,(ObjectTypeDescriptor)tmp.type);
	}
	
	public final FieldDescriptor getChildFieldOrNull(FieldDescriptor parent, ObjectTypeDescriptor child) {
		if(!isSubType(parent.parent().type,child.type)) return null;
		var ret = child.fieldsByName().get(parent.name);
		if(ret != null) return ret;
		throw new Error();
	}
	
	@SuppressWarnings("unchecked")
	public final <T> T createInstance(Class<T> type) { return (T)getDescriptor0(type).type.createInstance(); }
	
	/**Creates an instance of the given type or of a sub-type if the given type is abstract.*/
	public final <T> T createAnyInstance(Class<T> type) {
		T ret = createInstance(type);
		if(ret != null) return ret;
		for(var subType : getAllKnownSubtypes(type)) {
			ret = type.cast(createInstance(subType));
			if(ret != null) return ret;
		}
		throw new Error(errors.unreachable);
	}
	
	public final int hashCode(Object obj) { return hashCode0((Type)null,obj,-1); }
	
	public final int hashCode(FieldDescriptor staticType, Object obj) { return hashCode0(staticType.genericType,obj,-1); }
	
	public final int narrowHashCode(Object obj) { return hashCode0((Type)null,obj,narrowDepth); }
	
	public final boolean equals(Object obj1, Object obj2) { return equals0((Type)null,obj1,obj2,-1); }
	
	public final boolean equals(FieldDescriptor staticType, Object obj1, Object obj2) { return equals0(staticType.genericType,obj1,obj2,-1); }
	
	public final Boolean narrowEquals(Object obj1, Object obj2) { return equals0((Type)null,obj1,obj2,narrowDepth); }
	
	@SuppressWarnings("unchecked")
	public final <T> T clone(T obj) { return (T)clone0(obj,-1); }
	
	public final <T> T cloneField(FieldDescriptor f, T obj) { return cloneField0(f,obj,-1); }
	
	@SuppressWarnings("unchecked")
	public final <T> T narrowClone(T obj) { return (T)clone0(obj,1); }
	
	public final <T> T narrowCloneField(FieldDescriptor f, T obj) { return cloneField0(f,obj,1); }
	
	@SuppressWarnings("unchecked")
	private final <T> T cloneField0(FieldDescriptor f, T obj, int maxDepth) {
		if(!contains(f)) throw new IllegalArgumentException("field "+f+" is not from this serializer");
		return (T)copy0(f.genericType,obj,null,maxDepth);
	}
	
	public final Object copy(Object src, Object dest) { return copy0((Type)null,src,dest,-1); }
	
	public final Object narrowCopy(Object src, Object dest) { return copy0((Type)null,src,dest,1); }
	
	@SuppressWarnings("unchecked")
	public final <T> T resetToDefault(T obj) {
		if(obj == null) return null;
		var type = getDescriptor0(obj.getClass());
		return (T)copy(type.defaultValue,obj);
	}
	
	public final void readInput(InputStream in) throws IOException { readInput(language().createChannel(in)); }
	
	/**Reads and validates the input without actually deserializing it.*/
	public final void readInput(STokenChannel in) throws IOException {
		if(!in.canRead()) throw new UnsupportedOperationException("given channel cannot be read");
		checkLanguage(in); readInput0(null,in);
	}
	
	public final <T> T deserialize(Class<T> staticType, InputStream in) throws IOException {
		return deserialize(staticType,language().createChannel(in));
	}
	
	@SuppressWarnings("unchecked")
	public final <T> T deserialize(Class<T> staticType, STokenChannel in) throws IOException {
		if(!in.canRead()) throw new UnsupportedOperationException("given channel cannot be read");
		checkLanguage(in); return (T)deserialize0(staticType,null,in);
	}
	
	public final <T> T parse(Class<T> staticType, String str) {
		try(var buf = new StringReader(str); var in = charLanguage().createChannel(buf)) {
			return deserialize(staticType,in);
		} catch(IOException exc) { throw new Error(exc); }
	}
	
	public final <T> T parse(Class<T> staticType, byte[] arr) { return parse(staticType,arr,0,arr.length); }
	
	public final <T> T parse(Class<T> staticType, byte[] arr, int offset, int length) {
		try(var buf = new ByteArrayInputStream(arr,offset,length); var in = language().createChannel(buf)) {
			return deserialize(staticType,in);
		} catch(IOException exc) { throw new Error(exc); }
	}
	
	@SuppressWarnings("unchecked")
	public final <T> T toObject(Class<T> staticType, SObject obj) { return (T)deserializeS0(staticType,obj); }
	
	public final <T> void serialize(Class<T> staticType, T obj, OutputStream out) throws IOException {
		var tmp = language().createChannel(out); serialize(staticType,obj,tmp); tmp.flush();
	}
	
	public final <T> void serialize(Class<T> staticType, T obj, STokenChannel out) throws IOException {
		if(!out.canWrite()) throw new UnsupportedOperationException("given channel cannot be written");
		checkLanguage(out); serialize0(staticType,obj,out);
		out.write(new SToken(endOfInput));
	}
	
	public final <T> String toString(Class<T> staticType, T obj) {
		try(var buf = new StringWriter(); var out = charLanguage().createChannel(buf)) {
			serialize(staticType,obj,out); out.flush(); return buf.toString();
		} catch(IOException exc) { throw new Error(exc); }
	}
	
	public final <T> byte[] toArray(Class<T> staticType, T obj) {
		try(var buf = new ByteArrayOutputStream(); var out = language().createChannel(buf)) {
			serialize(staticType,obj,out); out.flush(); return buf.toByteArray();
		} catch(IOException exc) { throw new Error(exc); }
	}
	
	public final <T> SObject toSObject(Class<T> staticType, T obj) { return serializeS0(staticType,obj); }
	
	private void checkLanguage(STokenChannel ch) {
		var lang = ch.language();
		if(lang != null && !lang.equals(language())) throw new IllegalArgumentException("invalid language");
	}
	
	@Override Type validateExternalDynamicTypeInfo(FieldDescriptor f, Object obj) { return f.validateExternalDynamicTypeInfo(obj,this); }

	@Override Type getExternalDynamicTypeInfo(FieldDescriptor f, Object obj) { return f.getExternalDynamicTypeInfo(obj,this); }
}
