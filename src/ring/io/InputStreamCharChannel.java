package ring.io;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.CharBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

/**Wraps an {@link InputStream} to a {@link CharChannel}.*/
public final class InputStreamCharChannel implements CharChannel {
	private final Reader reader;
	
	public InputStreamCharChannel(InputStream in) { this(new InputStreamReader(in,StandardCharsets.UTF_8)); }
	
	public InputStreamCharChannel(Reader reader) { this.reader = Objects.requireNonNull(reader); }
	
	@Override public boolean canRead() { return true; }
	
	@Override public int read(CharBuffer dest) throws IOException { return reader.read(dest); }
	
	@Override public void close() throws IOException { reader.close(); }
}
