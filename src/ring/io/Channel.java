package ring.io;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.io.IOException;
import java.nio.Buffer;

/**The base interface used for io.<br>
 * In most cases, you do not need to inherit this directly.<br>
 * Use {@link ByteChannel} or {@link CharChannel}.*/
public interface Channel<T extends Buffer> extends ChannelBase {
	/**Reads data from the underlying stream to the given buffer.
	 * @return The amount read or 0 if the end has been reached.*/
	default int read(T dest) throws IOException { throw new UnsupportedOperationException(); }
	
	/**Writes data from the given buffer to the underlying stream.
	 * @return The amount written.*/
	default int write(T src) throws IOException { throw new UnsupportedOperationException(); }
}
