package ring.io;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.util.List;

import ring.serializer.Language;
import ring.serializer.token.SToken;

/**Use this to read and write from/to a list of tokens.<br>
 * This class is mainly for debugging purposes.
 * @see ListChannel*/
public final class STokenListChannel extends AbstractListChannel<SToken> implements STokenChannel {
	public STokenListChannel() {}
	
	public STokenListChannel(List<SToken> data) { super(data); }
	
	public STokenListChannel(List<SToken> data, boolean canRead, boolean canWrite, boolean canGet) {
		super(data,canRead,canWrite,canGet);
		for(var e : data) e.checkValid();
	}
	
	@Override public Language language() { return null; }
	
	@Override public SToken read() { var ret = super.read(); ret.checkValid(); return ret; }
	
	@Override public void write(SToken tok) { tok.checkValid(); super.write(tok); }
}
