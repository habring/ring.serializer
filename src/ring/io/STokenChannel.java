package ring.io;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.nio.channels.ByteChannel;

import ring.serializer.Language;
import ring.serializer.token.SToken;
import ring.serializer.token.STokenType;

/**The implementation may assume that the syntax of the given tokens is correct (without checking it). See {@link SToken} for details.<br>
 * In other words, the result is undefined if the syntax is incorrect when writing.<br>
 * If the input is invalid when reading, the implementation may either throw an exception or produce incorrect tokens (or an incorrect token syntax).<br>
 * A flush resolves such an invalid state and clears all internally stored data.<br>
 * Note that because of that, a flush is only allowed between serialization processes.<br>
 * It is recommended, but not required to detect an {@link STokenType#endOfInput} when reading.<br>
 * It is recommended to provide a constructor that takes either a {@link ByteChannel} or a {@link CharChannel}.*/
public interface STokenChannel extends TokenChannel<SToken> {
	/**The language this channel was created from.*/
	Language language();
}
