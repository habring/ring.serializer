package ring.io;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.io.IOException;
import java.util.Objects;

import ring.serializer.Serializer;

public class SerializerChannel<TLanguage> implements TokenChannel<TLanguage> {
	private final Serializer serializer;
	private final Class<TLanguage> langCls;
	private final STokenChannel channel;
	
	public SerializerChannel(Serializer serializer, Class<TLanguage> langCls, STokenChannel channel) {
		this.serializer = serializer; this.langCls = langCls; serializer.getDescriptor(langCls);
		this.channel = Objects.requireNonNull(channel);
	}
	
	@Override public boolean canRead() { return channel.canRead(); }
	
	@Override public boolean canWrite() { return channel.canWrite(); }
	
	@Override public void flush() throws IOException { channel.flush(); }
	
	@Override public void close() throws IOException { channel.close(); }
	
	@Override public TLanguage read() throws IOException { return serializer.deserialize(langCls,channel); }
	
	@Override public void write(TLanguage obj) throws IOException { serializer.serialize(langCls,obj,channel); }
}
