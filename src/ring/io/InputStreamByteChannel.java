package ring.io;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Objects;

/**Wraps an {@link InputStream} to a {@link ByteChannel}.*/
public final class InputStreamByteChannel implements ByteChannel {
	private final InputStream in;
	
	public InputStreamByteChannel(InputStream in) { this.in = Objects.requireNonNull(in); }
	
	@Override public boolean canRead() { return true; }
	
	@Override
	public int read(ByteBuffer dest) throws IOException {
		var ret = in.read(dest.array(),dest.arrayOffset()+dest.position(),dest.remaining());
		if(ret > 0) dest.position(dest.position()+ret);
		return ret;
	}
	
	@Override public void close() throws IOException { in.close(); }
}
