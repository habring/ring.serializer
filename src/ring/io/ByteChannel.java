package ring.io;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.io.IOException;
import java.nio.ByteBuffer;

/**A channel representing a byte stream.*/
public interface ByteChannel extends Channel<ByteBuffer>, java.nio.channels.ByteChannel {
	@Override default boolean isOpen() { return true; }
	
	/**Wraps a {@link java.nio.channels.ByteChannel}.*/
	static ByteChannel wrap(java.nio.channels.ByteChannel ch) {
		return new ByteChannel() {
			@Override public boolean canRead() { return true; }
			@Override public int read(ByteBuffer dest) throws IOException { return ch.read(dest); }
			@Override public boolean canWrite() { return true; }
			@Override public int write(ByteBuffer src) throws IOException { return ch.write(src); }
			@Override public void close() throws IOException { ch.close(); }
		};
	}
}
