package ring.io;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.CharBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

/**Wraps an {@link OutputStream} to a {@link CharChannel}.*/
public final class OutputStreamCharChannel implements CharChannel {
	private final Writer writer;
	
	public OutputStreamCharChannel(OutputStream out) { this(new OutputStreamWriter(out,StandardCharsets.UTF_8)); }
	
	public OutputStreamCharChannel(Writer writer) { this.writer = Objects.requireNonNull(writer); }
	
	@Override public boolean canWrite() { return true; }
	
	@Override
	public int write(CharBuffer src) throws IOException {
		var ret = src.remaining();
		writer.write(src.array(),src.arrayOffset()+src.position(),ret);
		src.position(src.limit()); return ret;
	}
	
	@Override public void flush() throws IOException { writer.flush(); }

	@Override public void close() throws IOException { writer.close(); }
}
