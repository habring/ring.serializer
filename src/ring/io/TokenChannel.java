package ring.io;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Objects;

/**The base interface for all token channels.*/
public interface TokenChannel<Token> extends ChannelBase {
	/**Reads the next token from the stream or null if the end has been reached.*/
	default Token read() throws IOException { throw new UnsupportedOperationException(); }
	
	/**Writes a token to the stream.*/
	default void write(Token tok) throws IOException { throw new UnsupportedOperationException(); }
	
	static TokenChannel<Byte> wrap(Channel<ByteBuffer> ch) {
		Objects.requireNonNull(ch);
		return new TokenChannel<>() {
			final ByteBuffer buf = ByteBuffer.allocate(1);
			private ByteBuffer buf() { return buf.position(0); }
			@Override public boolean canRead() { return ch.canRead(); }
			@Override public Byte read() throws IOException { int n = ch.read(buf()); return n == 0 ? null : buf.get(0); }
			@Override public boolean canWrite() { return ch.canWrite(); }
			@Override public void write(Byte tok) throws IOException { buf.put(0,tok); ch.write(buf()); }
			@Override public void flush() throws IOException { ch.flush(); }
			@Override public void close() throws IOException { ch.close(); }
		};
	}
}
