package ring.io;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

abstract class AbstractListChannel<Token> implements TokenChannel<Token> {
	private final List<Token> l;
	private final boolean canGet, canRead, canWrite;
	private int nextIdx;
	
	public AbstractListChannel() { this(new ArrayList<>()); }
	
	public AbstractListChannel(List<Token> data) { this(data,true,true,true); }
	
	public AbstractListChannel(List<Token> data, boolean canRead, boolean canWrite, boolean canGet) {
		this.l = Objects.requireNonNull(data); this.canGet = canGet; this.canRead = canRead; this.canWrite = canWrite;
		if(!canRead && !canWrite) throw new IllegalArgumentException("channel must be readable or writable or both");
	}
	
	/**The next index that would be returned by a consecutive read (or written by a consecutive write).*/
	public int nextIndex() { return nextIdx; }
	
	/**Sets the next index.
	 * @see #nextIndex()*/
	public void setNextIndex(int index) { this.nextIdx = index; }
	
	/**@return the underlying {@link List}*/
	public List<Token> getList() {
		if(canGet) return l;
		throw new UnsupportedOperationException("channel is not public");
	}
	
	@Override public boolean canRead() { return canRead; }
	
	@Override public boolean canWrite() { return canWrite; }
	
	@Override
	public Token read() {
		if(!canRead) throw new UnsupportedOperationException();
		if(nextIdx >= l.size()) return null;
		Token ret = l.get(nextIdx); nextIdx++; return ret;
	}
	
	@Override
	public void write(Token tok) {
		if(!canWrite) throw new UnsupportedOperationException();
		if(nextIdx < l.size()) l.set(nextIdx,tok);
		else l.add(tok);
		nextIdx = Math.min(nextIdx+1,l.size());
	}
	
	@Override public void flush() {}
	
	@Override public void close() {}
}
