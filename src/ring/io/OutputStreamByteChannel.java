package ring.io;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.Objects;

/**Wraps an {@link OutputStream} to a {@link ByteChannel}.*/
public final class OutputStreamByteChannel implements ByteChannel {
	private final OutputStream out;
	
	public OutputStreamByteChannel(OutputStream out) { this.out = Objects.requireNonNull(out); }
	
	@Override public boolean canWrite() { return true; }
	
	@Override
	public int write(ByteBuffer src) throws IOException {
		var ret = src.remaining();
		out.write(src.array(),src.arrayOffset()+src.position(),ret);
		src.position(src.limit()); return ret;
	}
	
	@Override public void flush() throws IOException { out.flush(); }

	@Override public void close() throws IOException { out.close(); }
}
