package ring.io;
/* Copyright 2020-2024 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Serializer.
 * 
 * Ring.Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Serializer. If not, see http://www.gnu.org/licenses/.
 * */

import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;

interface ChannelBase extends Closeable, Flushable {
	/**@return True if this channel can be read.*/
	default boolean canRead() { return false; }

	/**@return True if this channel can be written.*/
	default boolean canWrite() { return false; }
	
	/**{@inheritDoc}<br>
	 * An implementation should automatically flush when a read happens after a write.*/
	@Override default void flush() throws IOException {}
}
