# Ring.Serializer

This library allows you to serialize and deserialize objects to/from any serialization language.  
Predefined languages are: Json (incl. schema generation), Xml, Csv, Config Files (similar to ini or toml) and Binary;  
Users can define their own language or schema generator (with a few 100 lines of code).  
Each type that should be serializable is represented by one of five kinds of TypeDescriptor-objects:

<table border="1" cellpadding="5">
	<tr><th>TypeDescriptor kind</th><th>Description</th></tr>
	<tr><td>PrimitiveTypeDescriptor</td>	<td>Declares a type as primitive type.</td></tr>
	<tr><td>ListTypeDescriptor</td>			<td>Represents a List or Set.</td></tr>
	<tr><td>MapTypeDescriptor</td>			<td>Represents a Map.</td></tr>
	<tr><td>ConvertTypeDescriptor</td>		<td>Converts a type to a different one.</td></tr>
	<tr><td>ObjectTypeDescriptor</td>		<td>Type descriptor for "normal" objects.</td></tr>
	<tr><td>SObjectTypeDescriptor</td>		<td>Represents the SObject-class and its sub-types.</td></tr>
</table>

ObjectTypeDescriptors are essentially a list of FieldDescriptors. There are three kinds of FieldDescriptors:

<table border="1" cellpadding="5">
	<tr><th>FieldDescriptor kind</th><th>Description</th></tr>
	<tr><td>ReflectFieldDescriptor</td>		<td>Uses reflection to access fields or getter-/setter-methods.</td></tr>
	<tr><td>FunctionFieldDescriptor</td>	<td>Uses Function-objects to access a field (that does not have a direct representation in the according class).</td></tr>
	<tr><td>RecursiveFieldDescriptor</td>	<td>Can be used to "read-through" multiple levels of fields.</td></tr>
</table>

Users of this library must find a way to represent their custom types as a combination of these descriptors.  
To do this, TypeDescriptorFactory-objects are used. There are multiple predefined ones to simplify this process:

- For all kinds of primitive types, the TypeDescriptorFactories-class provides instances of TypeDescriptorFactory.
- The ReflectTypeDescriptorFactory-class provides a very flexible way to annotate your class in order to be serializable. Most cases should be covered by this class.

# Basic Example

```java
class Person {
	@SField String firstName;
	@SField String lastName;
}
var config = new SerializerConfig().setLanguage(new JsonLanguage());
var serializer = new Serializer(config);
var p = new Person();
p.firstName = "Foo";
p.lastName = "Bar";
var str = serializer.toString(Person.class,p);
System.out.println(str);
var p2 = serializer.parse(Person.class,str);
System.out.println("Person("+p2.firstName+","+p2.lastName+")");
```

The output will be:

```
{
  "firstName": "Foo",
  "lastName": "Bar"
}

Person(Foo,Bar)
```

Look into the provided SerializerTest-class for a more advanced example.

# Dynamic Typesafety

Dynamic typesafety is directly built into the serialization engine, only available on ObjectTypeDescriptors and the mechanisms cannot be changed by the user.  
There are two ways to express dynamic types:

1. The first field is marked with the dynamicTypeInfo-Flag.
2. There is a field marked with the externalDynamicTypeInfo-Flag, immediately followed by one marked with the externalDynamicTyped-Flag.

Example:

```json
{
  "type": "student",
  "id": 12345,
  "firstName": "Foo",
  "lastName": "Bar",
  "parentType": "person",
  "parent": {
    "firstName": "Baz",
    "lastName": "Bar"
  }
}
```

The class-structure for this example would look something like this:

```java
@SClass
class Person {
	@SField String firstName;
	@SField String lastName;
	@SField String parentType;
	@SField(externalTyped=true) Object parent;
}
@SSubClass
class Student extends Person {
	@SField int id;
}
```

By using external dynamic typing you can also use types that are currently not loaded (meaning that there is no assembly containing them).  
These objects will then be deserialized into the generic SObject-class.

# Dependencies

Ring.Serializer has no dependencies.

# Limitations

You may discover that there is no way to express conditional formats with TypeDescriptors.  
For example, you cannot specifiy that a certain field in an ObjectTypeDescriptor only exists if another fields value is larger than 10.  
This is done for simplicity reasons. Allowing this would lead to a huge load of additional code with very few use cases where this is really needed.
It would also complicate schema generation.  
In most of these cases, you should be able to use the ConvertTypeDescriptor to work around that limitation.  
There is however one exception where conditional formats are indeed allowed, namely dynamic typesafety.

# Copyright

Copyright 2020 Lukas Habring

For commercial licenses, just contact Lukas Habring: <ring.commercial@gmx.at>  
This file is part of Ring.Serializer.

Ring.Serializer is free software: you can redistribute it and/or modify  
it under the terms of the GNU Affero General Public License version 3  
(and only version 3) as published by the Free Software Foundation.

Ring.Serializer is distributed in the hope that it will be useful,  
but WITHOUT ANY WARRANTY; without even the implied warranty of  
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the  
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License  
along with Ring.Serializer. If not, see <http://www.gnu.org/licenses/>.
